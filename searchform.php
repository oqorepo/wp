<form method="get" class="search" action="<?php echo site_url(); ?>/">
    
    <input type="text" name="s" id="s" value="<?php echo get_search_query(); ?>" placeholder="<?php _e('Buscar', 'epcl_theme'); ?>">
    <button type="submit" class="submit button"><?php _e('Enviar', 'epcl_theme'); ?></button>
</form>