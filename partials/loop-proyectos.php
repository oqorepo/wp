<?php
global $proyectos;
$delay = 0;
foreach($proyectos as $post): setup_postdata($post);	
	$video_proyecto = get_field('video_proyecto');
	$logo_proyecto = get_field('logo_proyecto');
	$youtube_id = '';
	if($video_proyecto){
		$youtube_id = youtube_id($video_proyecto);	
	}
	$ciudad = get_field('ciudad');
	$delay += 100;
?>
	<div class="item grid-25 tablet-grid-50 grid-parent <?php if($youtube_id) echo 'has-video'; ?>" data-aos="fade-up" data-aos-delay="<?php echo $delay; ?>">
    	<div class="info">
            <h3 style="display: none;"><?php the_title(); ?></h3>
            <?php if(!empty($logo_proyecto)): ?>
                <p class="logo"><img src="<?php echo $logo_proyecto['sizes']['proyecto-logo']; ?>"></p>
            <?php endif; ?>
            <?php if($ciudad): ?>
                <p class="ciudad titulo small"><?php echo $ciudad; ?></p>
            <?php endif; ?>
        </div>
        <!--<div class="img cover" style="background: url(<?php echo get_the_post_thumbnail_url($post, 'proyecto-thumb'); ?>) no-repeat;"></div>-->
		<div class="img"><img src="<?php echo get_the_post_thumbnail_url($post, 'proyecto-thumb'); ?>" class="fullwidth"></div>
        <div class="video" data-id="<?php echo $youtube_id; ?>" id="video-<?php echo $youtube_id; ?>"></div>
        <a href="<?php the_permalink(); ?>" class="full-link"></a>
	</div>   
<?php endforeach; ?>