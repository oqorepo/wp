<?php global $epcl_theme; ?>
<!-- start: #footer -->
<footer id="footer" role="contentinfo" class="">
    <div class="grid-container grid-medium np-mobile">
    	<div class="grid-20 tablet-grid-33">
            <a href="<?php echo site_url(); ?>" class="logo"><img src="<?php echo EP_THEMEPATH; ?>/images/logo-color2.png"></a>
        </div>
        <div class="hide-on-mobile">
            <div class="widget grid-20 tablet-grid-33">
                <h5><a href="<?php echo site_url(); ?>/construccion/">Construcción</a></h5>
                <ul>
                    <li><a href="<?php echo site_url(); ?>/construccion/#obras">Proyectos en Desarrollo</a></li>
                    <li><a href="<?php echo site_url(); ?>/construccion/#obras-ejecutadas">Obras Ejecutadas</a></li>
                </ul>
            </div>
            <div class="widget grid-20 tablet-grid-33">
                <h5><a href="<?php echo site_url(); ?>/inmobiliaria">Inmobiliaria</a></h5>
                <ul>
                    <li><a href="<?php echo site_url(); ?>/inmobiliaria">Proyectos Inmobiliarios</a></li>
                    <li><a href="<?php echo site_url(); ?>/inmobiliaria/#proyectos-ejecutados">Proyectos Ejecutados</a></li>
                    <li><a href="<?php echo site_url(); ?>/inmobiliaria/#proyectos">Proyectos en Venta</a></li>
                    <!--<li><a href="http://ialmahue.com/buscamos-sitios/" target="_blank">Buscamos Sitios</a></li>-->
                </ul>
            </div>
            <div class="clear hide-on-desktop"></div>
            <?php
			$args = array(
				'posts_per_page' => -1,
				'post_type' => 'proyectos',
				'suppress_filters' => false,
				'orderby' => 'menu_order',
				'order' => 'ASC',
				'meta_key' => 'estado',
				'meta_value' => 'venta'
			);
			$proyectos = get_posts($args);
			if(!empty($proyectos)):
			?>
                <div class="widget grid-20 tablet-grid-33">
                    <h5>proyectos en venta</h5>
                    <ul>
                    	<?php foreach($proyectos as $post): setup_postdata($post); ?>
                            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; wp_reset_postdata(); ?>
            
            <div class="widget grid-20 tablet-grid-33">
                <h5><a href="<?php echo site_url(); ?>/inversionistas">Inversionistas</a></h5>
                <ul>
                    <li><a href="<?php echo site_url(); ?>/inversionistas/#beneficios">Beneficios</a></li>
                    <li><a href="<?php echo site_url(); ?>/inversionistas/#invertir">¿Por qué Desco?</a></li>
                    <li><a href="<?php echo site_url(); ?>/inversionistas/#faq">Preguntas Frecuentes</a></li>
                    <li><a href="<?php echo site_url(); ?>/inversionistas/#form">Inversionista Corporativo</a></li>
                </ul>
            </div>
            <!--<div class="clear"></div>-->
            <div class="widget grid-20 prefix-20 tablet-grid-33">
                <h5><a href="<?php echo site_url(); ?>/somos">Somos Desco</a></h5>
                <ul>
                    <li><a href="<?php echo site_url(); ?>/somos/#mision">Misión / Visión</a></li>
                    <li><a href="<?php echo site_url(); ?>/somos/#directores">Directorio</a></li>
                    <li><a href="<?php echo site_url(); ?>/somos/#obras-historicas">Obras 1938-2000</a></li>
                    <li><a href="<?php echo site_url(); ?>/somos/#novedades">Prensa</a></li>
                </ul>
            </div>
            <div class="widget grid-20 tablet-grid-33">
                <h5><a href="<?php echo site_url(); ?>/canal-de-denuncia">Canal de Denuncia</a></h5>
                <ul>
                    <li><a href="<?php echo site_url(); ?>/canal-de-denuncia">Denuncia</a></li>
                    <li><a href="<?php echo site_url(); ?>/canal-de-denuncia/#menu-2">Ley 20393</a></li>
                    <li><a href="<?php echo site_url(); ?>/canal-de-denuncia/#menu-3">Ley 19913</a></li>
                    <li><a href="<?php echo site_url(); ?>/canal-de-denuncia/#menu-4">Descargar Código Ética</a></li>
                </ul>
            </div>
            <div class="widget grid-20 tablet-grid-33">
                <h5>Trabaje con Nosotros</h5>
                <ul>
                    <li><a href="https://www.desco.cl/old/rrhh/index.php?lightbox" class="lightbox-rrhh mfp-iframe">Postular</a></li>
                </ul>
            </div>
            <div class="widget grid-20 tablet-grid-33">
                <h5><a href="<?php echo site_url(); ?>/portal-privado">Acceso Propietarios</a></h5>
                <ul>
                    <li><a href="<?php echo site_url(); ?>/portal-privado">Ingresar</a></li>
                </ul>
            </div>
        </div>
        <div class="clear"></div>
    
        <div class="volver grid-50 tablet-grid-50">
            <a href="javascript:void(0)" id="back-to-top">Ir arriba <span><i class="flaticon-up-arrow"></i></span></a>
            <div class="fondo"></div>
        </div>
        <div class="direccion grid-50 tablet-grid-50">
        	<div class="fondo"></div>
            <div class="grid-55 grid-parent">
                <p><?php echo $epcl_theme['direccion']; ?> <br>Tel: <a href="tel:<?php echo str_replace(array(' ', '(', ')'), '', $epcl_theme['telefono']); ?>"><?php echo $epcl_theme['telefono']; ?></a><?php /*?><br><a href="#"><?php echo antispambot('contacto@desco.cl'); ?></a><?php */?></p>
            </div>
            <div class="social grid-45">
                Síguenos en
                <?php if( $epcl_theme['facebook'] ): ?>
                    <a href="<?php echo $epcl_theme['facebook']; ?>" target="_blank" title="Facebook" class="facebook"><i class="fa fa-facebook"></i></a>
                <?php endif; ?>
                <?php if( $epcl_theme['twitter'] ): ?>
                    <a href="<?php echo $epcl_theme['twitter']; ?>" target="_blank" title="Twitter" class="twitter"><i class="fa fa-twitter"></i></a>
                <?php endif; ?>
                <?php if( $epcl_theme['youtube'] ): ?>
                    <a href="<?php echo $epcl_theme['youtube']; ?>" target="_blank" title="Youtube" class="youtube"><i class="fa fa-youtube-play"></i></a>
                <?php endif; ?>
                <?php if( $epcl_theme['instagram'] ): ?>
                    <a href="<?php echo $epcl_theme['instagram']; ?>" target="_blank" title="Instagram" class="instagram"><i class="fa fa-instagram"></i></a>
                <?php endif; ?>
            </div>
            <div class="clear"></div>
            <a href="http://www.oqo.cl" class="oqo">Desarrollado por OQO.cl</a>
        </div>
    </div>
    <div class="clear"></div>
</footer>
<!-- end: #footer -->
