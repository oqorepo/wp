<?php the_post(); ?>

<?php
$numero_obra = get_field('numero_obra');
$descripcion = get_field('descripcion');
$tipo = get_field('tipo');
$ubicacion = get_field('ubicacion');
$fecha = get_field('fecha');
$superficie = get_field('superficie');
$monto_contrato = get_field('monto_contrato');
$galeria = get_field('galeria');
$categoria = get_the_terms( $post->ID, 'categoria-obras');
?>

<!-- start: #obras -->
<section id="obras" class="carrusel-proyectos ajax grid-container grid-small grid-parent np-tablet">

        <div class="lista grid-30 tablet-grid-33 grid-parent">
            <div class="item">
                <div class="info">
                    <h4 class="titulo usmall">
                        <?php if( $numero_obra && $categoria[0]->slug != 'portuarias' ): ?>
                            Nº <?php echo $numero_obra; ?> - 
                        <?php endif; ?>
                        <?php the_title(); ?>
                    </h4>
                    <!-- <p class="hide-on-mobile"><?php echo $descripcion; ?></p> -->
                    <?php if($ubicacion): ?>
                        <div class="detalle hide-on-mobile hide-on-tablet">
                            <h5 class="titulo usmall">Ubicación:</h5>
                            <p><?php echo $ubicacion; ?></p>
                        </div>
                    <?php endif; ?>
                    <?php if($fecha): ?>
                        <div class="detalle hide-on-mobile hide-on-tablet">
                            <h5><span class="titulo usmall">Año:</span> <?php echo $fecha; ?></h5>
                        </div>
                    <?php endif; ?>
                    <?php if($monto_contrato): ?>
                        <div class="detalle hide-on-mobile hide-on-tablet">
                            <h5><span class="titulo usmall">Monto de Contrato (UF):</span> <?php echo $monto_contrato; ?></h5>
                        </div>
                    <?php endif; ?>
                    <?php if($superficie): ?>
                        <div class="detalle hide-on-mobile hide-on-tablet">
                            <h5><span class="titulo usmall">Sup. Edificada:</span> <?php echo $superficie; ?></h5>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="galeria grid-70 tablet-grid-66 grid-parent alta">
            

                <!-- start: .proyecto -->
                <div class="proyecto active">
                    <div class="slick-slider circle-dots">
                        <?php foreach($galeria as $g): ?>
                            <?php $class_width = ($g['width'] < $g['height']) ? 'contain': 'cover'; ?>
                            <div class="item">
                                <div class="fullimg <?php echo $class_width; ?>" style="background-image: url('<?php echo $g['sizes']['large']; ?>')"></div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="info hide-on-desktop">
                        <?php if($ubicacion): ?>
                            <div class="grid-30 tablet-grid-50 mobile-grid-65">
                                <h5>Ubicación:</h5>
                                <p><?php echo $ubicacion; ?></p>
                            </div>
                        <?php endif; ?>
                        <?php if($fecha): ?>
                            <div class="grid-20 tablet-grid-50 mobile-grid-35">
                                <h5>Año:</h5>
                                <p><?php echo $fecha; ?></p>
                            </div>
                        <?php endif; ?>
                        <div class="clear hide-on-desktop"></div>
                        <?php if($monto_contrato): ?>
                            <div class="grid-30 tablet-grid-50 mobile-grid-60">
                                <h5>Monto de Contrato (UF):</h5>
                                <p><?php echo $monto_contrato; ?></p>
                            </div>
                        <?php endif; ?>
                        <?php if($superficie): ?>
                            <div class="grid-20 tablet-grid-50 mobile-grid-40">
                                <h5>Sup. Edificada:</h5>
                                <p><?php echo $superficie; ?></p>
                            </div>
                        <?php endif; ?>

                        <div class="clear"></div>
                    </div>
                </div>
                <!-- end: .proyecto -->

        </div>

        <div class="clear"></div>
</section>
<!-- end: #obras -->

<script type="text/javascript" src="<?php echo EP_THEMEPATH; ?>/js/slick/slick.min.js"></script>
<script type="text/javascript">
(function($){
    $(window).load(function($){
        

        
    }); 
})(jQuery);
</script>
