<?php

/* Base Urls */

if(!defined('EP_ABSPATH')) define('EP_ABSPATH', get_template_directory());
if(!defined('EP_THEMEPATH')) define('EP_THEMEPATH', get_template_directory_uri());
if(!defined('EPCL_ABSPATH')) define('EPCL_ABSPATH', get_template_directory());
if(!defined('EPCL_THEMEPATH')) define('EPCL_THEMEPATH', get_template_directory_uri());
if(!defined('EPCL_THEMEPREFIX')) define('EPCL_THEMEPREFIX', 'epcl');
if(!defined('EPCL_FRAMEWORK_VAR')) define('EPCL_FRAMEWORK_VAR', 'epcl_theme');

/* Admin */

require_once(EP_ABSPATH.'/functions/admin/config.php'); // Initializes the framework
require_once(EP_ABSPATH.'/functions/admin/custom-functions.php'); // Just some modifications to the panel

/* Funciones Principales */

require_once(EP_ABSPATH.'/functions/custom-post-types.php');
require_once(EP_ABSPATH.'/functions/enqueue-scripts.php');
require_once(EP_ABSPATH.'/functions/shortcodes.php');

/* Campos Personalizados */

//add_filter('acf/settings/show_admin', '__return_false');
require_once(EP_ABSPATH.'/functions/custom-fields.php');
add_action('admin_head', 'ep_custom_admin_css');

function ep_custom_admin_css() {
  echo '
  <style type="text/css">
    .galeria-small .acf-gallery{ height: 385px !important; }
  </style>';
}

/* Botones Adicionales Editor Teeny */

add_filter( 'teeny_mce_buttons', 'my_editor_buttons', 10, 2 );
function my_editor_buttons( $buttons, $editor_id ){
	$buttons[] = 'removeformat';
    return $buttons;
}

/* Theme Setup */

if(!isset($content_width)) $content_width = 585; // oembed width

function theme_setup(){

	/* Thumbs */

	if(function_exists('add_theme_support')){
		add_theme_support('post-thumbnails');
		//add_theme_support('post-formats', array( 'video' ));
		//add_theme_support('automatic-feed-links');

		add_image_size('admin-thumb', 100, 100, false);
		add_image_size('slider-home', 1920, 1080, true);

		add_image_size('proyecto-thumb', 480, 760, true);
		add_image_size('proyecto-logo', 500, 150, false);
		add_image_size('proyecto-int', 1920, 1080, false);

		add_image_size('proyecto-galeria-thumb', 320, 320, true);
		add_image_size('planta-thumb', 700, 750, false);

		add_image_size('noticia', 700, 600, true);
		add_image_size('noticia-blog', 700, 380, true);
		add_image_size('noticia-int', 700, 450, false);
		add_image_size('noticia-sidebar', 200, 200, true);

		add_image_size('mailing-galeria', 600, 338, true);

		add_image_size('ep-large', 1600, 1200, false); // Required on portfolio lightbox

	}

	/* Menus */

	register_nav_menus(array(
		'header' => __('Menu Principal', 'epcl_theme'),
		'header2' => __('Menu Secundario', 'epcl_theme')
	));

	/* Sidebars */

	require_once(EPCL_ABSPATH.'/functions/sidebars.php');

}
add_action('after_setup_theme', 'theme_setup');

/* Columnas Globales */

add_filter('manage_posts_columns', 'posts_columns', 5);

function posts_columns($defaults){
    $defaults['imagen'] = __('Imagen', 'epcl_theme');
    return $defaults;
}

/* Proyectos */

add_filter( 'manage_edit-proyectos_columns', 'custom_proyectos_columns' ) ;
function custom_proyectos_columns( $columns ) {
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Title' ),
		'estado' => __( 'Estado' ),
		'imagen' => __( 'Imagen' ),
		'date' => __( 'Date' )
	);
	return $columns;
}

/* Modelos con Proyecto */

add_filter( 'manage_edit-modelos_columns', 'custom_modelos_columns' ) ;
function custom_modelos_columns( $columns ) {
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Title' ),
		'proyecto' => __( 'Proyecto' ),
		'tipologia' => __( 'Tipología URL' ),
		'n_depto' => __( 'Nº de Departamentos' ),
		'm2' => __( 'Superficie' ),
		'imagen' => __( 'Imagen' ),
		'date' => __( 'Date' )
	);
	return $columns;
}

add_filter( 'manage_edit-obras-ejecutadas_columns', 'custom_obras_ejecutadas_columns' ) ;
function custom_obras_ejecutadas_columns( $columns ) {
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Title' ),
		'obra' => __( 'Nº de Obra' ),
		'date' => __( 'Date' )
	);
	return $columns;
}

add_action( 'manage_posts_custom_column' , 'custom_columns', 10, 2 );
function custom_columns( $column_name, $post_id ) {
	global $post;
	switch( $column_name ) {
		case 'menu_order':
			$order = $post->menu_order;
			echo $order;
		break;
		case 'proyecto' :
			$id = get_post_meta( $post_id, 'proyecto', true );
			if ( empty( $id ) )
				echo 'Sin Asignar' ;
			else
				echo '<a href="'.admin_url().'post.php?post='.$id.'&action=edit" title="Editar Proyecto">'.get_the_title($id).'</a>';
        break;
        
        case 'obra' :
			$obra = get_post_meta( $post_id, 'numero_obra', true );
			if ( empty( $obra ) )
				echo '' ;
			else
				echo $obra;
		break;

		case 'estado' :
			$estado = get_post_meta( $post_id, 'estado', true );
			if ( !$estado )
				echo 'Sin Asignar' ;
			else
				echo $estado;
		break;

		case 'imagen':
			the_post_thumbnail('admin-thumb');
		break;

		default:
		break;
	}
}

/* Columnas Ordenables */

add_filter( 'manage_edit-proyectos_sortable_columns', 'proyectos_sortable_columns' );
function proyectos_sortable_columns( $columns ) {

	$columns['estado'] = 'estado';

	return $columns;
}

add_filter( 'manage_edit-modelos_sortable_columns', 'modelos_sortable_columns' );
function modelos_sortable_columns( $columns ) {

	$columns['proyecto'] = 'proyecto';
	$columns['tipologia'] = 'tipologia';

	return $columns;
}

add_filter( 'manage_edit-obras-ejecutadas_sortable_columns', 'obras_ejecutadas_sortable_columns' );
function obras_ejecutadas_sortable_columns( $columns ) {

	$columns['obra'] = 'obra';

	return $columns;
}

add_action( 'pre_get_posts', 'my_slice_orderby' );
function my_slice_orderby( $query ) {
    if( ! is_admin() )
        return;

    $orderby = $query->get( 'orderby');

	if( 'estado' == $orderby ) {
        $query->set('meta_key', 'estado');
        $query->set('orderby', 'meta_value');
    }

	if( 'proyecto' == $orderby ) {
        $query->set('meta_key', 'proyecto');
        $query->set('orderby', 'meta_value');
    }

    if( 'obra' == $orderby ) {
        $query->set('meta_key', 'numero_obra');
        $query->set('orderby', 'meta_value');
    }


}

/* Select box personalizados wp-admin */

add_action( 'restrict_manage_posts', 'custom_admin_posts_filter' );
function custom_admin_posts_filter(){
    $type = 'post';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }
	// Estados proyecto
    if( $type == 'proyectos' ){

        $values = array(
            'venta' => 'En Venta',
            'vendido_int' => 'Vendido',
			'vendido' => 'Histórico',
            'futuro' => 'Futuro',
        );
		$estado = isset($_GET['estado']) ? $_GET['estado'] : '';
        ?>
        <select name="estado">
            <option value="">Filtrar por Estado</option>
			<?php foreach ($values as $value => $label): ?>
            	<option value="<?php echo $value; ?>" <?php if($estado == $value) echo 'selected'; ?>><?php echo $label; ?></option>
            <?php endforeach; ?>
        </select>
        <?php
    }

	// Proyectos por Modelo
	 if( $type == 'modelos' ){

		 $args = array(
			'posts_per_page' => -1,
			'post_type' => 'proyectos',
			'order' => 'ASC',
			'suppress_filters' => false
		);
		$proyectos = get_posts($args);

		$values = array();
		foreach($proyectos as $p){
			$values[$p->ID] = $p->post_title;
		}
		$id_proyecto = isset($_GET['proyecto']) ? $_GET['proyecto'] : '';
        ?>
        <select name="proyecto">
            <option value="">Filtrar por Proyecto</option>
			<?php foreach ($values as $value => $label): ?>
            	<option value="<?php echo $value; ?>" <?php if($id_proyecto == $value) echo 'selected'; ?>><?php echo $label; ?></option>
            <?php endforeach; ?>
        </select>
        <?php
    }
}
add_filter( 'parse_query', 'custom_parse_posts_filter' );
function custom_parse_posts_filter( $query ){
    global $pagenow;

    $type = 'post';

    if( isset($_GET['post_type']) ) {
        $type = $_GET['post_type'];
    }

	// Select proyectos

    if ( $type == 'proyectos' && is_admin() && $pagenow =='edit.php' && isset($_GET['estado']) && $_GET['estado'] != '' && $query->is_main_query()) {
        $query->query_vars['meta_key'] = 'estado';
        $query->query_vars['meta_value'] = $_GET['estado'];
    }

	// Select modelos

    if ( $type == 'modelos' && is_admin() && $pagenow =='edit.php' && isset($_GET['proyecto']) && $_GET['proyecto'] != '' && $query->is_main_query()) {
        $query->query_vars['meta_key'] = 'proyecto';
        $query->query_vars['meta_value'] = $_GET['proyecto'];
    }

}

/* Custom Excerpt */

function new_excerpt_more($more){
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

function custom_excerpt_length($length){
    return 25;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999);

function small_excerpt_length($length){
    return 15;
}

/* Custom Pagination */

function ep_pagination($query = NULL){
	global $wp_query;
	if($query) $wp_query = $query;
?>
<!-- start: .paginacion -->
<div class="paginacion section">
	<?php
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace( $big, '%#%', esc_url(get_pagenum_link($big))),
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
		//'show_all' => true,
        'prev_text' => 'Anterior',
        'next_text' => 'Siguiente',
    ));
    ?>
</div>
<!-- end: .paginacion -->
<?php
}

/* Custom Gallery */

remove_shortcode('gallery', 'gallery_shortcode');
add_shortcode('gallery', 'ep_gallery_shortcode');

function ep_gallery_shortcode($atts){
	global $post;
	 if(!empty($atts['ids'])){
        if(empty( $atts['orderby']))
            $atts['orderby'] = 'post__in';

	    $atts['include'] = $atts['ids'];
    }
	extract(shortcode_atts(array(
       'orderby' => 'menu_order ASC, ID ASC',
        'include' => '',
        'id' => $post->ID,
        'columns' => 3,
    ), $atts));

	$args = array(
        'post_type' => 'attachment',
        'post_status' => 'inherit',
        'post_mime_type' => 'image',
        'orderby' => $orderby,
		'suppress_filters' => false
    );

    if(!empty($include)) $args['include'] = $include;
    else{ $args['post_parent'] = $id; $args['numberposts'] = -1; }

    $images = get_posts($args);
	$class = $wrapper_class = $output = '';
	switch($columns){
		case 2: $class = 'grid-50 tablet-grid-33 mobile-grid-50'; break;
		case 3: $class = 'grid-33 tablet-grid-33 mobile-grid-50'; break;
		case 4: $class = 'grid-25 tablet-grid-33 mobile-grid-50'; break;
		case 5: $class = 'grid-20 tablet-grid-33 mobile-grid-50'; break;
		case 6: $class = 'grid-20 tablet-grid-33 mobile-grid-50'; break;
		case 7: $class = 'grid-15 tablet-grid-33 mobile-grid-50'; break;
		case 8: $class = 'grid-10 tablet-grid-33 mobile-grid-50'; break;
		case 9: $class = 'grid-10 tablet-grid-33 mobile-grid-50'; break;
	}
	$thumb = 'proyecto-galeria-thumb';
	$output .= '<ul class="ep-gallery columns-'.$columns.' grid-container grid-parent">';
     	$count = 0;
		foreach($images as $image){
			$caption = $image->post_excerpt;
			$description = $image->post_content;
			if($description == '') $description = $image->post_title;
			$url = wp_get_attachment_image_src($image->ID, 'ep-large');
			$url_thumb = wp_get_attachment_image_src($image->ID, $thumb);
			$output .= '<li class="'.$class.'">
					<a href="'.$url[0].'" class="lightbox hover-effect" data-caption="'.$caption.'"><img src="'.$url_thumb[0].'"><i class="ep-icon fa fa-expand"></i></a>
				 </li>';
			$count++;
		}
	$output .= '</ul><div class="clear"></div>';
	return $output;
}

function remove_gallery($content) {
	add_shortcode('gallery', '__return_false');
	return $content;
}

/* Templates de Mailing */

require_once(EP_ABSPATH.'/functions/mailing/partials.php');
require_once(EP_ABSPATH.'/functions/mailing/respuesta.php'); // Contacto, inversiones, denuncias
require_once(EP_ABSPATH.'/functions/mailing/cotizacion.php'); // Proyecto y Modelos
require_once(EP_ABSPATH.'/functions/mailing/cliente.php'); // Respuestas a cliente (DESCO)

/* Ajax Contact */

function ajax_contact() {
	if(!empty($_POST) && wp_verify_nonce( $_REQUEST['nonce'], "contacto")) {
		global $epcl_theme;

		$guardado = 0;

		$from_email = 'noresponder@desco.cl';

		$title = sanitize_text_field($_POST['form_title']);

		$nombre = sanitize_text_field($_POST['nombre']);
		$email = sanitize_text_field($_POST['email']);
		$telefono = sanitize_text_field($_POST['telefono']);
		$motivo = sanitize_text_field($_POST['motivo']);
		$comentario = sanitize_text_field($_POST['comentario']);
		$recaptcha_response = sanitize_text_field($_POST['g-recaptcha-response']);

		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$recaptcha_data = array(
			'secret' => '6LeH8DoUAAAAACNlg9KdR_ZSg0a4ejhNNUAv7sul',
            'response' => $recaptcha_response,
            'remoteip' => $_SERVER['REMOTE_ADDR']
		);
		$options = array(
			'http' => array (
                'method' => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => http_build_query($recaptcha_data)
			)
		);
        $context  = stream_context_create($options);
		$verify = file_get_contents($url, false, $context);
        $captcha_success = json_decode($verify);

        // if( $captcha_success->success == false ){
		// 	exit('bot');
        // }
        
        
        // $ch = curl_init();

        // curl_setopt_array($ch, [
        //     CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
        //     CURLOPT_POST => true,
        //     CURLOPT_POSTFIELDS => [
        //         'secret' => '6LeH8DoUAAAAACNlg9KdR_ZSg0a4ejhNNUAv7sul',
        //         'response' => $recaptcha_response,
        //         'remoteip' => $_SERVER['REMOTE_ADDR']
        //     ],
        //     CURLOPT_RETURNTRANSFER => true
        // ]);
        
        // $output = curl_exec($ch);
        // curl_close($ch);
        
        // $json = json_decode($output);


        // if ($json->success == false) {
        //     exit('bot');
        // }

		// Solo proyectos
		$proyecto = sanitize_text_field($_POST['proyecto']);
		$proyecto_id = sanitize_text_field($_POST['proyecto_id']);
		$modelo = sanitize_text_field($_POST['modelo']);
		$modelo_id = sanitize_text_field($_POST['modelo_id']);
		$programa = sanitize_text_field($_POST['programa']);
		$rut = sanitize_text_field($_POST['rut']);


		/* Contacto General */
		if($title == 'Contacto'){

			$data = (object) array(
				'title' => $title,
				'posted_data' => array(
					'Nombre' => $nombre,
					'Email' => $email,
					'Teléfono' => $telefono,
					'Motivo' => $motivo,
					'Comentario' => $comentario
				)
			);

			$html = generar_email_respuesta($data->posted_data);
			$from_email = $epcl_theme['email'];

			$recipient = $email . ", " ; //note the comma
			$subject = $nombre.' gracias por contactarnos!';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= "Content-type: text/html; charset=UTF-8" . "\r\n";
			$headers .= "From: Desco Inmobiliaria <$from_email>\r\n";
			$headers .= "X-Sender: <$from_email>\r\n";
			$headers .= "X-Mailer: PHP\r\n";
			$headers .= "X-Priority: 3\r\n";

			if(!wp_mail($recipient, $subject, $html, $headers))
				echo 'error';

		}

		/* Inversionistas */
		if($title == 'Inversionistas'){

			$data = (object) array(
				'title' => $title,
				'posted_data' => array(
					'Nombre' => $nombre,
					'Email' => $email,
					'Teléfono' => $telefono,
					'Motivo' => $motivo,
					'Comentario' => $comentario
				)
			);

			$html = generar_email_respuesta($data->posted_data);
			$from_email = $epcl_theme['email'];

			$recipient = $email . ", " ; //note the comma
			$subject = $nombre.' gracias por contactarnos!';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= "Content-type: text/html; charset=UTF-8" . "\r\n";
			$headers .= "From: Desco Inmobiliaria <$from_email>\r\n";
			$headers .= "X-Sender: <$from_email>\r\n";
			$headers .= "X-Mailer: PHP\r\n";
			$headers .= "X-Priority: 3\r\n";

			if(!wp_mail($recipient, $subject, $html, $headers))
				echo 'error';

		}

		/* Denuncias */
		if($title == 'Denuncias'){

			$data = (object) array(
				'title' => $title,
				'posted_data' => array(
					'Nombre' => $nombre,
					'Email' => $email,
					'Comentario' => $comentario
				)
			);

			$html = generar_email_respuesta($data->posted_data);
			$from_email = $epcl_theme['email'];

			$recipient = $email . ", " ; //note the comma
			$subject = $nombre.' hemos recibido tu denuncia';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= "Content-type: text/html; charset=UTF-8" . "\r\n";
			$headers .= "From: Desco Inmobiliaria <$from_email>\r\n";
			$headers .= "X-Sender: <$from_email>\r\n";
			$headers .= "X-Mailer: PHP\r\n";
			$headers .= "X-Priority: 3\r\n";

			if(!wp_mail($recipient, $subject, $html, $headers))
				echo 'error';

		}

		/* Email Solicitar Información Proyecto */
		if($title == 'Proyecto'){

			$data = (object) array(
				'title' => $title,
				'posted_data' => array(
					'Proyecto' => $proyecto,
					'Modelo' => $modelo,
					'Nombre' => $nombre,
					'Email' => $email,
					'Teléfono' => $telefono,
					'Rut' => $rut,
					'Comentario' => $comentario
				)
			);

			$html = generar_email_cotizacion($data->posted_data, $proyecto_id);
			if(get_field('form_email', $proyecto_id)){
				$from_email = get_field('form_email', $proyecto_id);
			}

			$recipient = $email . ", " ; //note the comma
			$subject = $nombre.' gracias por cotizar!';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= "Content-type: text/html; charset=UTF-8" . "\r\n";
			$headers .= "From: Desco Inmobiliaria <$from_email>\r\n";
			$headers .= "X-Sender: <$from_email>\r\n";
			$headers .= "X-Mailer: PHP\r\n";
			$headers .= "X-Priority: 3\r\n";

			if(!wp_mail($recipient, $subject, $html, $headers))
				echo 'error';

		}

		/* Email Cotizar Planta */
		if($title == 'Cotizar Modelo'){

			$data = (object) array(
				'title' => $title,
				'posted_data' => array(
					'Proyecto' => $proyecto,
					'Modelo' => $modelo,
					'Nombre' => $nombre,
					'Email' => $email,
					'Teléfono' => $telefono,
					'Rut' => $rut,
					'Comentario' => $comentario
				)
			);

			$html = generar_email_cotizacion($data->posted_data, $proyecto_id);
			$from_email = get_field('form_email', $proyecto_id);

			$recipient = $email . ", " ; //note the comma
			$subject = 'Cotización '.$proyecto.' - Desco Inmobiliaria';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= "Content-type: text/html; charset=UTF-8" . "\r\n";
			$headers .= "From: Desco Inmobiliaria <$from_email>\r\n";
			$headers .= "X-Sender: <$from_email>\r\n";
			$headers .= "X-Mailer: PHP\r\n";
			$headers .= "X-Priority: 3\r\n";

			if(!wp_mail($recipient, $subject, $html, $headers))
				echo 'error';

		}


		require_once(ABSPATH  . 'wp-content/plugins/contact-form-7-to-database-extension/CFDBFormIterator.php');
		$plugin = new CF7DBPlugin();
		$guardado = $plugin->saveFormData($data);

		if($guardado){

			$admin_email = get_option('admin_email');
			$email_from = $email;
			//$recipient = $admin_email . ", " ; //note the comma
			$blogname = get_option('blogname');
			$blogname = 'Desco Inmobiliaria';

			/* Destinatarios */

			$recipient = $epcl_theme['email'] . ", " ; //note the comma
			$subject = "Contacto desde formulario ".$blogname;

			if($title == 'Contacto'){
				$recipient = $epcl_theme['email'];
				$html = generar_email_general_cliente($data->posted_data);
			}

			if($title == 'Inversionistas'){
				$subject = "Contacto desde formulario Inversiones ".$blogname;
				$recipient = $epcl_theme['email_inversiones'];
				$html = generar_email_general_cliente($data->posted_data);
			}

			if($title == 'Denuncias'){
				$recipient = $epcl_theme['email'];
				$html = generar_email_general_cliente($data->posted_data, 'Denuncia');
			}

			if($title == 'Proyecto'){
				$recipient = get_field('form_email', $proyecto_id);
				$cc = get_field('form_email_cc', $proyecto_id);
				$cco = get_field('form_email_cco', $proyecto_id);
				$gleads = get_field('form_email_gleads', $proyecto_id);
				$subject = "Cotización - ".$proyecto;
				$html = generar_email_general_cliente($data->posted_data, 'Cotización');
			}

			if($title == 'Cotizar Modelo'){
				$cco = '';
				$recipient = get_field('form_email', $proyecto_id);
				$cc = get_field('form_email_cc', $proyecto_id);
				$cco = get_field('form_email_cco', $proyecto_id);
				if( $epcl_theme['email_ventas_cco'] ) $cco .= ", ".$epcl_theme['email_ventas_cco'];
				$subject = "Cotización - ".$proyecto;
				$gleads = get_field('form_email_gleads', $proyecto_id);
				$html = generar_email_general_cliente($data->posted_data, 'Cotización');

				//$html = generar_email_cotizar_simonetti($data->posted_data, $proyecto_id, $modelo_id, $numero_depto);
			}

			if($gleads){
				$message = "Inicio de contenido:\n\n";
				$message .= "Nombre: $nombre\n";
				$message .= "Email: $email\n";
				$message .= "Telefono: $telefono\n";
				$message .= "Rut: $rut\n";
				$message .= "Proyecto: $proyecto\n";
				$message .= "Modelo: $modelo\n";
				$message .= "Programa: $programa\n";
				$message .= "Mensaje: $comentario\n\n";
				$message .= "Fin de contenido";

				$headers  = 'MIME-Version: 1.0' . "\r\n";
				//$headers .= "Content-type: text/html; charset=UTF-8" . "\r\n";
				$headers .= "From: Desco Inmobiliaria <cotizaciones@desco.cl>\r\n";
				$headers .= "X-Sender: <cotizaciones@desco.cl>\r\n";
				$headers .= "X-Mailer: PHP\r\n";
				$headers .= "X-Priority: 3\r\n";
				$headers .= "Return-Path: <cotizaciones@desco.cl>\r\n";
				$headers .= "Reply-To: <cotizaciones@desco.cl>\r\n";

				if(!wp_mail($gleads, $subject, $message, $headers))
					echo 'error';
			}

			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= "Content-type: text/html; charset=UTF-8" . "\r\n";
			$headers .= "From: Desco Inmobiliaria <noresponder@desco.cl>\r\n";
			$headers .= "X-Sender: <noresponder@desco.cl>\r\n";
			$headers .= "From: Desco Inmobiliaria <$email>\r\n";
			$headers .= "X-Sender: <$email>\r\n";
			$headers .= "X-Mailer: PHP\r\n";
			$headers .= "X-Priority: 3\r\n";
			$headers .= "Return-Path: <$email>\r\n";
			$headers .= "Reply-To: $nombre <$email>\r\n";
			if($cc) $headers .= "Cc: $cc" . "\r\n";
			if($cco) $headers .= "Bcc: $cco" . "\r\n";

			//$recipient = 'javierencalada20@gmail.com' . ',';

			if(!wp_mail($recipient, $subject, $html, $headers))
				echo 'error';
			else
				echo 'success';

		}else if($title != 'Compartir'){
			echo 'error';
		}

	}else // verificacion nonce y post
		echo 'error';
	die();
}
add_action('wp_ajax_ajax_contact', 'ajax_contact');
add_action('wp_ajax_nopriv_ajax_contact', 'ajax_contact');


/* Fixes */

add_filter('the_content', 'shortcode_empty_paragraph_fix');

function shortcode_empty_paragraph_fix($content){
	$array = array (
		'<p>[' => '[',
		']</p>' => ']',
		']<br />' => ']'
	);
	$content = strtr($content, $array);
	return $content;
}

/* Buscador Faq */

function ajax_faq() {
	if(!empty($_GET) && wp_verify_nonce( $_REQUEST['nonce'], "faq")) {

		$termino = sanitize_text_field($_GET['q']);

		$args = array(
			'posts_per_page' => -1,
			'post_type' => 'faq',
			'order' => 'ASC',
			's' => $termino
		);
		$resultados_normales = new WP_Query($args);

		$args = array(
			'posts_per_page' => -1,
			'post_type' => 'faq',
			'order' => 'ASC',
			'meta_query'=> array(
				array(
					'key' => 'palabras_claves',
					'compare' => 'LIKE',
					'value' => $termino,
				)
			)
		);
		$resultados_claves = new WP_Query($args);

		$resultados = new WP_Query();
		$resultados->posts = array_unique( array_merge( $resultados_normales->posts, $resultados_claves->posts ), SORT_REGULAR );
		$resultados->post_count = count( $resultados->posts );

		$array = array();
		while ($resultados->have_posts() ): $resultados->the_post();
			$array[] = array('titulo' => get_the_title(), 'url' => get_permalink() );
		endwhile;

		echo json_encode($array);

	}else
		echo 'error';
	die();
}

add_action('wp_ajax_ajax_faq', 'ajax_faq');
add_action('wp_ajax_nopriv_ajax_faq', 'ajax_faq');

/* Funciones basicas */

function set_active($current, $var){
	if($current == $var){
		echo 'class="current"';
	}
}

function youtube_id($url){
	preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
	return $matches[1];
}

// Deshabilitar XML y RSS

add_filter('xmlrpc_enabled', '__return_false');
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');

add_action('do_feed', 'disabler_kill_rss', 1);
add_action('do_feed_rdf', 'disabler_kill_rss', 1);
add_action('do_feed_rss', 'disabler_kill_rss', 1);
add_action('do_feed_rss2', 'disabler_kill_rss', 1);
add_action('do_feed_atom', 'disabler_kill_rss', 1);
if( !function_exists('disabler_kill_rss') ) {
	function disabler_kill_rss(){
		wp_die( _e("No feeds available.", 'ippy_dis') );
	}
}

//Remove feed link from header
remove_action( 'wp_head', 'feed_links_extra', 3 ); //Extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links', 2 ); // General feeds: Post and Comment Feed

/*
* Resize images dynamically using wp built in functions
* Victor Teixeira
*
* php 5.2+
*
* Exemplo de uso:
*
* <?php
* $thumb = get_post_thumbnail_id();
* $image = vt_resize($thumb, '', 140, 110, true);
* ?>
* <img src="<?php echo $image[url]; ?>" width="<?php echo $image[width]; ?>" height="<?php echo $image[height]; ?>" />
*
* @param int $attach_id
* @param string $img_url
* @param int $width
* @param int $height
* @param bool $crop
* @return array
*/
if(!function_exists('vt_resize')){
    function vt_resize($attach_id = null, $img_url = null, $width, $height, $crop = false){
    if($attach_id){
        // this is an attachment, so we have the ID
        $image_src = wp_get_attachment_image_src($attach_id, 'full');
        $file_path = get_attached_file($attach_id);
    } elseif($img_url){
        // this is not an attachment, let's use the image url
        $file_path = parse_url($img_url);
        $file_path = $_SERVER['DOCUMENT_ROOT'].$file_path['path'];
        // Look for Multisite Path
        if(file_exists($file_path) === false){
            global $blog_id;
            $file_path = parse_url($img_url);
            if(preg_match('/files/', $file_path['path'])){
                $path = explode('/', $file_path['path']);
                foreach($path as $k => $v){
                    if($v == 'files'){
                        $path[$k-1] = 'wp-content/blogs.dir/'.$blog_id;
                    }
                }
                $path = implode('/', $path);
            }
            $file_path = $_SERVER['DOCUMENT_ROOT'].$path;
        }
        //$file_path = ltrim( $file_path['path'], '/' );
        //$file_path = rtrim( ABSPATH, '/' ).$file_path['path'];
        $orig_size = getimagesize($file_path);
        $image_src[0] = $img_url;
        $image_src[1] = $orig_size[0];
        $image_src[2] = $orig_size[1];
    }
    $file_info = pathinfo($file_path);
    // check if file exists
    $base_file = $file_info['dirname'].'/'.$file_info['filename'].'.'.$file_info['extension'];
    if(!file_exists($base_file))
    return;
    $extension = '.'. $file_info['extension'];
    // the image path without the extension
    $no_ext_path = $file_info['dirname'].'/'.$file_info['filename'];
    $cropped_img_path = $no_ext_path.'-'.$width.'x'.$height.$extension;
    // checking if the file size is larger than the target size
    // if it is smaller or the same size, stop right here and return
    if($image_src[1] > $width){
        // the file is larger, check if the resized version already exists (for $crop = true but will also work for $crop = false if the sizes match)
        if(file_exists($cropped_img_path)){
            $cropped_img_url = str_replace(basename($image_src[0]), basename($cropped_img_path), $image_src[0]);
            $vt_image = array(
                'url'   => $cropped_img_url,
                'width' => $width,
                'height'    => $height
            );
            return $vt_image;
        }
        // $crop = false or no height set
        if($crop == false OR !$height){
            // calculate the size proportionaly
            $proportional_size = wp_constrain_dimensions($image_src[1], $image_src[2], $width, $height);
            $resized_img_path = $no_ext_path.'-'.$proportional_size[0].'x'.$proportional_size[1].$extension;
            // checking if the file already exists
            if(file_exists($resized_img_path)){
                $resized_img_url = str_replace(basename($image_src[0]), basename($resized_img_path), $image_src[0]);
                $vt_image = array(
                    'url'   => $resized_img_url,
                    'width' => $proportional_size[0],
                    'height'    => $proportional_size[1]
                );
                return $vt_image;
            }
        }
        // check if image width is smaller than set width
        $img_size = getimagesize($file_path);
        if($img_size[0] <= $width) $width = $img_size[0];
            // Check if GD Library installed
            if(!function_exists('imagecreatetruecolor')){
                echo 'GD Library Error: imagecreatetruecolor does not exist - please contact your webhost and ask them to install the GD library';
                return;
            }
            // no cache files - let's finally resize it
            $new_img_path = image_resize($file_path, $width, $height, $crop);
            $new_img_size = getimagesize($new_img_path);
            $new_img = str_replace(basename($image_src[0]), basename($new_img_path), $image_src[0]);
            // resized output
            $vt_image = array(
                'url'   => $new_img,
                'width' => $new_img_size[0],
                'height'    => $new_img_size[1]
            );
            return $vt_image;
        }
        // default output - without resizing
        $vt_image = array(
            'url'   => $image_src[0],
            'width' => $width,
            'height'    => $height
        );
        return $vt_image;
    }
}

// function jquery_to_footer(){
//     if ( !is_admin() ){
//         wp_deregister_script('jquery');
//         wp_register_script('jquery', site_url().'/wp-includes/js/jquery/jquery.js', FALSE, '1.12.0', TRUE);
//         wp_enqueue_script('jquery');
//     }
// }
// add_action('init', 'jquery_to_footer');

function a_pr_enqueues() {
	wp_register_script('a-pr-js', get_template_directory_uri() . '/js/main.js', false, null, true);
	wp_enqueue_script('a-pr-js');
}
add_action('wp_enqueue_scripts', 'a_pr_enqueues', 100);

function add_slug_body_class( $classes ) {
    global $post;
    if ( isset( $post ) ) {
    $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

?>
