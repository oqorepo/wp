<?php get_header(); ?>
<!-- start: #pagina-404 -->
<main id="pagina-404" class="page" role="main">

    <div class="top fullheight">
    	<div class="fullimg cover" style="background-image: url('<?php echo EP_THEMEPATH; ?>/images/bg-404.jpg'); "></div>
        <div class="middle textcenter">
            <div class="grid-container grid-small" data-aos="fade-down">
                <h1 class="titulo large white">ERROR 404</h1>
                <h2 class="titulo medium white">LO SENTIMOS, EL CONTENIDO QUE BUSCAS NO ESTÁ DISPONIBLE.</h2>
                <div class="botones">
                    <a href="<?php echo site_url()?>" class="button outline">Volver al Inicio</a>
                </div>
            </div>
        </div>
    </div>

</main>
<!-- end: #pagina-404 -->
<?php get_footer(); ?>
