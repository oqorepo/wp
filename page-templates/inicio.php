<?php /* Template Name: Inicio */ ?>
<?php get_header(); ?>

<!-- start: #home -->
<main id="home" class="main" role="main">
	<div class="fullpage-section">
		<?php
        $args = array(
            'posts_per_page' => -1,
            'post_type' => 'slider',
            'suppress_filters' => false,
			'orderby' => 'date'
        );
        $sliders = get_posts($args);
        if(!empty($sliders)):
        ?>
            <div id="masterslider" class="master-slider ms-skin-default" style="height: 100vh;">
                <?php foreach($sliders as $post): setup_postdata($post); ?>
                    <?php
						$estado = get_field('estado');
                        $titulo = get_field('titulo');
                        $bajada = get_field('bajada');
						$destino = get_field('destino');
						$url = get_field('url');
						$texto_boton = get_field('texto_boton');
						if(!$segundos) $segundos = 5;
                        $imagen = get_field('imagen');
                        $url_imagen = $imagen['sizes']['ep-large'];
                        $video = get_field('video');
                        $youtube_id = '';
                        if($video){
                            $youtube_id = youtube_id($video);
                        }
						$texto_boton_defecto = 'VER PROYECTO';
						if($texto_boton){
							$texto_boton_defecto = $texto_boton;
						}
                    ?>
                    <!-- start: .ms-slide -->
                    <div class="ms-slide <?php if($youtube_id) echo 'has-video'; ?>" data-delay="<?php echo $segundos; ?>">
                        <!--<img src="<?php echo EP_THEMEPATH; ?>/images/blank.gif" data-src="<?php echo $url_imagen; ?>">-->
                        <div class="fullimg cover" style=" background-image: url(<?php echo $url_imagen; ?>);"></div>
                        <div class="video" data-id="<?php echo $youtube_id; ?>" id="video-<?php echo $youtube_id; ?>"></div>
                        <?php if($url): ?>
                        	<a href="<?php echo $url; ?>" target="<?php echo $destino; ?>" class="full-link"></a>
                        <?php endif; ?>
                        <div class="ms-layer ms-caption" data-type="text" data-effect="bottom(0)" data-duration="900" data-delay="300" data-ease="easeOutExpo">
                        	<?php if($estado): ?>
                                <div class="estado"><?php echo $estado; ?></div>
                            <?php endif; ?>
                            <h3 class="titulo large"><?php echo $titulo; ?></h3>
                            <p class="titulo medium"><?php echo $bajada; ?></p>
                            <?php if($url): ?>
                                <a href="<?php echo $url; ?>" target="<?php echo $destino; ?>" class="button outline"><?php echo $texto_boton_defecto; ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- end: .ms-slide -->
                <?php endforeach; ?>
                <a href="#somos" class="next-section hide-on-mobile">Use scroll para navegar<img src="<?php echo EP_THEMEPATH; ?>/images/scrolldown.png"></a>
            </div>
        <?php endif; wp_reset_postdata(); ?>
    </div>

	<?php
    $titulo_somos = get_field('titulo_somos');
    $texto_somos = get_field('texto_somos');
    $video_somos = get_field('video_somos');
    $imagen_somos = get_field('imagen_somos');
    $url_imagen = $imagen_somos['sizes']['proyecto-thumb'];
    $youtube_id = '';
    if($video_somos){
        $youtube_id = youtube_id($video_somos);
    }
    ?>

    <?php if($texto_somos != ''): // deshabilitado ?>
<?php /*?>        <!-- start: #somos -->
        <section id="somos" class="fullheight fullscreen fullpage-section <?php if($youtube_id) echo 'has-video'; ?>">
            <div class="fullimg cover parallax" style="background-image: url(<?php echo $url_imagen; ?>);"></div>
            <?php if($youtube_id != ''): ?>
                <div class="video" data-id="<?php echo $youtube_id; ?>" id="video-<?php echo $youtube_id; ?>"></div>
            <?php endif; ?>
            <div class="middle section">
                <div class="grid-container grid-large">
                    <h2 class="titulo medium green"><?php echo $titulo_somos; ?></h2>
                </div>
                <div class="grid-container" data-aos="fade">

                    <div class="resumen"><h1><span class="comilla">“</span><?php echo $texto_somos; ?><span class="comilla right">„</span></h1></div>
                    <a href="<?php echo site_url(); ?>/somos" class="button green outline">CONOCE MÁS DE NOSOTROS</a>
                    <div class="clear"></div>
                </div>
            </div>
        </section>
        <!-- end: #somos --><?php */?>
    <?php endif; ?>


    <?php
    $args = array(
        'posts_per_page' => -1,
        'post_type' => 'proyectos',
        'post_status' => 'publish',
        'suppress_filters' => false,
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'meta_key' => 'estado',
        'meta_value' => 'venta'
    );
    $proyectos = get_posts($args);
    if(!empty($proyectos)):
    ?>
        <!-- start: #proyectos -->
        <section id="proyectos" class="fullpage-section" data-anchor="proyectos">
            <div class="grid-container grid-large">
                <h3 class="titulo medium section">#PROYECTOS</h3>
            </div>
            <?php if(!empty($proyectos)): ?>
                <div class="listado slick-slider">
                    <?php get_template_part('partials/loop-proyectos'); ?>
                </div>
            <?php endif; ?>
            <div class="clear"></div>
        </section>
        <!-- end: #proyectos -->
    <?php endif; wp_reset_postdata(); ?>

<!-- ACÁ VA SECCIÓN DE PROMOCIONES DESHABILITADA -->

    <?php
    $args = array('posts_per_page' => 6, 'post_type' => array('post', 'desco-medios'), 'suppress_filters' => false);
    $novedades = get_posts($args);
    if(!empty($novedades)):
    ?>
        <!-- start: #novedades -->
        <section id="novedades" class="section fullpage-section grid-container grid-medium">
            <h3 class="titulo medium">#NOTICIAS</h3>
            <?php if(!empty($novedades)): ?>
                <div class="listado" data-aos="fade-right">
                    <?php $i = 1; $c = 0; foreach($novedades as $post): setup_postdata($post); ?>
                        <?php if($i == 1): ?>
                            <div class="grid-45 grid-parent">
                                <article class="item large">
                                    <h4 class="titulo medium"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                    <div class="excerpt"><?php the_excerpt(); ?></div>
                                    <a href="<?php the_permalink(); ?>" class="button outline black">LEER MÁS</a>
                                </article>
                            </div>
                        <?php endif; ?>
                        <?php if($i == 2) echo '<div class="grid-55 grid-parent right">'; ?>
                        <?php if($i == 2): ?>
                                <?php
                                $img = get_the_post_thumbnail_url($post->ID, 'noticia');
                                ?>
                                <div class="grid-50 tablet-grid-50 np-mobile">
                                    <article class="item white cover <?php if(!$img) echo 'no-image'; ?>" style="background-image: url(<?php echo $img; ?>);">
                                        <div class="overlay"></div>
                                        <div class="caption">
                                            <h4 class="titulo medium border-effect"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                            <a href="<?php the_permalink(); ?>" class="button outline white">LEER MÁS</a>
                                        </div>
                                    </article>
                                </div>
                        <?php endif; ?>

                        <?php if($i == 3 || $i == 4): ?>
                            <article class="item grid-50 tablet-grid-50 small">
                                <h4 class="titulo small border-effect" style="margin: 0;"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            </article>
                        <?php endif; ?>

                        <?php if($i == 5) echo '<div class="clear"></div>'; ?>

                        <?php if($i == 5 || $i == 6): ?>
                                <?php
                                $img = get_the_post_thumbnail_url($post->ID, 'noticia');
                                ?>
                                <div class="grid-50 tablet-grid-50 <?php if($i == 6) echo 'grid-parent'; ?> np-mobile">
                                    <article class="item white cover <?php if($i == 5) echo 'no-image'; ?>" style="background-image: url(<?php echo $img; ?>);">
                                        <div class="overlay"></div>
                                        <div class="caption">
                                            <h4 class="titulo medium border-effect"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                            <a href="<?php the_permalink(); ?>" class="button outline white">LEER MÁS</a>
                                        </div>
                                    </article>
                                </div>
                        <?php endif; ?>

                        <?php if($i == 6) echo '</div>'; ?>
                    <?php $i++; $c++; endforeach; ?>
                </div>
            <?php endif; ?>
        </section>
        <!-- end: #novedades -->
    <?php endif; wp_reset_postdata(); ?>


</main>
<!-- end: #main -->

<?php get_footer(); ?>
