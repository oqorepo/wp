<?php /* Template Name: Inmobiliaria */ ?>
<?php get_header(); ?>

<!-- start: #pagina-inmobiliaria -->
<main id="pagina-inmobiliaria" class="page" role="main">
	<?php if(have_posts()): the_post(); ?>

        <?php
		// Info General
		$etiqueta = get_field('etiqueta');
		$titulo = get_field('titulo');
		$bajada = get_field('bajada');
		$imagen_superior = get_field('imagen_superior');
		$url_imagen_superior = $imagen_superior['sizes']['slider-home']
		?>
        <!-- start: .top -->
        <div class="top fullheight fullpage-section">
            <?php if($url_imagen_superior): ?>
                <div class="fullimg cover" style="background-image: url(<?php echo $url_imagen_superior; ?>);"></div>
            <?php endif; ?>
            <div class="middle aligntop" data-aos="fade-down">
                <div class="grid-container grid-small">
                	<?php if($etiqueta): ?>
                        <h3 class="etiqueta"><?php echo $etiqueta; ?></h3>
                    <?php endif; ?>
                    <?php if($titulo): ?>
                        <h1 class="titulo large white"><?php echo $titulo; ?></h1>
                    <?php endif; ?>
                    <?php if($bajada): ?>
                        <p class="titulo white"><?php echo $bajada; ?></p>
                    <?php endif; ?>
                    <div class="botones">
	                    <a href="#proyectos-ejecutados" class="button outline">PROYECTOS EJECUTADOS</a>
                        <a href="#proyectos" class="button outline">PROYECTOS EN VENTA</a>
                        <a href="<?php echo site_url(); ?>/inversionistas" class="button outline">INVERSIONISTAS</a>
                    </div>
                </div>
            </div>
            <a href="#proyectos-ejecutados" class="next-section hide-on-mobile">Use scroll para navegar<img src="<?php echo EP_THEMEPATH; ?>/images/scrolldown.png"></a>
        </div>
        <!-- end: .top -->

        <?php
		$args = array(
			'posts_per_page' => -1,
			'post_type' => 'proyectos-ejecutados',
			'suppress_filters' => false,
			'meta_key' => 'fecha',
            'orderby' => 'meta_value_num',
            'order' => 'DESC'
		);
        $proyectos = get_posts($args);
        if(!empty($proyectos)):
        ?>
            <!-- start: #obras -->
            <section id="obras" class="carrusel-proyectos section fullpage-section" style="padding-top: 30px;">
            	<div class="grid-container grid-medium">
                    <h3 class="titulo textcenter">#PROYECTOS EJECUTADOS</h3>

                    <div class="listav2">
                        <?php $i = 0; foreach($proyectos as $post): setup_postdata($post); ?>
                            <?php
                            $descripcion = get_field('descripcion');
                            $galeria = get_field('galeria');
                            if(empty($galeria)) continue;
                            $imagen = $galeria[0]['sizes']['proyecto-galeria-thumb'];
                            $class = ($i == 0) ? 'active' : '';
                            if($i == 4) $i = 0;
                            ?>
                            <div class="item grid-25 tablet-grid-50">
                                <div class="img cover lazy"  data-src="<?php echo $imagen; ?>"><div class="overlay"></div></div>
                                <div class="info">
                                    <h4 class="titulo usmall"><?php the_title(); ?></h4>
                                    <div class="borde"></div>
                                    <a href="<?php the_permalink(); ?>" class="button outline black lightbox-obras">VER MÁS</a>
                                </div>                              
                                <div class="clear"></div>
                            </div>
                            <?php
                            if($i == 3){
                                echo '<div class="clear"></div>';
                            }
                            ?>
                        <?php $i++; endforeach;  wp_reset_postdata(); ?>
                    </div>

                    <div class="clear"></div>
                </div>
            </section>
            <!-- end: #obras -->   
        <?php endif; wp_reset_postdata(); ?>

		<?php
		$args = array(
			'posts_per_page' => -1,
			'post_type' => 'proyectos',
			'suppress_filters' => false,
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'meta_key' => 'estado',
			'meta_value' => 'venta'
		);
        $proyectos = get_posts($args);
        if(!empty($proyectos)):
        ?>
            <!-- start: #proyectos -->
            <section id="proyectos" class="fullpage-section">
            	<div class="grid-container grid-large">
                    <h3 class="titulo medium section" style="padding-top: 30px;">#PROYECTOS EN VENTA</h3>
                </div>
                <?php if(!empty($proyectos)): ?>
                    <div class="listado slick-slider">
                        <?php get_template_part('partials/loop-proyectos'); ?>
                    </div>
                <?php endif; ?>
                <div class="clear"></div>
            </section>
            <!-- end: #proyectos -->
        <?php endif; wp_reset_postdata(); ?>

    <?php endif; ?>
</main>
<!-- end: #pagina-inmobiliaria -->

<?php get_footer(); ?>
