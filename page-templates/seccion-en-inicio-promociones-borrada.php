
    <?php
    $args = array(
        'posts_per_page' => -1,
        'post_type' => 'proyectos',
        'suppress_filters' => false,
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'posts_per_page' => 3,
        'meta_query' => array(
            array(
                'key'     => 'promociones',
                'value'   => '',
                'compare' => '!='
            )
        )
    );
    $promociones = get_posts($args);
    ?>

    <?php if( !empty($promociones) ):// deshabilitado ?>
      <!-- start: #promociones -->
        <section id="promociones" class="section fullpage-section grid-container grid-medium">
            <div class="middle">
                <h2 class="titulo medium">#PROMOCIONES</h2>
                <div class="listado" data-aos="fade-right">
                    <?php $i = 1; $c = 0; foreach($promociones as $post): setup_postdata($post); ?>
                        <?php
                            if(get_the_ID() != 54){
                                break;
                            }
                            $logo = get_field('logo_proyecto');
                            $url_logo_promocion = $logo['url'];
                            $promocion_logo = get_field('promocion_logo');
                            if( !empty($promocion_logo) ){
                                $url_logo_promocion = $promocion_logo['url'];
                            }
                        ?>
                        <?php if($i == 1): ?>
                            <div class="grid-45 grid-parent <?php the_ID(); ?>">
                                <article class="item large">
                                    <div class="logo">
                                        <a href="<?php the_permalink(); ?>#promociones" style="background-image: url('<?php echo $url_logo_promocion; ?>');"></a>
                                    </div>
                                    <div class="excerpt"><?php the_excerpt(); ?></div>
                                    <a href="<?php the_permalink(); ?>#promociones" class="button outline black">VER PROMO</a>
                                </article>
                            </div>
                        <?php endif; ?>
        
                        <?php if($i > 1): ?>
                            <?php
                            $img = get_the_post_thumbnail_url($post->ID, 'noticia');
                            ?>
                            <div class="grid-55 grid-parent right">
                                <article class="item">
                                    <div class="caption grid-40 tablet-grid-50">
                                        <div class="logo">
                                            <a href="<?php the_permalink(); ?>#promociones" style="background-image: url('<?php echo $url_logo_promocion; ?>');"></a>
                                        </div>
                                        <a href="<?php the_permalink(); ?>#promociones" class="button outline black">VER PROMO</a>
                                    </div>
                                    <div class="img grid-60 tablet-grid-50 grid-parent hide-on-mobile cover" style="background-image: url(<?php echo $img; ?>);"></div>

                                </article>
                            </div>
                        <?php endif; ?>                        

                    <?php $i++; $c++; endforeach; ?>
                    <div class="clear"></div>
                </div>
            </div>
        </section>
        <!-- end: #promociones -->
    <?php endif; ?>