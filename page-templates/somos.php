<?php /* Template Name: Somos */ ?>
<?php get_header(); ?>

<!-- start: #pagina-somos -->
<main id="pagina-somos" class="page" role="main">
	<?php if(have_posts()): the_post(); ?>

        <?php
		// Info General
		$etiqueta = get_field('etiqueta');
		$titulo = get_field('titulo');
		$bajada = get_field('bajada');
		$imagen_superior = get_field('imagen_superior');
		$url_imagen_superior = $imagen_superior['sizes']['slider-home'];

		//Mision
		$texto_mision = get_field('texto_mision');

		//Trayectoria
		$texto_historia = get_field('texto_historia');

		//Directores
		$directores = get_field('directores');
		$imagen_directores = get_field('imagen_directores');

		// Obras Historicas
		$obras_historicas = get_field('obras_historicas');

		// Revista y Video corporativo
		$fondo_revista = get_field('fondo_revista');
		$titulo_revista = get_field('titulo_revista');
		$texto_revista = get_field('texto_revista');
		$titulo_video = get_field('titulo_video');
		$url_video = get_field('url_video');

		?>
        <!-- start: .top -->
        <div class="top fullheight fullpage-section">
            <?php if($url_imagen_superior): ?>
                <div class="fullimg cover" style="background-image: url(<?php echo $url_imagen_superior; ?>);"></div>
            <?php endif; ?>
            <div class="middle aligntop" data-aos="fade-down">
                <div class="grid-container grid-small">
                	<?php if($etiqueta): ?>
                        <h3 class="etiqueta"><?php echo $etiqueta; ?></h3>
                    <?php endif; ?>
                    <?php if($titulo): ?>
                        <h1 class="titulo large white"><?php echo $titulo; ?></h1>
                    <?php endif; ?>
                    <?php if($bajada): ?>
                        <p class="titulo white"><?php echo $bajada; ?></p>
                    <?php endif; ?>
                </div>
            </div>
            <a href="#mision" class="next-section">Use scroll para navegar<img src="<?php echo EP_THEMEPATH; ?>/images/scrolldown.png"></a>
        </div>
        <!-- end: .top -->

        <!-- start: #mision -->
		<section id="mision" class="grid-container grid-medium fullpage-section">
        	<h3 class="titulo medium section">#MISIÓN / VISIÓN</h3>
        	<div class="texto">
				<?php echo $texto_mision; ?>
            </div>
        </section>
        <!-- end: #mision -->

        <!-- start: #trayectoria -->
		<section id="trayectoria" class="grid-container grid-medium fullpage-section">
        	<h3 class="titulo medium section">#TRAYECTORIA</h3>
        	<div class="texto">
				<?php echo $texto_historia; ?>
            </div>
        </section>
        <!-- end: #trayectoria -->

        <?php if( !empty($directores) ): ?>
            <!-- start: #directores -->
            <section id="directores" class="grid-container grid-medium fullpage-section">
                <h3 class="titulo medium section">#DIRECTORES</h3>
                <div class="lista textcenter">
                    <?php foreach($directores as $d): ?>
                    	<div class="item">
                        	<img src="<?php echo $d['imagen_director']['sizes']['proyecto-logo']; ?>">
                            <h4 class="titulo usmall"><?php echo $d['nombre_director']; ?></h4>
                            <p><?php echo $d['cargo_director']; ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="img-directores textcenter">
                	<img src="<?php echo $imagen_directores['url']; ?>">
                </div>
            </section>
            <!-- end: #directores -->
        <?php endif; ?>

        <?php if( !empty($obras_historicas) ):  $total = count($obras_historicas); ?>
            <!-- start: #obras-historicas -->
            <section id="obras-historicas" class="section grid-container grid-medium fullpage-section fp-auto-height">
                <h3 class="titulo medium"># 80 años en obras de 80 arquitectos</h3>
                <!--div class="detalle grid-40 hide-on-mobile hide-on-tablet">
                	<div class="middle">
                        <div class="contador titulo white">Imagen <span>1</span> de <?php echo $total; ?></div>
                        <h4 class="titulo large white"><?php echo $obras_historicas[0]['nombre_obra']; ?></h4>
                        <p class="titulo green">(<?php echo $obras_historicas[0]['fecha_obra']; ?>)</p>
                    </div>
                </div>
                <div class="lista grid-60 grid-parent">
                    <div class="slick-slider">
                        <?php $i = 1; foreach($obras_historicas as $o): ?>
                            <?php
                            $img = $o['imagen_obra']['sizes']['noticia-int'];
							$nombre = $o['nombre_obra'];
							$fecha = $o['fecha_obra'];
                            ?>
                            <div class="item">
                                <div class="img fullimg cover" style="background-image: url(<?php echo $img; ?>);"></div>
                                <div class="contenido">
                                	<div class="contador titulo white hide-on-mobile hide-on-tablet">Imagen <span><?php echo $i; ?></span> de <?php echo $total; ?></div>
                                    <h4 class="titulo large white"><?php echo $nombre; ?></h4>
                                    <p class="titulo green">(<?php echo $fecha; ?>)</p>
                                </div>
                            </div>
                        <?php $i++; endforeach; ?>
                    </div>
                </div-->
                <iframe src="https://e.issuu.com/anonymous-embed.html?u=eljengibre&d=libro_aniversario_desco_www" style="border:none;width:100%;height:500px;" allowfullscreen></iframe>
                <div class="clear"></div>
            </section>
            <!-- end: #obras-historicas -->
        <?php endif; ?>

        <?php /*?><div class="grid-container banner section textcenter fullscreen fullpage-section">
        	<div class="middle" data-aos="fade-up" data-aos-offset="600">
                <h3 class="titulo">¿quieres ver obras más recientes de la constructora?</h3>
                <br>
                <p><a href="<?php echo site_url(); ?>/construccion/#obras" class="button outline blue">VER MÁS</a></p>
            </div>
        </div><?php */?>

        <!-- start: .columns -->
        <div class="fullscreen columns bg-black fullpage-section" data-aos="fade">
            <div class="fullimg cover" style="background-image: url(<?php echo $fondo_revista['sizes']['slider-home']; ?>);"></div>
            <?php //if($texto_revista): ?>
                <!-- start: #revista -->
                <section id="revista" class="section middle grid-50">
                    <h2 class="titulo small white textcenter"><?php echo $titulo_revista; ?></h2>
                    <div class="overlay"></div>
                    <div class="hidden">
                        <div class="texto">
                        	<?php echo $texto_revista; ?>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>
                <!-- end: #revista -->
            <?php //endif; ?>

            <!-- start: #video-corporativo -->
            <section id="video-corporativo" class="section middle textcenter grid-50 active">
                <h2 class="titulo small white"><?php echo $titulo_video; ?></h2>
                <div class="overlay"></div>
                <div class="hidden">
                    <iframe width="480" height="320" src="https://www.youtube.com/embed/<?php echo youtube_id($url_video); ?>" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>
                <div class="clear"></div>
            </section>
            <!-- end: #video-corporativo -->
        </div>
        <!-- end: .columns -->

		<?php
        $args = array('posts_per_page' => 6, 'post_type' => 'desco-medios', 'suppress_filters' => false);
        $novedades = get_posts($args);
        if(!empty($novedades)):
        ?>
            <!-- start: #novedades -->
            <section id="novedades" class="section grid-container grid-medium fullpage-section fp-auto-height">
            	<h3 class="titulo medium">#DESCO EN LOS MEDIOS</h3>
                <div class="left-content grid-70 tablet-grid-70 np-mobile">
					<?php if(!empty($novedades)): ?>
                        <div class="listado">
                            <?php $i = 1; foreach($novedades as $post): setup_postdata($post); ?>
                            	<?php
                            		$medio = get_field('medio');
									$pdf = get_field('archivo_pdf');
								?>
                                <?php if($i == 1): ?>

                                    <article class="item large">
                                    	<div class="grid-60 np-mobile">
                                            <div class="meta titulo usmall">
                                                <?php if($medio) echo $medio.' - '; ?>
                                                <time datetime="<?php the_time('Y/m/d'); ?>"><?php echo get_the_date(); ?></time>
                                            </div>
                                            <h4><a href="<?php the_permalink(); ?>" class="titulo medium"><?php the_title(); ?></a></h4>
                                            <div class="excerpt"><?php the_excerpt(); ?></div>
                                            <a href="<?php the_permalink(); ?>" class="button outline black">LEER MÁS</a>
                                        </div>
                                        <div class="grid-40 tablet-grid-40 hide-on-mobile hide-on-tablet">
                                        	<div class="thumb">
												<div class="overflow"><?php the_post_thumbnail('medium'); ?></div>
												<?php if($pdf): ?>
                                                    <a href="<?php echo $pdf; ?>" class="descargar azul" target="_blank">Descargar <i class="flaticon-pdf"></i></a>
                                                <?php endif; ?>
                                            </div>

                                        </div>
                                        <div class="clear"></div>
                                    </article>
                                <?php endif; ?>

                                <?php if($i == 2 || $i == 3): ?>
                                        <?php
                                        $img = get_the_post_thumbnail_url($post->ID, 'noticia');
                                        ?>
                                        <div class="grid-50 count-<?php echo $i; ?>">
                                            <article class="item white no-image">
                                                <div class="meta titulo usmall white">
                                                	<?php if($medio) echo $medio.' - '; ?>
                                                	<time datetime="<?php the_time('Y/m/d'); ?>"><?php echo get_the_date(); ?></time>
                                                </div>
                                                <h4 class="titulo medium border-effect"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                                <div class="caption">
                                                    <a href="<?php the_permalink(); ?>" class="button outline white alignleft">LEER MÁS</a>
                                                    <?php if($pdf): ?>
                                                        <a href="<?php echo $pdf; ?>" class="descargar alignright" target="_blank">Descargar <i class="flaticon-pdf"></i></a>
                                                    <?php endif; ?>
                                                    <div class="clear"></div>
                                                </div>

                                            </article>
                                        </div>
                                <?php endif; ?>

                                <?php if($i > 4): ?>
     								<div class="clear"></div>
                                    <article class="item large">
                                    	<div class="meta titulo usmall">
                                            <?php if($medio) echo $medio.' - '; ?>
                                            <time datetime="<?php the_time('Y/m/d'); ?>"><?php echo get_the_date(); ?></time>
                                        </div>
                                        <h4><a href="<?php the_permalink(); ?>" class="titulo medium"><?php the_title(); ?></a></h4>
                                        <div class="excerpt"><?php the_excerpt(); ?></div>
                                        <a href="<?php the_permalink(); ?>" class="button outline black alignleft">LEER MÁS</a>
                                        <?php if($pdf): ?>
                                            <a href="<?php echo $pdf; ?>" class="descargar alignright" target="_blank">Descargar <i class="flaticon-pdf"></i></a>
                                        <?php endif; ?>
                                        <div class="clear"></div>
                                    </article>
                                <?php endif; ?>


                            <?php $i++;  endforeach; ?>

                        </div>
                    <?php endif; ?>
                </div>
                <?php get_sidebar(); ?>
            </section>
            <!-- end: #novedades -->
        <?php endif; wp_reset_postdata(); ?>


    <?php endif; ?>
</main>
<!-- end: #pagina-somos -->

<?php get_footer(); ?>
