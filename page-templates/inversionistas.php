<?php /* Template Name: Inversionistas */ ?>
<?php get_header(); ?>

<!-- start: #pagina-inversionistas -->
<main id="pagina-inversionistas" class="page" role="main">
	<?php if(have_posts()): the_post(); ?>

        <?php
		// Info General
		$etiqueta = get_field('etiqueta');
		$titulo = get_field('titulo');
		$bajada = get_field('bajada');
		$imagen_superior = get_field('imagen_superior');
		$url_imagen_superior = $imagen_superior['sizes']['slider-home'];

		//Beneficios
		$titulo_beneficios = get_field('titulo_beneficios');
		$beneficios = get_field('beneficios');

		//Por que Invertir
		$fondo_invertir = get_field('fondo_invertir');
		$texto_invertir = get_field('texto_invertir');

		// Desco en Cifras
		$titulo_cifras = get_field('titulo_cifras');
		$cifras = get_field('cifras');

		// Preguntas Frecuentes
		$titulo_faq = get_field('titulo_faq');
		$fondo_faq = get_field('fondo_faq');
		$faq = get_field('faq');


		?>
        <!-- start: .top -->
        <div class="top fullheight fullpage-section">
            <?php if($url_imagen_superior): ?>
                <div class="fullimg cover" style="background-image: url(<?php echo $url_imagen_superior; ?>);"></div>
            <?php endif; ?>
            <div class="middle aligntop" data-aos="fade-down">
                <div class="grid-container grid-small">
                	<?php if($etiqueta): ?>
                        <h3 class="etiqueta"><?php echo $etiqueta; ?></h3>
                    <?php endif; ?>
                    <?php if($titulo): ?>
                        <h1 class="titulo large white"><?php echo $titulo; ?></h1>
                    <?php endif; ?>
                    <?php if($bajada): ?>
                        <p class="titulo white"><?php echo $bajada; ?></p>
                    <?php endif; ?>
                </div>
            </div>
            <a href="#mision" class="next-section hide-on-mobile">Use scroll para navegar<img src="<?php echo EP_THEMEPATH; ?>/images/scrolldown.png"></a>
        </div>
        <!-- end: .top -->

        <div class="menu hide-on-mobile hide-on-tablet">
            <ul>

                <li class="nombre hide-on-desktop-sm">INVERSIONISTAS</li>
                <li><a href="#beneficios">Beneficios</a></li>
                <li><a href="#invertir">¿Por qué Desco?</a></li>
                <li><a href="#faq">Preguntas Fecuentes</a></li>
                <li><a href="#form">Inversionista Corporativo</a></li>
            </ul>
        </div>

        <?php if( !empty($beneficios) ): ?>
            <!-- start: #beneficios -->
            <section id="beneficios" class="section grid-container grid-medium fullpage-section">
                <div data-aos="fade-up">
                    <h3 class="titulo medium textcenter"><?php echo $titulo_beneficios; ?></h3>
                    <div class="lista">
                        <?php $i = 0; foreach($beneficios as $b): ?>
                            <?php
                            $icono = $b['icono_beneficio']['url'];
                            if($i % 3 == 0) echo '<div class="clear hide-on-tablet"></div>';
							if($i % 2 == 0 && $i > 1) echo '<div class="clear hide-on-desktop"></div>';

                            ?>
                            <div class="item grid-33 tablet-grid-50 textcenter">
                                <img src="<?php echo $icono; ?>" class="icono">
                                <h4 class="titulo small"><?php echo $b['titulo_beneficio']; ?></h4>
                                <p><?php echo $b['bajada_beneficio']; ?></p>
                            </div>

                        <?php $i++; endforeach; ?>
                        <div class="clear"></div>
                    </div>
                </div>
            </section>
            <!-- end: #beneficios -->
        <?php endif; ?>

        <?php if( $texto_invertir ): ?>
            <!-- start: #invertir -->
            <section id="invertir" class="fullpage-section cover section" style="background-image: url(<?php echo $fondo_invertir['sizes']['slider-home']; ?>);">
            	<div class="middle">
                    <div class="grid-container grid-medium">
                        <div class="texto white grid-50 tablet-grid-65" data-aos="fade-right">
                            <?php echo $texto_invertir; ?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end: #invertir -->
        <?php endif; ?>

        <?php if( !empty($cifras) ): ?>
            <!-- start: #cifras -->
            <section id="cifras" class="section grid-container grid-medium fullpage-section">
            	<div data-aos="fade-down">
                    <h3 class="titulo medium textcenter"><?php echo $titulo_cifras; ?></h3>
                    <div class="lista slick-slider circle-arrows">
                        <?php foreach($cifras as $c): ?>
                            <?php
                            $imagen_url = $c['imagen_cifras']['sizes']['medium'];
                            ?>
                            <div class="item grid-33">
                                <div class="img cover" style="background-image: url('<?php echo $imagen_url; ?>'); min-height: 150px;"></div><br>
                                <p><?php echo $c['texto_cifras']; ?></p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="clear"></div>
                </div>
            </section>
            <!-- end: #cifras -->
        <?php endif; ?>

        <?php if( !empty($faq) ): ?>
            <!-- start: #faq -->
            <section id="faq" class="section fullpage-section cover" style="background: url(<?php echo $fondo_faq['sizes']['slider-home']; ?>) no-repeat center center;">
            	<div class="grid-container grid-medium" data-aos="fade-up">
                    <h3 class="titulo medium white textcenter"><?php echo $titulo_faq; ?></h3>
                    <div class="lista">
                    	<div class="grid-50">
							<?php $i = 0; foreach($faq as $f): ?>
                            	<?php

								if(ceil(count($faq) / 2) == $i) echo '</div><div class="grid-50">';
								?>
                                <div class="item toggle <?php if($i == 0) echo 'first active'; ?>">
                                    <h5 class="desplegar"><?php echo $f['pregunta_faq']; ?><i class="flaticon-down-arrow"></i></h5>
                                    <div class="toggle-content texto small scrollpane">
                                    	<?php echo $f['respuesta_faq']; ?>
                                    </div>
                                </div>
                            <?php $i++; endforeach; ?>
                        </div>
                        <div class="clear"></div>
                    </div>

                </div>
            </section>
            <!-- end: #faq -->
        <?php endif; ?>

        <!-- start: #form -->
		<section id="form" class="grid-container grid-usmall section fullpage-section">
        	<div data-aos="fade-right" data-aos-offset="300">
                <h3 class="titulo medium textcenter" style="padding-top: 30px;">#Invierte Hoy, Conversemos</h3>
                <!-- start: .fondo -->
                <div class="fondo">
                    <form action="send.php" data-url="<?php echo site_url(); ?>/wp-admin/admin-ajax.php" method="post" class="formulario">
                        <p class="textcenter">Escríbenos para conocer alternativas y beneficios de Desco</p>
                        <div class="input-wrapper grid-50 tablet-grid-50 first">
                            <input type="text" name="nombre" class="inputbox" placeholder="Nombre Completo *" required>
                        </div>
                        <div class="input-wrapper grid-50 tablet-grid-50 last">
                            <input type="email" name="email" class="inputbox" placeholder="Email *" required>
                        </div>
                        <div class="clear"></div>
                        <div class="input-wrapper grid-50 tablet-grid-50 first">
                            <input type="text" name="telefono" class="inputbox" placeholder="Teléfono *" required>
                        </div>
                        <div class="input-wrapper grid-50 tablet-grid-50 last">
                            <select name="motivo" required>
                                <option value="">Seleccione Motivo de Contacto *</option>
                                <option value="Inversiones Generales">Inversiones Generales</option>
                                <option value="Inversiones Corporativas">Inversiones Corporativas</option>
                                <option value="Reclamos">Reclamos</option>
                                <option value="Otro">Otro</option>
                            </select>
                        </div>
                        <div class="clear"></div>
                        <div class="input-wrapper">
                            <textarea name="comentario" placeholder="Comentario"></textarea>
                        </div>
                        <input type="hidden" name="nonce" value="<?php echo wp_create_nonce("contacto"); ?>">
                        <input type="hidden" name="form_title" value="Inversionistas">
                        <input type="hidden" name="action" value="ajax_contact">

                        <div class="msg-exito">
                            <p><i class="fa fa-check-square-o"></i> Tu mensaje ha sido enviado, te responderemos a la brevedad.</p>
                        </div>
                        <div class="captcha grid-65 grid-parent">
                            <div id="recaptcha-inversionistas" class="g-recaptcha" data-sitekey="6LeH8DoUAAAAAP9jUE2QUGB1ZXIx8305ihOSjcjE"></div>
                        </div>
                        <button type="submit" class="submit button white icon">
                            <span class="default">ENVIAR</span>
                            <span class="loading">ENVIANDO</span>
                        </button>
                        <div class="clear"></div>
                        <p><small>(*) Campos obligatorios</small></p>
                    </form>
                    <div class="grid-50 tablet-grid-50 np-mobile">
                        <div class="cta llamar">
                            <h4 class="titulo small white">Llámanos</h4>
                            <p class="hide-on-mobile">+(56 2) 23929200</p>
                            <p class="hide-on-desktop hide-on-tablet underline-effect"><a href="+56223929200">+(56 2) 23929200</a></p>
                        </div>
                    </div>
                    <div class="grid-50 tablet-grid-50 np-mobile">
                        <div class="cta ubicacion">
                            <h4 class="titulo small white">OFICINA CENTRAL</h4>
                            <p>Av. Del Parque 4160,<br>5 piso, Torre B, Ciudad Empresarial, Huechuraba</p>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <!-- end: .fondo -->
            </div>
        </section>
        <!-- end: #form -->


    <?php endif; ?>
</main>
<!-- end: #pagina-inversionistas -->

<?php get_footer(); ?>
