<?php /* Template Name: Canal de Denuncia */ ?>
<?php get_header(); ?>

<!-- start: #pagina-denuncia -->
<main id="pagina-denuncia" class="page" role="main">
	<?php if(have_posts()): the_post(); ?>
    	
        <?php
		// Info General
		$etiqueta = get_field('etiqueta');
		$titulo = get_field('titulo');
		$bajada = get_field('bajada');
		$imagen_superior = get_field('imagen_superior');
		$url_imagen_superior = $imagen_superior['sizes']['slider-home'];

		?>
        <!-- start: .top -->
        <div class="top fullheight fullpage-section">
            <?php if($url_imagen_superior): ?>
                <div class="fullimg cover" style="background-image: url(<?php echo $url_imagen_superior; ?>);"></div>
            <?php endif; ?>
            <div class="middle" data-aos="fade-down">
                <div class="grid-container grid-small">
                	<?php if($etiqueta): ?>
                        <h3 class="etiqueta"><?php echo $etiqueta; ?></h3>
                    <?php endif; ?>
                    <?php if($titulo): ?>
                        <h1 class="titulo large white"><?php echo $titulo; ?></h1>
                    <?php endif; ?>
                    <?php if($bajada): ?>
                        <p class="titulo white"><?php echo nl2br($bajada); ?></p>
                    <?php endif; ?>
                    <div class="botones">
	                    <a href="https://www.jotform.com/bprecht/desco" class="button outline" target="_blank">Haz tu denuncia</a>
                        <br><br>
                    </div>
                </div>
            </div>
            <a href="#form" class="next-section hide-on-mobile">Use scroll para navegar<img src="<?php echo EP_THEMEPATH; ?>/images/scrolldown.png"></a>
        </div>
        <!-- end: .top -->
        
        <div class="menu hide-on-mobile hide-on-tablet">
            <ul>

                <li class="nombre hide-on-desktop-sm">CANAL DE DENUNCIA</li> 
                <li><a href="https://www.jotform.com/bprecht/desco" target="_blank" class="no-scroll">Haz tu Denuncia</a></li>
                <li><a href="#menu-1">Código de Ética</a></li>
                <li><a href="#menu-2">Ley 20.393</a></li>
                <li><a href="#menu-3">Ley 19.913</a></li>
                <li><a href="#menu-4">Descargar Aquí</a></li>
            </ul>
        </div> 
        
<?php /*?>        <!-- start: #form -->
		<section id="form" class="grid-container grid-usmall section fullpage-section">
        	<h3 class="titulo medium textcenter" style="padding-top: 30px;">realice su denuncia en el siguiente Formulario</h3>
        	<!-- start: .fondo -->
            <div class="fondo">
                <form action="send.php" data-url="<?php echo site_url(); ?>/wp-admin/admin-ajax.php" method="post" class="formulario">
                    <p class="textcenter">Completa los siguientes campos y nos comunicaremos a la brevedad.<br>Recuerde que su denuncia será anónima</p>
                    <div class="input-wrapper grid-50 tablet-grid-50 first">
                        <input type="text" name="nombre" class="inputbox" placeholder="Nombre Completo *" required>
                    </div>
                    <div class="input-wrapper grid-50 tablet-grid-50 last">
                        <input type="email" name="email" class="inputbox" placeholder="Email *" required>
                    </div>
                    <div class="clear"></div>
                    <div class="input-wrapper">
                        <textarea name="comentario" placeholder="Canal Denuncia (*)" required></textarea>
                    </div>
                    <input type="hidden" name="nonce" value="<?php echo wp_create_nonce("contacto"); ?>">
                    <input type="hidden" name="form_title" value="Denuncias">
                    <input type="hidden" name="action" value="ajax_contact">
       
                    <div class="msg-exito">
                        <p><i class="fa fa-check-square-o"></i> Tu mensaje ha sido enviado, te responderemos a la brevedad.</p>
                    </div>
                    <div class="captcha grid-65 grid-parent">
                        <div id="recaptcha-denuncia" class="g-recaptcha" data-sitekey="6LeH8DoUAAAAAP9jUE2QUGB1ZXIx8305ihOSjcjE"></div>
                    </div>
                    <button type="submit" class="submit button white icon">
                        <span class="default">ENVIAR</span>
                        <span class="loading">ENVIANDO</span>
                    </button>
                    <div class="clear"></div>
                    <p><small>(*) Campos obligatorios</small></p>
                </form>
            </div>
            <!-- end: .fondo -->
        </section>
        <!-- end: #form --><?php */?>
        
        <!-- start: #texto-libre -->
		<section id="texto-libre" class="grid-container grid-medium fullpage-section section">
        	<div class="texto">
				<?php the_content(); ?>
            </div>
        </section>
        <!-- end: #texto-libre -->
        
    
    <?php endif; ?>
</main>
<!-- end: #pagina-denuncia -->

<?php get_footer(); ?>