<?php /* Template Name: Contacto */ ?>
<?php get_header(); ?>

<!-- start: #pagina-contacto -->
<main id="pagina-contacto" class="page" role="main">
	<?php if(have_posts()): the_post(); ?>

        <?php
		// Info General
		$etiqueta = get_field('etiqueta');
		$titulo = get_field('titulo');
		$imagen_superior = get_field('imagen_superior');
		$url_imagen_superior = $imagen_superior['sizes']['slider-home']
		?>
        <!-- start: .top -->
        <div class="top fullheight">
            <?php if($url_imagen_superior): ?>
                <div class="fullimg cover" style="background-image: url(<?php echo $url_imagen_superior; ?>);"></div>
            <?php endif; ?>
            <div class="middle">
                <div class="grid-container grid-small" data-aos="fade-down">
                	<?php if($etiqueta): ?>
                        <h3 class="etiqueta"><?php echo $etiqueta; ?></h3>
                    <?php endif; ?>
                    <?php if($titulo): ?>
                        <h1 class="titulo large white"><?php echo $titulo; ?></h1>
                    <?php endif; ?>
                    <!-- start: .fondo -->
                    <div class="fondo">
                        <form action="send.php" data-url="<?php echo site_url(); ?>/wp-admin/admin-ajax.php" method="post" class="formulario">
                            <p class="textcenter">Completa los siguientes campos y nos comunicaremos a la brevedad</p>
                            <div class="input-wrapper grid-50 tablet-grid-50 first">
                                <input type="text" name="nombre" class="inputbox" placeholder="Nombre Completo *" required>
                            </div>
                            <div class="input-wrapper grid-50 tablet-grid-50 last">
                                <input type="email" name="email" class="inputbox" placeholder="Email *" required>
                            </div>
                            <div class="input-wrapper grid-50 tablet-grid-50 first">
                                <input type="text" name="telefono" class="inputbox" placeholder="Teléfono *" required>
                            </div>
                            <div class="input-wrapper grid-50 tablet-grid-50 last">
                                <select name="motivo" required>
                                    <option value="">Seleccione Motivo de Contacto</option>
                                    <option value="Consultas Generales">Consultas Generales</option>
                                    <option value="Servicio al Cliente">Servicio al Cliente</option>
                                    <option value="Cotizar Proyecto">Cotizar Proyecto</option>
                                    <option value="Reclamos">Reclamos</option>
                                    <option value="Otro">Otro</option>
                                </select>
                            </div>
                            <div class="clear"></div>
                            <div class="input-wrapper">
                                <textarea name="comentario" placeholder="Comentario"></textarea>
                            </div>
                            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce("contacto"); ?>">
                            <input type="hidden" name="form_title" value="Contacto">
                            <input type="hidden" name="action" value="ajax_contact">

                            <div class="msg-exito">
                                <p><i class="fa fa-check-square-o"></i> Tu mensaje ha sido enviado, te responderemos a la brevedad.</p>
                            </div>
                            <div class="captcha grid-65 grid-parent">
                            	<div id="recaptcha-contacto" class="g-recaptcha" data-sitekey="6LeH8DoUAAAAAP9jUE2QUGB1ZXIx8305ihOSjcjE"></div>
                            </div>
                            <button type="submit" class="submit button white">
                                <span class="default">ENVIAR</span>
                                <span class="loading">ENVIANDO</span>
                            </button>
                            <div class="clear"></div>
                            <p><small>(*) Campos obligatorios</small></p>
							<input type="hidden" value="<?php echo EP_THEMEPATH; ?>">
                        </form>
                    </div>
                    <!-- end: .fondo -->
                </div>
            </div>
        </div>
        <!-- end: .top -->

    <?php endif; ?>
</main>
<!-- end: #pagina-contacto -->

<?php get_footer(); ?>
