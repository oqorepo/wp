<?php /* Template Name: Constructora */ ?>
<?php get_header(); ?>
<!-- start: #pagina-constructora -->
<main id="pagina-constructora" class="page" role="main">

	<?php if(have_posts()): the_post(); ?>

        <?php
		// Info General
		$etiqueta = get_field('etiqueta');
		$titulo = get_field('titulo');
		$bajada = get_field('bajada');
		$imagen_superior = get_field('imagen_superior');
		$url_imagen_superior = $imagen_superior['sizes']['slider-home'];

		// Galeria Aerea
		$galeria_aerea = get_field('galeria_aerea');
		?>
        <!-- start: .top -->
        <div class="top fullheight fullpage-section">
            <?php if($url_imagen_superior): ?>
                <div class="fullimg cover" style="background-image: url(<?php echo $url_imagen_superior; ?>);"></div>
            <?php endif; ?>
            <div class="middle aligntop" data-aos="fade-down">
                <div class="grid-container grid-small">
                	<?php if($etiqueta): ?>
                        <h3 class="etiqueta"><?php echo $etiqueta; ?></h3>
                    <?php endif; ?>
                    <?php if($titulo): ?>
                        <h1 class="titulo large white"><?php echo $titulo; ?></h1>
                    <?php endif; ?>
                    <?php if($bajada): ?>
                        <p class="titulo white"><?php echo $bajada; ?></p>
                    <?php endif; ?>
                    <div class="botones textcenter">
	                    <a href="#obras" class="button outline">OBRAS EN DESARROLLO</a>
                        <a href="#obras-ejecutadas" class="button outline">OBRAS EJECUTADAS</a>
                    </div>
                </div>
            </div>
            <a href="#obras" class="next-section hide-on-mobile">Use scroll para navegar<img src="<?php echo EP_THEMEPATH; ?>/images/scrolldown.png"></a>
        </div>
        <!-- end: .top -->

        <?php
		$args = array(
			'posts_per_page' => -1,
			'post_type' => 'obras',
			'suppress_filters' => false,
			'orderby' => 'menu_order',
			'order' => 'ASC',
		);
        $proyectos = get_posts($args);
        ?>     
        
        <?php if( !empty($proyectos) ): // Obras v2?>
            
            <!-- start: #obras -->
            <section id="obras" class="carrusel-proyectos section fullpage-section" style="padding-top: 30px;">
            	<div class="grid-container grid-medium">
                    <h3 class="titulo textcenter">#OBRAS EN DESARROLLO</h3>

                    <div class="listav2">
                        <?php $i = 0; foreach($proyectos as $post): setup_postdata($post); ?>
                            <?php
                            $descripcion = get_field('descripcion');
                            $galeria = get_field('galeria');
                            if(empty($galeria)) continue;
                            $imagen = $galeria[0]['sizes']['proyecto-galeria-thumb'];
                            $class = ($i == 0) ? 'active' : '';
                            if($i == 4) $i = 0;
                            ?>
                            <div class="item grid-25 tablet-grid-50">
                                <div class="img cover lazy"  data-src="<?php echo $imagen; ?>"><div class="overlay"></div></div>
                                <div class="info">
                                    <h4 class="titulo usmall"><?php the_title(); ?></h4>
                                    <div class="borde"></div>
                                    <a href="<?php the_permalink(); ?>?v=2" class="button outline black lightbox-obras">VER MÁS</a>
                                </div>                              
                                <div class="clear"></div>
                            </div>
                            <?php
                            if($i == 3){
                                echo '<div class="clear"></div>';
                            }
                            ?>
                        <?php $i++; endforeach;  wp_reset_postdata(); ?>
                    </div>

                    <div class="clear"></div>
                </div>
            </section>
            <!-- end: #obras -->   
            
        <?php endif; ?>

        <?php
        $sql = "  
        SELECT $wpdb->posts.ID
        FROM $wpdb->posts, $wpdb->postmeta
        WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id 

        AND $wpdb->postmeta.meta_key = 'fecha'   

        AND $wpdb->posts.post_status = 'publish' 
        AND $wpdb->posts.post_type = 'obras-ejecutadas'
        ORDER BY SUBSTRING($wpdb->postmeta.meta_value, -4) DESC
            ";
        $results = $wpdb->get_results($sql, ARRAY_A);
        $ids_obras = array();
        foreach($results as $r){
            $ids_obras[] = $r['ID'];
        }
        //var_dump($ids_obras);

		// $args = array(
		// 	'posts_per_page' => -1,
		// 	'post_type' => 'obras-ejecutadas',
		// 	'suppress_filters' => false,
		// 	//'orderby' => 'meta_value_num',
		// 	//'meta_key' => 'fecha',
		// 	//'order' => 'DESC',
        //     'orderby' => 'post__in',
        //     'post__in' => $ids_obras,
        // );
        $args = array(
			'posts_per_page' => -1,
			'post_type' => 'obras-ejecutadas',
			'suppress_filters' => false,
			'orderby' => 'meta_value_num',
			'meta_key' => 'numero_obra',
			'order' => 'DESC',
            // 'orderby' => 'meta_value',
		);
        $proyectos = get_posts($args);
        $args = array(
            'taxonomy' => 'categoria-obras',
            'hide_empty' => 1,
            'orderby' => 'count',
            'order' => 'DESC'
        );
        $categorias = get_categories( $args );

        ?>

        <?php if( !empty($categorias) ): // Obras ejecutadas v2 ?>
            <!-- start: #obras-ejecutadas -->
            <section id="obras-ejecutadas" class="v2 carrusel-proyectos section fullpage-section" style="padding-top: 30px;">
            	<div class="grid-container grid-medium">
                    <h3 class="titulo textcenter">#OBRAS EJECUTADAS</h3>

                    <div class="filtro hide-on-tablet hide-on-desktop">
                        <label>
                            Selecciona la categoría:
                            <select class="blue tab">
                                <?php $i = 0; foreach($categorias as $c): ?>
                                    <option value="<?php echo $i; ?>"><?php echo $c->name; ?></option>
                                <?php $i++; endforeach; ?>
                            </select>
                        </label>
                    </div>

                    <ul class="categorias tabs grid-20 tablet-grid-25 hide-on-mobile">
                    	<?php foreach($categorias as $c): ?>
                            <li><a href="javascript:void(0)" class="titulo usmall"><?php echo $c->name; ?></a></li>
                        <?php endforeach; ?>
                    </ul>

					<div class="panes grid-80 tablet-grid-75 np-mobile">
                    	<?php foreach($categorias as $c): ?>
                            <!-- start: .tab -->
                            <div class="tab">

                                <!-- <div class="filtro hide-on-tablet hide-on-desktop">
                                    <label>
                                        Selecciona el proyecto:
                                        <select class="blue">
                                            <?php $i = 0; foreach($proyectos as $post): setup_postdata($post); $postcat = get_the_terms( $post->ID, 'categoria-obras' ); ?>
                                                <?php if($c->term_id == $postcat[0]->term_id): ?>
                                                    <option value="carrusel-<?php echo $post->ID; ?>"><?php the_title(); ?></option>
                                                <?php endif; ?>
                                            <?php $i++; endforeach; ?>
                                        </select>
                                    </label>
                                </div> -->

                                <!-- start: .lista -->
                                <div class="lista scrollpane">
                                    <?php $i = 0; foreach($proyectos as $post): setup_postdata($post); ?>
                                        <?php
										$postcat = get_the_terms( $post->ID, 'categoria-obras' );
										if($c->term_id == $postcat[0]->term_id):
											$galeria = get_field('galeria');
											if(empty($galeria)) continue;
											$imagen = $galeria[0]['sizes']['proyecto-galeria-thumb'];
                                            $class = ($i == 0) ? 'active' : '';
                                            if($i == 4) $i = 0;
											?>
											<div class="item grid-25 tablet-grid-50 obra-hover">
                                                <a href="<?php the_permalink(); ?>?v=2" class="lightbox-obras">
                                                    <div class="img cover grid-35 tablet-grid-30 mobile-grid-40 lazy" data-src="<?php echo $imagen; ?>">
                                                        <div class="obra-hover-div">
                                                            <i class="fa fa-search" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                    <div class="info grid-65 tablet-grid-70 mobile-grid-60 grid-parent">
                                                        <h4 class="titulo usmall"><?php echo ucwords( strtolower( get_the_title() ) ); ?></h4>
                                                    </div>
                                                    <div class="clear"></div>
                                                </a>
											</div>
                                            <?php
                                            if($i ==  1){
                                                echo '<div class="clear hide-on-desktop hide-on-mobile border"></div>'; 
                                            } 
                                            if($i ==  3){
                                                echo '<div class="clear hide-on-mobile border"></div>'; 
                                            }                                                
                                            ?>
                                    <?php $i++; endif; endforeach;  wp_reset_postdata(); ?>
                                </div>
                                <!-- end: .lista -->
                                
                                <div class="clear"></div>
                            </div>
                            <!-- end: .tab -->
                        <?php endforeach; ?>
                    </div>
                </div>
            </section>
            <!-- end: #obras-ejecutadas -->
        <?php endif; wp_reset_postdata(); ?>



		<?php if(!empty($galeria_aerea)): ?>
            <!-- start: #galeria -->
            <section id="galeria" class="section fullpage-section" style="padding-top: 30px;">
                <div class="grid-container grid-medium">
                    <h3 class="titulo textcenter">#GALERÍA VISTA AÉREAS DESDE UN DRON</h3>

                    <div class="lista ep-gallery">
                        <?php $i = 1; foreach($galeria_aerea as $g): ?>
                            <div class="grid-33 tablet-grid-50 np-mobile">
                                <div class="item">
                                    <?php if( $g['galeria_tipo'] == 'video' ): ?>
                                        <?php
                                            preg_match('/src="([^"]+)"/', $g['url_video'], $match);
                                            $id_video = youtube_id($match[1]);
                                         ?>
                                        <a href="https://www.youtube.com/watch?v=<?php echo $id_video; ?>" class="lightbox mfp-iframe thumb"><div class="img fullimg cover" style="background-image: url(<?php echo $g['galeria_imagen']['sizes']['planta-thumb']; ?>);"></div><span class="zoom"><i class="fa fa-play"></i></span></a>
                                    <?php else: ?>
                                        <a href="<?php echo $g['galeria_imagen']['sizes']['ep-large']; ?>" class="lightbox"><div class="img fullimg cover" style="background-image: url(<?php echo $g['galeria_imagen']['sizes']['planta-thumb']; ?>);"></div><span class="zoom"><i class="fa fa-search"></i></span></a>
                                    <?php endif; ?>
                                    <?php if( $g['galeria_titulo'] ): ?>
                                        <h4 class="titulo usmall"><?php echo $g['galeria_titulo']; ?></h4>
                                    <?php endif; ?>
                                    <?php if( $g['galeria_bajada'] ): ?>
                                        <p class="bajada"><?php echo $g['galeria_bajada']; ?></p>
                                    <?php endif; ?>
                                    <div class="clear"></div>

                                </div>
                            </div>
                            <?php if($i % 3 == 0 ) echo '<div class="clear hide-on-tablet"></div>'; ?>
                            <?php if($i % 2 == 0 ) echo '<div class="clear hide-on-desktop"></div>'; ?>
                        <?php $i++; endforeach;  wp_reset_postdata(); ?>
                        <div class="clear"></div>
                    </div>

                    <div class="clear"></div>
                </div>
            </section>
            <!-- end: #galeria -->
        <?php endif; wp_reset_postdata(); ?>

    <?php endif; ?>
</main>
<!-- end: #pagina-constructora -->

<?php get_footer(); ?>
