<?php /* Template Name: Exportar Contactos */ ?>
<?php
if (! is_user_logged_in() ) {
	auth_redirect();
}
?>

<?php get_header(); ?>

<!-- start: #exportar -->

<main id="exportar" class="main page" role="main">

	<?php if(have_posts()): the_post(); ?>

    	<!-- start: .content -->
        <div class="content grid-container aligncenter">
            <article <?php post_class(); ?>>
                <header class="interior">
                    <h1 class="titulo textcenter"><?php the_title(); ?></h1>
                    <h3 class="textcenter titulo small">Todos los campos son opcionales.</h3>
                    <div class="clear"></div>
                </header>

                <?php
					$tipo = 2;
					$fecha_desde_en = $fecha_hasta_en = $proyecto = '';
					$filtro = array();
					
					if(isset($_GET['tipo'])){
						$tipo = $_GET['tipo'];
					}

					if(isset($_GET['proyecto']) && $_GET['proyecto']){
						$proyecto = $_GET['proyecto'];
						//$m2_total = rwmb_meta('superficie_total_mt', '', $proyecto);
						$titulo = get_the_title($proyecto);
						//if($m2_total) $titulo.= ' '.$m2_total.'m&sup2;';
						$filtro[] = 'Proyecto==='.$titulo;
					}

					if(isset($_GET['nombre']) && $_GET['nombre']){
						$nombre = $_GET['nombre'];
						$filtro[] = 'stripos(Nombre,'.$nombre.')===0||stripos(Nombre,'.$nombre.')>0';
					}

					if(isset($_GET['rut']) && $_GET['rut']){
						$rut = $_GET['rut'];
						$filtro[] = 'Rut==='.$rut;
					}

					if(isset($_GET['fecha_desde']) && $_GET['fecha_desde']){
						$f = explode('-', $_GET['fecha_desde']);
						$fecha_desde_en = $f[2].'-'.$f[1].'-'.$f[0];
						$filtro[] = 'submit_time>='.$fecha_desde_en;
					}

					if(isset($_GET['fecha_hasta']) && $_GET['fecha_hasta']){
						$fh = explode('-', $_GET['fecha_hasta']);
						$fecha_hasta_en = $fh[2].'-'.$fh[1].'-'.$fh[0]; 
						$filtro[] = 'submit_time<='.$fecha_hasta_en;
					}

					switch($tipo){
						case 1: $formulario = 'Compartir'; break;	
						case 2: $formulario = 'Proyecto'; break;	
						case 3: $formulario = 'Cotizar'; break;	
						case 4: $formulario = 'Servicio al Cliente'; break;	
						default: $formulario = 'Proyecto';
					}
					
					//$formulario = 'Proyecto';

				?>

                <section class="post-content center">
					<?php
                    if ( post_password_required() ):
                        echo get_the_password_form();
                    else:
                    ?>

                	<form action="<?php echo site_url(); ?>/exportar" method="get">
                    	
                        
                        <div class="clear20"></div>
                        
                        <div class="input-wrapper tipo">
                        	<label for="tipo">Tipo de Formulario</label>
                            <select name="tipo" id="tipo">
                                <option value="1" <?php if($tipo == 1) echo 'selected'; ?>>Compartir Planta</option>
                                <option value="2" <?php if($tipo == 2) echo 'selected'; ?>>Proyecto (solicitar información)</option>
                                <option value="3" <?php if($tipo == 3) echo 'selected'; ?>>Cotizar Planta</option>
                                <option value="4" <?php if($tipo == 4) echo 'selected'; ?>>Servicio al Cliente</option>
                            </select>
                        </div>
                        
                        <div class="input-wrapper grid-50">
                        	<label for="fecha_desde">Fecha inicial</label>
                            <input type="text" name="fecha_desde" class="inputbox fecha" id="fecha_desde"  value="<?php echo @$_GET['fecha_desde']; ?>">
                            
                        </div>

                        <div class="input-wrapper grid-50">
                            <label for="fecha_hasta">Fecha limite</label>
                            <input type="text" name="fecha_hasta" class="inputbox fecha" id="fecha_hasta" value="<?php echo @$_GET['fecha_hasta']; ?>">
                        </div>

                        <div class="clear"></div>
                        
                        <?php if($formulario == 'Proyecto' || $formulario == 'Cotizar' || $formulario == 'Servicio al Cliente'): ?>
                            <div class="input-wrapper grid-50">
                                <?php
                                $args = array('posts_per_page' => -1, 'post_type' => 'proyectos', 'order' => 'ASC', 'suppress_filters' => false);
								if($formulario == 'Servicio al Cliente'){
									$args = array(
										'posts_per_page' => -1,
										'post_type' => 'proyectos',
										'order' => 'ASC',
										'suppress_filters' => false,
										'meta_key' => 'garantia',
										'meta_value' => 'si'
									);	
								}
                                $proyectos = get_posts($args);
                                ?>
                                <label for="proyecto">Proyecto</label>
                                <select name="proyecto">
                                    <option value="">Todos</option>
                                    <?php foreach($proyectos as $post): setup_postdata($post); $m2_total = get_field('mt_total'); ?>
                                        <option value="<?php echo $post->ID; ?>" <?php if($proyecto == $post->ID) echo 'selected'; ?>><?php the_title(); ?><?php if($m2_total) echo ' '.$m2_total.'m&sup2;'; ?></option>
                                    <?php endforeach; wp_reset_postdata(); ?>
                                </select>
                            </div>
                        <?php endif; ?>

                        <div class="input-wrapper grid-50">
                        	<label for="nombre">Nombre (del emisor)</label>
                            <input type="text" name="nombre" class="inputbox" id="nombre"  value="<?php echo @$_GET['nombre']; ?>">
                        </div>

                        <div class="clear"></div>

                    	<button type="submit" class="button border white alignright" value="Filtrar">Filtrar</button>
                        <input type="reset" class="button border white alignright" value="Limpiar" style="margin-right: 20px;">

                        <div class="clear"></div>

                    </form>

                    <div class="text">

                    	<?php
							$current_page = 1;
							$limit = 50;
							if(isset($_GET['pagina']) && $_GET['pagina']){
								$current_page = $_GET['pagina'];
							}

							//$total = do_shortcode('[cfdb-count form="'.$formulario.'" filter="'.implode('&&', $filtro).'"]');

							require_once(ABSPATH . 'wp-content/plugins/contact-form-7-to-database-extension/ExportToValue.php');

							$export = new ExportToValue();

							$atts = array(
								'function' => 'count',
								'no_echo' => true,
								//'limit' => "$offset,$limit",
								'filter' => implode('&&', $filtro)
								//'hide' => 'Submitted Login,Submitted From'
							);

							//$total = $export->export($formulario, $atts);

							$total = do_shortcode('[cfdb-count form="'.$formulario.'" filter="'.implode('&&', $filtro).'"]');
						
							$n_pages = ceil($total / $limit);
							$offset = ($current_page - 1)  * $limit;
							$start = $offset + 1;
							$end = min(($offset + $limit), $total);
							$get = array();
							$get = $_GET;
							unset($_GET['pagina']);
							$current_url = '?'.http_build_query($_GET);
							
							if($n_pages < 1) $n_pages = 1;

							echo '<div class="paginacion textcenter">'; 
								echo '<p>Página <strong>'.$current_page.'</strong> de un total de <strong>'.$n_pages.'</strong>.</p>';

								// The "back" link
								echo $prevlink = ($current_page > 1) ? '<a href="'.$current_url.'&pagina=1" title="">Primera Página</a> <a href="'.$current_url.'&pagina=' . ($current_page - 1) . '" title="">Anterior</a>' : '<span class="disabled">Primera Página</span> <span class="disabled">Anterior</span>';
							
								// The "forward" link

								echo $nextlink = ($current_page < $n_pages) ? '<a href="'.$current_url.'&pagina=' . ($current_page + 1) . '" title="">Siguiente</a> <a href="'.$current_url.'&pagina=' . $n_pages . '" title="">Última Página</a>' : '<span class="disabled">Siguiente</span> <span class="disabled">Última Página</span>';
								
							echo '</div>';

							require_once(ABSPATH . 'wp-content/plugins/contact-form-7-to-database-extension/ExportToHtmlTable.php');

							$table = new ExportToHtmlTable();

							$atts = array(
								'function' => 'table',
								'limit' => "$offset,$limit",
								'filter' => implode('&&', $filtro),
								'hide' => 'Submitted Login,Submitted From'
							);

							$table_html = $table->export($formulario, $atts);	

							echo $table_html;

							//echo do_shortcode('[cfdb-table limit="'.$offset.','.$limit.'" form="'.$formulario.'" filter="'.implode('&&', $filtro).'" hide="Submitted Login,Submitted From"]');

							echo '<div class="clear10"></div>';

							//echo do_shortcode('[cfdb-export-link form="'.$formulario.'" filter="'.implode('&&', $filtro).'" enc="TSVUTF16LEBOM" linktext="Descargar todos los registros encontrados en Excel" hide="Submitted Login,Submitted From"]');

							$download_link = site_url().'/wp-admin/admin-ajax.php?action=cfdb-login&cfdb-action=cfdb-export&form='.$formulario.'&hide=Submitted%20Login,Submitted%20From&filter='.implode('%26%26', $filtro).'&enc=TSVUTF16LEBOM&l=c1ae777f998c40eae8495866d37f2038';

						?>

                        <a href="<?php echo $download_link; ?>" target="_blank">Descargar todos los registros encontrados en Excel</a>

                        <?php //the_content(); ?>
                    </div>
                    <div class="clear"></div>
                </section>
            </article>
        </div>

        <!-- end: .content -->

        <script type="text/javascript">
        (function($){
			$(document).ready(function($){
				
				/*$('form #tipo').change(function(){
					$('form').submit();
					return false;
				});*/
				$('form input[type=reset]').click(function(){
					$('form .inputbox, form select').not('[name=tipo]').val('');
					return false;
				});

				$('.fecha').pikaday({
					firstDay: 1,
					format: 'DD-MM-YYYY',
					i18n: {
						previousMonth : 'Mes Anterior',
						nextMonth     : 'Mes Siguiente',
						months        : ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
						weekdays      : ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
						weekdaysShort : ['Dom','Lun','Mar','Mie','Jue','Vie','Sab']
					}
				});

			});
		})(jQuery);
        </script>

        <?php endif; ?>

    <?php endif; ?>

</main>
<!-- end: #exportar -->

<?php get_footer(); ?>