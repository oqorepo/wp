<?php /* Template Name: Obras en desarrollo - Constructora */ ?>
<?php get_header(); ?>

<!-- start: #pagina-constructora -->
<main id="pagina-constructora" class="page" role="main">

	<?php if(have_posts()): the_post(); ?>

        <?php
		// Info General
		$etiqueta = get_field('etiqueta');
		$titulo = get_field('titulo');
		$bajada = get_field('bajada');
		$imagen_superior = get_field('imagen_superior');
		$url_imagen_superior = $imagen_superior['sizes']['slider-home'];

		// Galeria Aerea
		$galeria_aerea = get_field('galeria_aerea');
		?>
        <!-- start: .top -->
        <div class="top fullheight fullpage-section">
            <?php if($url_imagen_superior): ?>
                <div class="fullimg cover" style="background-image: url(<?php echo $url_imagen_superior; ?>);"></div>
            <?php endif; ?>
            <div class="middle aligntop" data-aos="fade-down">
                <div class="grid-container grid-small">
                	<?php if($etiqueta): ?>
                        <h3 class="etiqueta"><?php echo $etiqueta; ?></h3>
                    <?php endif; ?>
                    <?php if($titulo): ?>
                        <h1 class="titulo large white"><?php echo $titulo; ?></h1>
                    <?php endif; ?>
                    <?php if($bajada): ?>
                        <p class="titulo white"><?php echo $bajada; ?></p>
                    <?php endif; ?>
                    <!-- <div class="botones textcenter">
	                    <a href="#obras" class="button outline">OBRAS EN DESARROLLO</a>
                        <a href="#obras-ejecutadas" class="button outline">OBRAS EJECUTADAS</a>
                    </div> -->
                </div>
            </div>
            <a href="#obras" class="next-section hide-on-mobile">Use scroll para navegar<img src="<?php echo EP_THEMEPATH; ?>/images/scrolldown.png"></a>
        </div>
        <!-- end: .top -->

        <?php
		$args = array(
			'posts_per_page' => -1,
			'post_type' => 'obras',
			'suppress_filters' => false,
			'orderby' => 'menu_order',
			'order' => 'ASC',
		);
        $proyectos = get_posts($args);
        if(!empty($proyectos)):
        ?>
            <!-- start: #obras -->
            <section id="obras" class="carrusel-proyectos section fullpage-section" style="padding-top: 30px;">
            	<div class="grid-container grid-medium">
                    <h3 class="titulo textcenter">#OBRAS EN DESARROLLO</h3>

                    <div class="filtro hide-on-desktop">
                        <label>
                            Selecciona el proyecto:
                            <select class="blue">
                                <?php $i = 0; foreach($proyectos as $post): setup_postdata($post); ?>
                                    <option value="carrusel-<?php echo $post->ID; ?>"><?php the_title(); ?></option>
                                <?php $i++; endforeach; ?>
                            </select>
                        </label>
                    </div>

                    <div class="lista grid-40 grid-parent scrollpane hide-on-mobile hide-on-tablet">
                        <?php $i = 0; foreach($proyectos as $post): setup_postdata($post); ?>
                            <?php
                            $descripcion = get_field('descripcion');
                            $galeria = get_field('galeria');
                            if(empty($galeria)) continue;
                            $imagen = $galeria[0]['sizes']['proyecto-galeria-thumb'];
							$class = ($i == 0) ? 'active' : '';
                            ?>
                            <div class="item <?php echo $class; ?>" data-id="carrusel-<?php echo $post->ID; ?>">
                                <div class="info grid-60 grid-parent">
                                    <h4 class="titulo usmall"><?php the_title(); ?></h4>
                                    <p><?php echo $descripcion; ?></p>
                                    <div class="flecha"></div>
                                    <div class="borde"></div>
                                </div>
                                <div class="img cover grid-40 lazy"  data-src="<?php echo $imagen; ?>"><div class="overlay"></div></div>
                                <div class="clear"></div>

                            </div>
                        <?php $i++; endforeach;  wp_reset_postdata(); ?>
                    </div>

                    <div class="galeria grid-60 grid-parent alta">
                    	<?php $i = 0; foreach($proyectos as $post): setup_postdata($post); ?>
                            <?php
							$class = ($i == 0) ? 'active' : '';
							$tipo = get_field('tipo');
                            $ubicacion = get_field('ubicacion');
							$fecha = get_field('fecha');
							$superficie = get_field('superficie');
							$monto_contrato = get_field('monto_contrato');
							$mandante = get_field('mandante');
                            $galeria = get_field('galeria');
                            if(empty($galeria)) continue;
                            ?>
                            <!-- start: .proyecto -->
                            <div class="proyecto carrusel-<?php echo $post->ID; ?> <?php echo $class; ?>">
                            	<div class="slick-slider circle-dots">
									<?php foreach($galeria as $g): ?>
                                    	<?php $class_width = ($g['width'] < $g['height']) ? 'contain': 'cover'; ?>
                                    	<div class="item">
                                        	<img data-lazy="<?php echo $g['sizes']['noticia-int']; ?>" style="display: none;">
                                            <div class="fullimg <?php echo $class_width; ?>"></div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                                <div class="info">
                                	<?php if($monto_contrato): ?>
                                        <div class="grid-20 tablet-grid-25 mobile-grid-50">
                                            <h5>Precio Contrato:</h5>
                                            <p>U.F. <?php echo $monto_contrato; ?></p>
                                        </div>
                                    <?php endif; ?>
                                    <?php if($fecha): ?>
                                        <div class="grid-20 tablet-grid-25 mobile-grid-50">
                                            <h5>Inicio de Obras:</h5>
                                            <p><?php echo $fecha; ?></p>
                                        </div>
                                    <?php endif; ?>
                                    <div class="clear hide-on-tablet hide-on-desktop"></div>
                                	<?php if($ubicacion): ?>
                                        <div class="grid-30 tablet-grid-50">
                                            <h5>Ubicación:</h5>
                                            <p><?php echo $ubicacion; ?></p>
                                        </div>
                                    <?php endif; ?>
                                    <div class="clear hide-on-desktop"></div>
                                    <?php if($superficie): ?>
                                        <div class="grid-15 tablet-grid-25 mobile-grid-50">
                                            <h5>Superficie:</h5>
                                            <p><?php echo $superficie; ?> m&sup2;</p>
                                        </div>
                                    <?php endif; ?>
                                    <?php if($mandante): ?>
                                        <div class="grid-15 tablet-grid-50 mobile-grid-50">
                                            <h5>Mandante:</h5>
                                            <p><?php echo $mandante; ?></p>
                                        </div>
                                    <?php endif; ?>

                                    <div class="clear"></div>
                                </div>
                            </div>
                            <!-- end: .proyecto -->
                        <?php $i++; endforeach;  wp_reset_postdata(); ?>
                    </div>

                    <div class="clear"></div>
                </div>
            </section>
            <!-- end: #obras -->
		<?php endif; wp_reset_postdata(); ?>

		<?php if(!empty($galeria_aerea)): ?>
            <!-- start: #galeria -->
            <section id="galeria" class="section fullpage-section" style="padding-top: 30px;">
                <div class="grid-container grid-medium">
                    <h3 class="titulo textcenter">#GALERÍA VISTA AÉREAS DESDE UN DRON</h3>

                    <div class="lista ep-gallery">
                        <?php $i = 1; foreach($galeria_aerea as $g): ?>
                            <div class="grid-33 tablet-grid-50 np-mobile">
                                <div class="item">
                                    <?php if( $g['galeria_tipo'] == 'video' ): ?>
                                        <?php
                                            preg_match('/src="([^"]+)"/', $g['url_video'], $match);
                                            $id_video = youtube_id($match[1]);
                                         ?>
                                        <a href="https://www.youtube.com/watch?v=<?php echo $id_video; ?>" class="lightbox mfp-iframe thumb"><div class="img fullimg cover" style="background-image: url(<?php echo $g['galeria_imagen']['sizes']['planta-thumb']; ?>);"></div><span class="zoom"><i class="fa fa-play"></i></span></a>
                                    <?php else: ?>
                                        <a href="<?php echo $g['galeria_imagen']['sizes']['ep-large']; ?>" class="lightbox"><div class="img fullimg cover" style="background-image: url(<?php echo $g['galeria_imagen']['sizes']['planta-thumb']; ?>);"></div><span class="zoom"><i class="fa fa-search"></i></span></a>
                                    <?php endif; ?>
                                    <?php if( $g['galeria_titulo'] ): ?>
                                        <h4 class="titulo usmall"><?php echo $g['galeria_titulo']; ?></h4>
                                    <?php endif; ?>
                                    <?php if( $g['galeria_bajada'] ): ?>
                                        <p class="bajada"><?php echo $g['galeria_bajada']; ?></p>
                                    <?php endif; ?>
                                    <div class="clear"></div>

                                </div>
                            </div>
                            <?php if($i % 3 == 0 ) echo '<div class="clear hide-on-tablet"></div>'; ?>
                            <?php if($i % 2 == 0 ) echo '<div class="clear hide-on-desktop"></div>'; ?>
                        <?php $i++; endforeach;  wp_reset_postdata(); ?>
                        <div class="clear"></div>
                    </div>

                    <div class="clear"></div>
                </div>
            </section>
            <!-- end: #galeria -->
        <?php endif; wp_reset_postdata(); ?>

    <?php endif; ?>
</main>
<!-- end: #pagina-constructora -->

<?php get_footer(); ?>
