var CaptchaCallback;
var captcha_ids = new Array();
(function($){
	/* Dom Loaded */
	$(document).ready(function($){
		deBouncer(jQuery,'smartresize', 'resize', 50);
		deBouncer(jQuery,'smartscroll', 'scroll', 25);
		var window_height = $(window).height();
		var width = $(document).width();
		
		var myLazyLoad = new LazyLoad({
			elements_selector: ".lazy"
		});
		
		CaptchaCallback = function() {
			$('.g-recaptcha').each(function(index, el) {
				var id = $(this).attr('id')
				captcha_ids[index] = grecaptcha.render(id, {'sitekey' : '6LeH8DoUAAAAAP9jUE2QUGB1ZXIx8305ihOSjcjE'});
			});
		};
		
		
		$('.fullheight').height(window_height);
		
		if(width > 1024){
			$.stellar({
				horizontalScrolling: false,
				verticalOffset: 40
			});
		}

		
		if(width < 980){
			$('div.texto p').filter(function() {
				return $.trim($(this).text()) === '' && $(this).children().length == 0
			}).remove();
		}
		
		$('#back-to-top').click(function(event) {
			event.preventDefault();
			$('html, body').animate({scrollTop: 0}, 500);
			return false;
		});
		
		$('.page div.top, .page div.menu').localScroll({ offset: 0 });
		

		
/*		$('#fullpage').fullpage({
			sectionSelector: '.fullpage-section',
			scrollBar: true,
			fixedElements: '#header',
			fitToSection: false
			//offsetSections: true,
			//offsetSectionsKey: 'YWx2YXJvdHJpZ28uY29tX2ZZM2IyWm1jMlYwVTJWamRHbHZibk09MWpN'
		});*/
		
				
		$('.tooltip').tooltipster({ position: 'left', contentAsHTML: true, animation: 'fade' });
		$('.tooltip-small').tooltipster({ position: 'left', theme: 'tooltipster-small', contentAsHTML: true, animation: 'grow', maxWidth: 260 });
		
		$('.auto-tooltip-small').each(function(){
			var position = $(this).data('position');
			if(!position) position = 'bottom';
			$(this).tooltipster({ theme: 'tooltipster-small', position: position, contentAsHTML: true, animation: 'grow', maxWidth: 260 });
		});
		
		/* Efecto de Columnas */
		
		$('.fullscreen .section').mouseenter(function(){
			var parent = $(this).parents('.fullscreen');
			parent.find('.section').removeClass('active');
			$(this).addClass('active');
		});
		
		/* Global */
				
		$('#header div.abrir-menu').click(function(){
			$('body').toggleClass('menu-open');
		});
		
		$('#menu .cerrar, #wrapper #overlay').click(function(){
			$('body').removeClass('menu-open');
		});
		
		$('#menu .menu-item-has-children > a').click(function(){
			$(this).parent().toggleClass('open');
			return false;
		});

		$('input[placeholder], textarea[placeholder]').placeholder();
		
		$('ul.tabs').not('.noplugin').each(function(){
			$(this).tabs($(this).next().find('div.tab'), {
				history: false,
				effect: 'fade'
			});
		});
		
		$.extend(true, $.magnificPopup.defaults, {
			tClose: 'Cerrar (Esc)', // Alt text on close button
			tLoading: 'Cargando...', // Text that is displayed during loading. Can contain %curr% and %total% keys
			gallery: {
				tPrev: 'Anterior', // Alt text on left arrow
				tNext: 'Siguiente', // Alt text on right arrow
				tCounter: '%curr% de %total%' // Markup for "1 of 7" counter
			},
			image: {
				tError: '<a href="%url%">La imagen</a> no pudo ser cargada.' // Error message when image could not be loaded
			},
			ajax: {
				tError: '<a href="%url%">El contenido</a> no pudo ser cargado.' // Error message when ajax request failed
			}
		});
			
		$('p.attachment a').magnificPopup({
			type: 'image',
			mainClass: 'my-mfp-zoom-in mfp-bg-black',
			removalDelay: 300,
			closeMarkup: '<i title="%title%" class="mfp-close flaticon-cross97"></i>'
		});	
		
		$('.lightbox').magnificPopup({
			//type: 'iframe',
			mainClass: 'my-mfp-zoom-in mfp-bg-black',
			removalDelay: 300,
			iframe: {
				patterns: {
					youtube: {
						index: 'youtube.com/', 
						id: function(url) {        
							var m = url.match(/[\\?\\&]v=([^\\?\\&]+)/);
							if ( !m || !m[1] ) return null;
							return m[1];
						},
						src: '//www.youtube.com/embed/%id%?autoplay=1&rel=0'
					},
					vimeo: {
						index: 'vimeo.com/', 
						id: function(url) {        
							var m = url.match(/(https?:\/\/)?(www.)?(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/);
							if ( !m || !m[5] ) return null;
							return m[5];
						},
						src: '//player.vimeo.com/video/%id%?autoplay=1'
					}
				}
			}
		});
		
		$('a.lightbox-rrhh, .lightbox-rrhh a').magnificPopup({
			type: 'iframe',
			mainClass: 'my-mfp-zoom-in mfp-bg-black bg-white',
			removalDelay: 300,
		});
		
		$('.ep-gallery').each(function(){
			var vertical = true;
			var vertical_images = $(this).data('vertical-images');
			if(vertical_images == false) vertical = false;
			$(this).magnificPopup({
				delegate: 'a.lightbox',
				type: 'image',
				mainClass: 'my-mfp-zoom-in',
				removalDelay: 300,
				closeBtnInside: true,
				gallery: {
					enabled: true,
					arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir% flaticon-arrow-%dir%"></button>'
				},
				image:{
					verticalFit: vertical,
					titleSrc: 'data-caption',
				},
				mainClass: 'my-mfp-zoom-in mfp-bg-black',
				callbacks: {
					open: function(){
						//overwrite default prev + next function. Add timeout for css3 crossfade animation
						$.magnificPopup.instance.next = function() {
							var self = this;
							self.wrap.removeClass('mfp-image-loaded');
							setTimeout(function() { $.magnificPopup.proto.next.call(self); }, 120);
						};
						$.magnificPopup.instance.prev = function() {
							var self = this;
							self.wrap.removeClass('mfp-image-loaded');
							setTimeout(function() { $.magnificPopup.proto.prev.call(self); }, 120);
						};
					},
					imageLoadComplete: function() {	
						var self = this;
						setTimeout(function() { self.wrap.addClass('mfp-image-loaded'); }, 16);
					},
				}
			});
		});
		
		$('.scrollpane').mCustomScrollbar({
			snapAmount: 60,
			theme: 'inset',
			autoHideScrollbar: false,
			scrollButtons:{enable:false},
			alwaysShowScrollbar: 2,
			keyboard:{scrollAmount: 60 },
			mouseWheel:{deltaFactor: 60, preventDefault: true },
			scrollInertia: 400
		});
		
		/* Proyectos en Venta */
		
		$('#proyectos .slick-slider').slick({
			cssEase: 'ease',
			fade: false,
			arrows: true,
			infinite: false,
			dots: false,
			autoplay: false,
			vertical: false,
			slidesToShow: 4,
			slidesToScroll: 4,
			responsive: [
				{
				breakpoint: 1170,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3
					}
				},
				{
				breakpoint: 980,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
				breakpoint: 767,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				},
			]
		});
		
		$('.carrusel-proyectos .lista div.item').click(function(){
			var parent = $(this).parents('div.tab');
			if(parent.length < 1){
				parent = $(this).parents('.carrusel-proyectos');	
			}
			var id = $(this).data('id');
			parent.find('div.item').removeClass('active');			
			parent.find('.proyecto').removeClass('active');
			parent.find('.proyecto.'+id).addClass('active');
			$(this).addClass('active');
		});		
		
		$('.carrusel-proyectos .galeria .proyecto .slick-slider').slick({
			lazyLoad: 'ondemand',
			cssEase: 'ease',
			fade: true,
			arrows: true,
			infinite: false,
			dots: true,
			autoplay: false,
			speed: 1000,
			autoplaySpeed: 5000,
			pauseOnHover: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			responsive: [
				{
				breakpoint: 980,
					settings: {
						dots: false
					}
				},
			]
		});
		
		$('.carrusel-proyectos div.filtro select').not('.tab').change(function(){
			var parent = $(this).parents('.carrusel-proyectos');
			var id = $(this).val();
			parent.find('div.item').removeClass('active');			
			parent.find('.proyecto').removeClass('active');
			parent.find('.proyecto.'+id).addClass('active');
			//$(this).addClass('active');
		});
		
		$('.carrusel-proyectos .galeria .proyecto .slick-slider').on('lazyLoaded', function (e, slick, image, imageSource) {
			image.attr('src',''); //remove source
			image.next().css('background-image','url("'+imageSource+'")'); 
        });
        
        // Ajax lightbox obras

        $('.lightbox-obras').magnificPopup({
			type: 'ajax',
			mainClass: 'my-mfp-zoom-in mfp-bg-black',
            removalDelay: 300,
            closeOnContentClick: false,
            closeOnBgClick: false,
			callbacks: {
                parseAjax: function(mfpResponse) {
                  // mfpResponse.data is a "data" object from ajax "success" callback
                  // for simple HTML file, it will be just String
                  // You may modify it to change contents of the popup
                  // For example, to show just #some-element:
                  // mfpResponse.data = $(mfpResponse.data).find('#some-element');
              
                  // mfpResponse.data must be a String or a DOM (jQuery) element
              
                  console.log('Ajax content loaded:', mfpResponse);
                },
                ajaxContentAdded: function() {
                  // Ajax content is loaded and appended to DOM
                  console.log(this.content);
                    $('.carrusel-proyectos.ajax .galeria .proyecto .slick-slider').slick({
                        // lazyLoad: 'ondemand',
                        cssEase: 'ease',
                        fade: true,
                        arrows: true,
                        infinite: false,
                        dots: true,
                        autoplay: false,
                        speed: 1000,
                        autoplaySpeed: 5000,
                        pauseOnHover: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        responsive: [
                            {
                            breakpoint: 980,
                                settings: {
                                    dots: false
                                }
                            },
                        ]
                    });
                }
              }
		});

		
		// Obras ejecutadas
		
		$('ul.categorias.tabs li a').click(function(){
			$('.carrusel-proyectos .galeria .proyecto .slick-slider').slick('setPosition');
		});
		
		if($('#obras-ejecutadas ul.tabs').length > 0){
			var api_tabs = $('#obras-ejecutadas ul.tabs').data('tabs');
			$('#obras-ejecutadas.carrusel-proyectos div.filtro select').change(function(){
				var index = parseInt($(this).val());
				api_tabs.click(index);
				var parent = $(this).parents('.carrusel-proyectos');
				var id = $('#obras-ejecutadas.carrusel-proyectos div.panes div.tab:visible select').val();
				parent.find('.proyecto.'+id).addClass('active');
				$('#obras-ejecutadas.carrusel-proyectos .galeria .proyecto .slick-slider').slick('setPosition');
			});
		}		
		
		
		$('.listado form.filtro select').change(function(){
			$(this).parents('form').submit();
		});
		
		$('select.nice-select').niceSelect();
		
		/* Formularios */
		
		$('#formulario a.cerrar').click(function(){
			$(this).parents('#formulario').removeClass('visible');
		});
		
		$('form div.input-wrapper .inputbox').focus(function(){
			$('form div.input-wrapper').removeClass('focus');
			$(this).parents('div.input-wrapper').addClass('focus');
		});
		
		/* Sidebar Noticias */
		
		$('.toggle .desplegar').click(function(){
			var parent = $(this).parent();
			$('.toggle').removeClass('active');
			$('.toggle').not('.active').find('.toggle-content').stop().slideUp();
			$(this).next().stop().slideDown();
			parent.addClass('active');	

		});
		
		$(window).smartresize(function(){
			var window_height = $(window).height();
			$('.fullheight').height(window_height);
		});
		
		/* Ocultar logo al scroll */
		
		var lastId,
		topMenu = $(".page div.menu"),
		topMenuHeight = topMenu.height(),
		menuItems = topMenu.find("ul li a").not('.no-scroll'),
		scrollItems = menuItems.map(function(){
			var item = $($(this).attr("href"));
			if (item.length) { return item; }
		});
		$(window).smartscroll(function(e){
			var y_scroll_pos = window.pageYOffset;
			var fromTop = y_scroll_pos+300;
			var window_height = $(window).height();
			var document_height = $(document).height();
			var footer = document_height - window_height;
			if( fromTop >= window_height){
				
				$('#back-to-top').addClass('visible');
			}else{
				//$('#header').removeClass('hidden-logo');
				$('#back-to-top').removeClass('visible');
			}

			if( y_scroll_pos >= window_height){ // footer
				$('#header').addClass('hidden-logo');	
				
			}else{
				$('#header').removeClass('hidden-logo');
			}
			
			var cur = scrollItems.map(function(){
			if ($(this).offset().top < fromTop)
				return this;
			});
			cur = cur[cur.length-1];
			var id = cur && cur.length ? cur[0].id : "";
			if (lastId !== id) {
				lastId = id;
				menuItems.removeClass("active").filter($("a[href=#"+id+"]")).addClass("active");
			}

		});
		

	});
	$(window).load(function(){
		
		/* Videos */

		$('.has-video .video').each(function(){
			var id = $(this).data('id');
			if(id !== ''){
				$(this).YTPlayer({
					fitToBackground: true,
					videoId: id,
					pauseOnScroll: false,
					mute: true,
					//width: 300,
					callback: function() {
						//video_callbacks(id);
					}
				});
			}
		});
				
		
	});
})(jQuery);