(function($){
	$(document).ready(function(){
		var window_height = $(window).height();
		var document_width = $(document).width();

		var obras = $('#pagina-somos #obras-historicas .slick-slider');
		obras.slick({
			cssEase: 'ease',
			dots: false,
			vertical: true,
			centerMode: 1,
			slidesToShow: 2,
			slidesToScroll: 1,
			speed: 800,
			responsive: [
				{
				breakpoint: 1170,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
				breakpoint: 980,
					settings: {
						vertical: false,
						slidesToShow: 1,
						slidesToScroll: 1
					}
				},
			]
		});

		if(document_width > 980){

			obras.bind('wheel', function(e){
				e.preventDefault();
				if(e.originalEvent.wheelDelta /120 > 0) {
					obras.slick('slickPrev');
				}
				else{
					obras.slick('slickNext');
				}
			});

		}

		obras.on('beforeChange', function(event, slick, currentSlide, nextSlide){

			var contenido = $(this).find('.slick-slide[data-slick-index='+nextSlide+'] div.contenido').html();

			$('#pagina-somos #obras-historicas div.detalle div.middle').html(contenido);
			$('#pagina-somos #obras-historicas div.detalle').addClass('animate');
			setTimeout(function(){
				$('#pagina-somos #obras-historicas div.detalle').removeClass('animate');
			}, 400);
		});


		$(window).smartresize(function(){


		});

	});

})(jQuery);
