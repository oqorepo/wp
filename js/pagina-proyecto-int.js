(function($){
	$(document).ready(function(){
		var window_height = $(window).height();
		var document_width = $(document).width();
		
		

		//$('#proyectos-int div.menu').sticky();
		$('#proyectos-int div.menu').localScroll({ offset: 0, hash: false });
		
		
		/* Eventos */
		
		var dispositivo = 'desktop';
		if(document_width > 767 && document_width < 1025){ dispositivo = 'tablet'; }
		if(document_width < 768){ dispositivo = 'mobile'; }
		
		
		/*$('#ubicacion .direccion a').click(function(){
			ga('send', 'event', 'env-comollegar', proyecto, dispositivo);
		});
		
		$('#sala-ventas a.telefono').click(function(){
			ga('send', 'event', 'env-llamar', proyecto, dispositivo);
		});
		
		$('#sala-ventas a.email').click(function(){
			ga('send', 'event', 'env-clickemail', proyecto, dispositivo);
		});*/
		
		$('#consultas #modelo').change(function(){
			var programa = $(this).find('option:selected').data('programa');
			$('#consultas #programa').val(programa);
		});
		
		$('#proyectos-int #slider').slick({
			cssEase: 'ease',
			fade: false,
			arrows: true,
			infinite: false,
			dots: false,
			autoplay: false,
			speed: 1000,
			autoplaySpeed: 5000,
			pauseOnHover: true,
			slidesToShow: 1,
			slidesToScroll: 1
		});
		
		$('#proyectos-int #plantas .slick-slider.vertical').slick({
			cssEase: 'ease',
			fade: false,
			arrows: true,
			infinite: false,
			dots: false,
			autoplay: false,
			vertical: true,
			slidesToShow: 4,
			slidesToScroll: 1,
			focusOnSelect: true,
			asNavFor: '#proyectos-int #plantas .slick-slider.lista-plantas',
			responsive: [
				{
				breakpoint: 1170,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
				breakpoint: 767,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				},
			]
		});
		
		$('#proyectos-int #plantas .slick-slider.lista-plantas').slick({
			cssEase: 'ease',
			fade: false,
			arrows: false,
			infinite: false,
			dots: false,
			autoplay: false,
			vertical: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			adaptiveHeight: false,
			asNavFor: '#proyectos-int #plantas .slick-slider.vertical',
			responsive: [
				{
				breakpoint: 767,
					settings: {
						arrows: true,
						fade: false,
						adaptiveHeight: true
					}
				},
			]
		});
		
		$('#proyectos-int #plantas div.filtro select').change(function(){
			var value = $(this).val();
			$('#proyectos-int #plantas .slick-slider.lista-plantas').slick('slickGoTo', value);
		
		});
		
		$('#proyectos-int #modelos .slick-slider').slick({
			cssEase: 'ease',
			fade: false,
			arrows: true,
			infinite: false,
			dots: true,
			autoplay: false,
			speed: 1000,
			autoplaySpeed: 5000,
			pauseOnHover: true,
			slidesToShow: 3,
			slidesToScroll: 3,
			responsive: [
				{
				breakpoint: 1170,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
				breakpoint: 767,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				},
			]
		});
		

		
		
		
		
		var parent = $('#proyectos-int #modelos');
		
		if(parent.length > 0){
			$('#proyectos-int #modelos ul.tabs').tabs($('#proyectos-int #modelos div.panes > div'), {
				history: false,
				effect: 'default'
			});
			var tabs = parent.find('ul.tabs').data('tabs');
			tabs.onClick(function(index) {
				var current_pane = tabs.getCurrentPane();
				parent.find('div.panes > div').removeClass('current');
				parent.find('div.slick-slider.slick-initialized').slick('setPosition');
				if($(document).width() > 767){
					$('#proyectos-int #modelos .slick-slider.aligncenter .slick-track').width($(document).width());	
				}
				current_pane.addClass('current');
				
			});
		}
		
		var rows = 2;
		var count = parseInt($('#proyectos-int #galeria').data('count'));
		if(count < 4){
			rows = 1;	
		}
		
		$('#proyectos-int #galeria div.slick-slider').slick({
			cssEase: 'ease',
			fade: false,
			arrows: true,
			infinite: false,
			dots: false,
			autoplay: false,
			autoplaySpeed: 4000,
			speed: 600,
			rows: rows,
			slidesToShow: 3,
			slidesToScroll: 3,
			pauseOnHover: false
		});
		
		$('#proyectos-int #equipamiento div.slick-slider').slick({
			cssEase: 'ease',
			fade: false,
			arrows: true,
			infinite: false,
			dots: false,
			autoplay: false,
			autoplaySpeed: 4000,
			speed: 600,
			slidesToShow: 5,
			slidesToScroll: 5,
			pauseOnHover: false
		});
		
		$('#plantas a.lightbox').magnificPopup({
			type: 'image',
			removalDelay: 300,
			fixedContentPos: true,
			image:{
				verticalFit: false,
				titleSrc: 'data-caption',
			},
			mainClass: 'my-mfp-zoom-in mfp-bg-black',
			callbacks: {
				open: function(){
					
					//overwrite default prev + next function. Add timeout for css3 crossfade animation
					$.magnificPopup.instance.next = function() {
						var self = this;
						self.wrap.removeClass('mfp-image-loaded');
						setTimeout(function() { $.magnificPopup.proto.next.call(self); }, 120);
					};
					$.magnificPopup.instance.prev = function() {
						var self = this;
						self.wrap.removeClass('mfp-image-loaded');
						setTimeout(function() { $.magnificPopup.proto.prev.call(self); }, 120);
					};
				},
				imageLoadComplete: function() {	
					var self = this;
					setTimeout(function() { self.wrap.addClass('mfp-image-loaded'); }, 16);
				},
			}
		});	

		$(window).smartresize(function(){
			var window_height = $(window).height();
			
		});
		
		var lastId,
		topMenu = $("#proyectos-int div.menu"),
		topMenuHeight = topMenu.height(),
		menuItems = topMenu.find("ul li a"),
		scrollItems = menuItems.map(function(){
			var item = $($(this).attr("href"));
			if (item.length) { return item; }
		})	
		
		$(window).smartscroll(function(e){
			var y_scroll_pos = window.pageYOffset;
			var fromTop = y_scroll_pos+300;
			var window_height = $(window).height();
			if( (fromTop) >= 350){
				$('#header').addClass('nombre-visible');
			}else{
				$('#header').removeClass('nombre-visible');	
			}
			
			var cur = scrollItems.map(function(){
			if ($(this).offset().top < fromTop)
				return this;
			});
			cur = cur[cur.length-1];
			var id = cur && cur.length ? cur[0].id : "";
			if (lastId !== id) {
				lastId = id;
				menuItems.removeClass("active").filter($("a[href=#"+id+"]")).addClass("active");
			}
			
		});
		
		$('#proyectos-int #ubicacion div.info a.cerrar').click(function(){
			$(this).parent().fadeOut();
			return false;
		});
		
		var icon_url = $('#options').data('marker');
		var lat = $('#options').data('lat');
		var lng = $('#options').data('lng');
		var zoom = $('#options').data('zoom');
		var content = $('#options').html();
		if(lat && lat && zoom)
			initialize(lat, lng, zoom, icon_url, content);
		
	});
	function initialize(lat, lng, zoom, icon_url, content) {
	
		var latlng = new google.maps.LatLng(lat, lng);
		var myOptions = {
		  zoom: parseInt(zoom),
		  center: latlng,
		  scrollwheel: false,
		  streetViewControl: false,
		  mapTypeControl: false,
		  styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
		};
		map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
		
		//map.setMapTypeId(maptype);
	
		var marker = new google.maps.Marker({
			position: latlng,
			map: map,
			//animation: google.maps.Animation.DROP,
			icon: icon_url
		});

		if(content){
			var infowindow = new google.maps.InfoWindow({
				content: '<div class="infowindow">'+content+'</div>'
			});
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map, marker);
			});
			infowindow.open(map, marker);
			setTimeout(function(){
				map.panBy(-25, -70);
			}, 600);
				
			
		}
		
		google.maps.event.addDomListener(window, 'resize', function(){
			map.setCenter(latlng);
			
		});	
		
	}
})(jQuery);