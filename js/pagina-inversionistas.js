(function($){
	$(document).ready(function(){
		var window_height = $(window).height();
		var document_width = $(document).width();
		
		$('#pagina-inversionistas #cifras .slick-slider').slick({
			cssEase: 'ease',
			fade: false,
			arrows: true,
			infinite: false,
			dots: false,
			autoplay: false,
			slidesToShow: 3,
			slidesToScroll: 3,
			responsive: [
				{
				breakpoint: 1170,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
				breakpoint: 767,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				},
			]
		});

		
	});
})(jQuery);