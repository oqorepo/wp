(function ($) {
    /* Dom Loaded */
    $(document).ready(function ($) {

        $(window).on('load', function () {
            $('.cd-loader').fadeOut('slow', function () {
                $(this).remove();
            });
        });

        if (matchMedia) {
            const mq = window.matchMedia("(min-width: 500px)");
            mq.addListener(WidthChange);
            WidthChange(mq);
        }


        if (window.location.href.indexOf("laguna-de-la-piramide") > -1) {
            $('a[href="#cotizar-3"]').show();
            /* $("h5.titulo").text(function () {
                return $(this).text().replace("DESDE UF 100% vendido", "100% vendido");
            });*/
        } 

        if (window.location.href.indexOf('condominio-atlantico-ii') > -1) {
            $('h5.titulo.green').hide();
        }

        if (window.location.href.indexOf('torreones-de-serena') > -1) {
            $('.proyectos-torreones-de-serena div.menu ul').append('<li><a href="mailto:anavarro@desco.cl" target="_blank">Post Venta</a></li>');
        }

        // media query change
        function WidthChange(mq) {
            if (mq.matches) {
                
                $('.abrir-menu').text('INMOBILIARIA | CONSTRUCTORA').append(' <i class="fa fa-bars"></i>');

                /*if (window.location.pathname == '/') {

                    $('.abrir-menu').text('INMOBILIARIA | CONSTRUCTORA').append(' <i class="fa fa-bars"></i>');
                    
                }*/
            } 
        }

        // $('.proyectos-altomiramar #equipamiento .texto').addClass('grid-50');
        // $('.proyectos-altomiramar #equipamiento .texto').after('<div class="desco-btn grid-50 aos-init aos-animate" data-aos="zoom-out"><a href="https://www.desco.cl/alto_miramar/" target="blank" class="">Tour Virtual</a></div>');

    });
})(jQuery);