(function($){
	$(document).ready(function(){
		var window_height = $(window).height();
		var document_width = $(document).width();

		
		$('.slick-slider.listado').slick({
			cssEase: 'ease',
			fade: false,
			arrows: true,
			infinite: false,
			dots: false,
			autoplay: false,
			speed: 1000,
			autoplaySpeed: 5000,
			pauseOnHover: true,
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [
				{
				breakpoint: 1170,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
				breakpoint: 767,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				},
			]
		});
		
		$('.scrollpane').mCustomScrollbar({
			snapAmount: 60,
			theme: 'dark',
			scrollButtons:{enable:false},
			keyboard:{scrollAmount: 60 },
			mouseWheel:{deltaFactor: 60 },
			scrollInertia: 400
		});
		
		$(window).smartresize(function(){
			var window_height = $(window).height();
			var document_width = $(document).width();
			$('main.page div.top').height(window_height);
			$('main.page div.content').css('margin-top', window_height+'px');
			
		});

	});
		
})(jQuery);