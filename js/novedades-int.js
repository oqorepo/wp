(function($){
	$(document).ready(function(){
		var window_height = $(window).height();
		var document_width = $(document).width();
		
		$('#proyectos-destacados .slick-slider.listado').slick({
			cssEase: 'ease',
			fade: false,
			arrows: true,
			infinite: false,
			dots: false,
			autoplay: false,
			speed: 1000,
			pauseOnHover: true,
			slidesToShow: 1,
			slidesToScroll: 1
		});
		
		$('.relacionados .slick-slider.listado').slick({
			cssEase: 'ease',
			fade: false,
			arrows: true,
			infinite: false,
			dots: false,
			autoplay: false,
			speed: 1000,
			autoplaySpeed: 5000,
			pauseOnHover: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			responsive: [
				{
				breakpoint: 1170,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
				breakpoint: 767,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				},
			]
		});
		

		
		$(window).smartresize(function(){
			var window_height = $(window).height();
			var document_width = $(document).width();
			
		});

	});
		
})(jQuery);