jQuery(document).ready(function($){

	$('.formulario').submit(function(){
		var c = i = 0;
		var form = $(this);
		var document_width = $(document).width();
		var dispositivo = 'desktop';
		if(document_width > 767 && document_width < 1025){ dispositivo = 'tablet'; }
		if(document_width < 768){ dispositivo = 'mobile'; }
		form.find('button[type=submit]').attr('disabled', 'disabled').addClass('loading');
		form.find('.empty').removeClass('empty');
		form.find('[required]').each(function(){
			if(!$(this).val()){
				$(this).addClass('empty');
				c++;
			}else if($(this).attr('name').indexOf('email', 0) > -1 && $(this).val().indexOf('@', 0) == -1){
				$(this).addClass('empty');
				c++;
				i++;
			}
		});

		// IE fix
		$('.ie input[placeholder], .ie textarea[placeholder]').placeholder();
		if(c == 0)
			$.ajax({
				url: form.attr('data-url'),
				type: 'POST',
				beforeSend: function(){ },
				data: $(this).serialize(),
				success: function(data){
					var tipo_form = form.find('input[name=form_title]').val();
					var proyecto = form.find('input[name=proyecto]').val();

					switch(data){
						case 'success':
							form.find('div.msg-exito').fadeIn().delay(7500).fadeOut();
							form.find('input[type=text], input[type=email], textarea, select').val('');
							dataLayer.push({
								'event': 'sendForm',
								'nameForm': tipo_form,
								'statusForm': 'mail_sent'
							});
						break;

						case 'bot':
							alert("El Captcha no es correcto, intente nuevamente");
						break;

						default:
							$('.g-recaptcha').each(function(index, el) {
								grecaptcha.reset(captcha_ids[index]);
							});
							alert("Se ha producido un error intente más tarde");
						break;

					}

					form.find('.submit').attr('disabled', false).removeClass('loading');
					if(data != 'error'){


						/*dataLayer.push({
							'event': 'sendForm',
							'nameForm': tipo_form,
							'statusForm': 'mail_sent'
						});*/
/*						switch(tipo_form){
							case 'Contacto':
								ga('send', 'event', 'env-form', 'general', dispositivo);
							break;
							case 'Servicio al Cliente':
								ga('send', 'event', 'env-form', 'atencion', dispositivo);
							break;
							case 'Proyecto':
								ga('send', 'event', 'env-form', proyecto, dispositivo)
							break;
						}*/
					}else{

					}


				}
			});
		return false;
	});

	$('div.msg-exito a.cerrar, #compartir-email a.cerrar').click(function(){
		$('.formulario').removeClass('enviado');
	});

});
