var map;
jQuery(document).ready(function($){
	var icon_url = $('#options').data('marker');
	var lat = $('#options').data('lat');
	var lng = $('#options').data('lng');
	var zoom = $('#options').data('zoom');
	var content = $('#options').html();
	if(lat && lat && zoom)
		initialize(lat, lng, zoom, icon_url, content);
		
});
function initialize(lat, lng, zoom, icon_url, content) {

    var latlng = new google.maps.LatLng(lat, lng);
    var myOptions = {
      zoom: parseInt(zoom),
      center: latlng,
	  scrollwheel: false,
	  streetViewControl: false,
	  styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#c9c9c9"},{"visibility":"on"}]}]
    };
    map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
	//map.setMapTypeId(maptype);

	var marker = new google.maps.Marker({
		position: latlng,
		map: map,
		//animation: google.maps.Animation.DROP,
		icon: icon_url
	});

	if(content){
		var infowindow = new google.maps.InfoWindow({
			content: '<div class="infowindow">'+content+'</div>'
		});
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map, marker);
		});
		infowindow.open(map, marker);
	}
	
	google.maps.event.addDomListener(window, 'resize', function(){
		map.setCenter(latlng);
	});	
	
}
