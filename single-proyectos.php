<?php get_header(); ?>
<!-- start: #proyectos-int -->
<main id="proyectos-int" class="main" role="main">
	<?php if(have_posts()): the_post(); ?>
    	<?php
		// Info General
		$id_proyecto = get_the_ID();
		$url_proyecto = get_permalink();
		$estado = get_field('estado');

		$logo_proyecto = get_field('logo_proyecto');
		$url_logo_proyecto = $logo_proyecto['sizes']['proyecto-logo'];

		// Slider

		$slider = get_field('slider');

		// Descripcion y Galeria

		$fondo_descripcion = get_field('fondo_descripcion');
		$galeria_proyecto = get_field('galeria_proyecto');

		$bajada = get_field('bajada');
		$texto_proyecto = get_field('texto_proyecto');

		$video_proyecto_loop = get_field('video_proyecto');
		if($video_proyecto_loop) $youtube_id = youtube_id($video_proyecto_loop);

		$video_proyecto_int = get_field('video_proyecto_int');
		if($video_proyecto_int) $youtube_int_id = youtube_id($video_proyecto_int);

		// Plantas

		$plantas = get_field('plantas');

		// Equipamiento

		$fondo_equipamiento = get_field('fondo_equipamiento');
		$texto_equipamiento = get_field('texto_equipamiento');
		$galeria_equipamiento = get_field('galeria_equipamiento');

		// Obra en Vivo

		$fondo_obra = get_field('fondo_obra');
		$subtitulo_obra = get_field('subtitulo_obra');
		$texto_obra = get_field('texto_obra');
		$video_obra = get_field('video_obra');
		$video_obra_id = youtube_id($video_obra);

		// Ubicacion

		$mapa_como_llegar = get_field('mapa_como_llegar');
		$gmap_proyecto = get_field('gmap_proyecto');
		$texto_proyecto = get_field('texto_proyecto');
		$direccion_proyecto = get_field('direccion_proyecto');
		$direccion_proyecto_bajada = get_field('direccion_proyecto_bajada');

		// Sala de ventas

		$sv_telefono = get_field('sv_telefono');
		$sv_telefono2 = get_field('sv_telefono2');
		$sv_email = get_field('sv_email');
		$sv_direccion = get_field('sv_direccion');
		$has_sv = false;
		if($sv_telefono || $sv_email || $sv_direccion){
			$has_sv = true;
		}

		?>

        <div class="menu hide-on-mobile hide-on-tablet" data-aos="fade-down" data-aos-offset="0">
            <ul>
                <?php if( !empty($logo_proyecto) ): ?>
                    <li class="logo hide-on-desktop-sm"><img src="<?php echo $url_logo_proyecto; ?>"></li>
                <?php endif; ?>
                <li><a href="#wrapper">Proyecto</a></li>
                <li><a href="#descripcion">Información</a></li>
                <?php if( !empty($plantas) ): ?>
                    <li><a href="#plantas">Plantas</a></li>
                <?php endif; ?>
                <?php if( !empty($texto_equipamiento) ): ?>
                    <li><a href="#equipamiento">Equipamiento</a></li>
                <?php endif; ?>
                <?php if(!empty($gmap_proyecto)): ?>
                    <li><a href="#ubicacion">Ubicación</a></li>
                <?php endif; ?>
                <li><a href="#consultas">Contacto</a></li>
                <?php if ( is_singular('torreones-de-serena') ): ?>
                     <li><a href="mailto:anavarro@desco.cl" target="_blank">Post Venta</a></li>
                <?php endif; ?>
            </ul>
        </div>

        <?php if( !empty($slider) ): ?>

        <!-- start: #slider -->
        <div id="slider" class="top circle-arrows fullheight fullpage-section" data-section-name="proyecto">
        	<?php $c = 1; foreach($slider as $imagen): ?>
            	<?php
				$youtube_id = '';
				if($imagen['tipo_slider'] == 'video'){
					$youtube_id = youtube_id($imagen['video_loop_slider']);
				}
				?>
            	<div class="item <?php if($youtube_id) echo 'has-video'; ?>">
                    <div class="fullimg cover" style="background-image: url(<?php echo $imagen['imagen_slider']['sizes']['slider-home']; ?>);"></div>
                    <?php if($youtube_id): ?>
                        <div class="video" data-id="<?php echo $youtube_id; ?>" id="video-top-<?php echo $c; ?>-<?php echo $youtube_id; ?>"></div>
                    <?php endif; ?>
                    <div class="caption">
                        <div class="grid-container">
                            <h3 class="titulo medium white"><?php echo $imagen['titulo_slider']; ?></h3>
                            <?php if($imagen['bajada_slider']): ?>
                                <h4 class="titulo large white"><?php echo $imagen['bajada_slider']; ?></h4>
                            <?php endif; ?>
                            <?php if($imagen['enlace_slider']): ?>
                                <a href="<?php echo $imagen['enlace_slider']; ?>" class="button outline white <?php if($youtube_id) echo 'lightbox mfp-iframe'; ?>">Descubre más</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
			<?php $c++; endforeach; ?>
        </div>
        <!-- end: #slider -->

        <?php endif; ?>

    	<!-- start: .content -->
        <div class="content">

			<!-- start: .columns -->
            <div class="fullscreen columns bg-black white fullpage-section">
            	<div class="fullimg cover" style="background-image: url(<?php echo $fondo_descripcion['sizes']['slider-home']; ?>);"></div>
            	<?php if(!empty($galeria_proyecto) ): ?>
                    <!-- start: #galeria -->
                    <section id="galeria" class="section middle grid-50 textcenter" data-count="<?php echo count($galeria_proyecto); ?>">
                    	<h2 class="white button outline">Galería</h2><br><br>
                        <div class="overlay"></div>
                        <div class="hidden">
                            <!-- start: .slick-slider -->
                            <div class="slick-slider ep-gallery">
                                <?php foreach($galeria_proyecto as $imagen): ?>
                                    <!-- start: .item -->
                                    <div class="item">
                                        <?php
                                        $url_imagen = $imagen['sizes']['proyecto-galeria-thumb'];
                                        $url_imagen_hd = $imagen['sizes']['ep-large'];
                                        $caption = $imagen['caption'];
                                        ?>
                                        <div class="fullimg cover" style="background-image: url(<?php echo $url_imagen; ?>);"></div>
                                        <a href="<?php echo $url_imagen_hd; ?>" class="lightbox full-link" data-caption="<?php echo $caption; ?>"></a>
                                        <span class="zoom">+</span>
                                    </div>
                                    <!-- end: .item -->
                                <?php endforeach; ?>
                            </div>
                            <!-- end: .slick-slider -->
                        </div>
                        <div class="clear"></div>
                    </section>
                    <!-- end: #galeria -->
                <?php endif; ?>

                <!-- start: #descripcion -->
                <section id="descripcion" class="section middle textcenter grid-50 active">
                    <h1 class="titulo small white"><?php the_title(); ?></h1>
                    <h2 class="titulo small white"><?php echo $bajada; ?></h2>
                    <div class="overlay"></div>
                    <div class="hidden">
                        <div class="texto">
                            <?php echo $texto_proyecto; ?>
                        </div>
                        <?php if($video_proyecto_int): ?>
                            <a href="<?php echo $video_proyecto_int; ?>" class="button outline white lightbox mfp-iframe">VER VIDEO</a>
                        <?php endif; ?>
                    </div>
                    <div class="clear"></div>
                </section>
                <!-- end: #descripcion -->
            </div>
            <!-- end: .columns -->

            <?php if( !empty($plantas) ): ?>

                <div class="">

                    <!-- start: #plantas -->
                    <section id="plantas" class="section fullpage-section">

						<div class="grid-container" data-aos="fade-up">
                        	<div class="filtro hide-on-desktop">
                            	<h3 class="titulo textleft">#PLANTAS DISPONIBLES</h3>
                            	<label>
                                	Selecciona tu modelo:
                                	<select name="planta" class="blue">
                                        <?php $i = 0; foreach($plantas as $planta): ?>
                                            <option value="<?php echo $i; ?>"><?php echo $planta['nombre_planta']; ?></option>
                                        <?php $i++; endforeach; ?>
                                    </select>
                                </label>
                            </div>
                            <div class="clear"></div>
                        	<div class="slick-slider vertical grid-25 grid-parent hide-on-mobile hide-on-tablet">
                            	<?php foreach($plantas as $planta): ?>
                                    <div class="item">
                                    	<div class="middle">
                                            <h4 class="titulo medium"><?php echo $planta['nombre_planta']; ?></h4>
                                            <h5 class="titulo usmall"><?php echo $planta['bajada_planta']; ?></h5>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                            <div class="slick-slider lista-plantas grid-75 grid-parent">

                            	<?php $i = 0; foreach($plantas as $planta): ?>
                                    <div class="item">
                                    	<div class="grid-40 tablet-grid-40">
                                            <h3 class="titulo large"><?php echo $planta['nombre_planta']; ?></h3>
                                            <?php if($planta['bajada_planta']): ?>
                                                <h4 class="titulo"><?php echo $planta['bajada_planta']; ?></h4>
                                            <?php endif; ?>
                                            <?php if($planta['valor_uf_planta']): ?>
                                                <h5 class="titulo green">DESDE UF <?php echo $planta['valor_uf_planta']; ?></h5>
                                            <?php endif; ?>
                                            <div class="img hide-on-tablet hide-on-desktop">
												<?php
                                                $url_hd = $planta['imagen_planta']['sizes']['ep-large'];
                                                if(! empty($planta['imagen_lightbox_planta']) ) $url_hd = $planta['imagen_lightbox_planta']['sizes']['ep-large'];
                                                ?>
                                                <a href="#zoom-<?php echo $i; ?>" class="lightbox mfp-inline"><img src="<?php echo $planta['imagen_planta']['sizes']['planta-thumb']; ?>"><span class="zoom button"><i class="fa fa-search"></i></span></a>

                                            </div>
                                            <ul>
                                                <?php if( $planta['superficie_int'] ): ?>
                                                    <li><span class="left">Superficie Interior</span> <span class="right"><?php echo $planta['superficie_int']; ?> m&sup2;</span></li>
                                                <?php endif; ?>
                                                <?php if( $planta['superficie_logia'] ): ?>
                                                    <li><span class="left">Superficie Loggia</span> <span class="right"><?php echo $planta['superficie_logia']; ?> m&sup2;</span></li>
                                                <?php endif; ?>
                                                <?php if( $planta['superficie_terraza'] ): ?>
                                                    <li><span class="left">Superficie Terraza</span> <span class="right"><?php echo $planta['superficie_terraza']; ?> m&sup2;</span></li>
                                                <?php endif; ?>
                                                <?php if( $planta['superficie_total'] ): ?>
                                                    <li class="total"><span class="left">Superficie Total</span> <span class="right"><?php echo $planta['superficie_total']; ?> m&sup2;</span></li>
                                                <?php endif; ?>
                                            </ul>
                                            <a href="#cotizar-<?php echo $i; ?>" class="lightbox mfp-inline button outline black">COTIZAR</a>

                                        </div>
                                        <div class="grid-60 tablet-grid-60 img hide-on-mobile">
                                        	<?php
											$url_hd = $planta['imagen_planta']['sizes']['ep-large'];
											if(! empty($planta['imagen_lightbox_planta']) ) $url_hd = $planta['imagen_lightbox_planta']['sizes']['ep-large'];
											?>
                                        	<a href="#zoom-<?php echo $i; ?>" class="lightbox mfp-inline"><img src="<?php echo $planta['imagen_planta']['sizes']['planta-thumb']; ?>"><span class="zoom button"><i class="fa fa-search"></i></span></a>

                                        </div>
                                        <div class="clear"></div>

                                        <!-- start: #cotizar -->
                                        <div id="cotizar-<?php echo $i; ?>" class="lightbox-inline mfp-hide">
                                        	<div class="grid-container grid-small section">
                                                <div class="grid-40 tablet-grid-40 info np-mobile">
                                                    <h3 class="titulo green">COTIZAR <?php echo $planta['nombre_planta']; ?></h3>
                                                    <h4 class="titulo gray"><?php echo $planta['bajada_planta']; ?></h4>
                                                    <?php if($planta['valor_uf_planta']): ?>
                                                        <h5 class="titulo gray">DESDE UF <?php echo $planta['valor_uf_planta']; ?></h5>
                                                    <?php endif; ?>
                                                    <ul>
                                                        <?php if( $planta['superficie_int'] ): ?>
                                                            <li><span class="left">Superficie Interior</span> <span class="right"><?php echo $planta['superficie_int']; ?> m&sup2;</span></li>
                                                        <?php endif; ?>
                                                        <?php if( $planta['superficie_logia'] ): ?>
                                                            <li><span class="left">Superficie Loggia</span> <span class="right"><?php echo $planta['superficie_logia']; ?> m&sup2;</span></li>
                                                        <?php endif; ?>
                                                        <?php if( $planta['superficie_terraza'] ): ?>
                                                            <li><span class="left">Superficie Terraza</span> <span class="right"><?php echo $planta['superficie_terraza']; ?> m&sup2;</span></li>
                                                        <?php endif; ?>
                                                        <?php if( $planta['superficie_total'] ): ?>
                                                            <li class="total"><span class="left">Superficie Total</span> <span class="right"><?php echo $planta['superficie_total']; ?> m&sup2;</span></li>
                                                        <?php endif; ?>
                                                    </ul>
                                                </div>
                                                <div class="grid-60 tablet-grid-60 img np-mobile">
                                                    <form action="send.php" data-url="<?php echo site_url(); ?>/wp-admin/admin-ajax.php" method="post" class="formulario">
                                                        <p>Completa los siguientes campos y nos comunicaremos a la brevedad</p>
                                                        <div class="input-wrapper grid-50 tablet-grid-50 first">
                                                            <input type="text" name="nombre" class="inputbox" placeholder="Nombre Completo *" required>
                                                        </div>
                                                        <div class="input-wrapper grid-50 tablet-grid-50 last">
                                                            <input type="email" name="email" class="inputbox" placeholder="Email *" required>
                                                        </div>
                                                        <div class="input-wrapper grid-50 tablet-grid-50 first">
                                                            <input type="text" name="telefono" class="inputbox" placeholder="Teléfono">
                                                        </div>
                                                        <div class="input-wrapper grid-50 tablet-grid-50 last">
                                                            <input type="text" name="rut" class="inputbox" placeholder="Rut *" required>
                                                        </div>
                                                        <div class="clear"></div>
                                                        <div class="input-wrapper">
                                                            <textarea name="comentario" placeholder="Comentario"></textarea>
                                                        </div>
                                                        <input type="hidden" name="proyecto" value="<?php the_title(); ?>">
                                                        <input type="hidden" name="proyecto_id" value="<?php echo $id_proyecto; ?>">
                                                        <input type="hidden" name="nonce" value="<?php echo wp_create_nonce("contacto"); ?>">
                                                        <input type="hidden" name="modelo" value="<?php echo $planta['nombre_planta']; ?>">
                                                        <input type="hidden" name="programa" value="<?php echo $planta['bajada_planta']; ?>">
                                                        <input type="hidden" name="form_title" value="Cotizar Modelo">
                                                        <input type="hidden" name="action" value="ajax_contact">

                                                        <div class="msg-exito" style="color: #333;">
                                                            <p><i class="fa fa-check-square-o"></i> Tu mensaje ha sido enviado, te responderemos a la brevedad.</p>
                                                        </div>
                                                        <div class="captcha grid-65 grid-parent">
                                                            <div id="recaptcha-cotizar-<?php echo $i; ?>" class="g-recaptcha" data-sitekey="6LeH8DoUAAAAAP9jUE2QUGB1ZXIx8305ihOSjcjE"></div>
                                                        </div>
                                                        <button type="submit" class="submit button outline white icon btn-desco">
                                                            <span class="default">ENVIAR</span>
                                                            <span class="loading">ENVIANDO</span>
                                                        </button>
                                                        <div class="clear"></div>
                                                        <p><small>(*) Campos obligatorios</small></p>
                                                    </form>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                        <!-- end: #cotizar -->

                                        <!-- start: #zoom -->
                                        <div id="zoom-<?php echo $i; ?>" class="lightbox-inline mfp-hide">
                                        	<div class="grid-container medium section">
                                                <div class="grid-45 tablet-grid-45 info">
                                                    <h3 class="titulo green"><?php echo $planta['nombre_planta']; ?></h3>
                                                    <h4 class="titulo gray"><?php echo $planta['bajada_planta']; ?></h4>
                                                    <ul class="hide-on-mobile">
                                                        <?php if( $planta['superficie_int'] ): ?>
                                                            <li><span class="left">Superficie Interior</span> <span class="right"><?php echo $planta['superficie_int']; ?> m&sup2;</span></li>
                                                        <?php endif; ?>
                                                        <?php if( $planta['superficie_logia'] ): ?>
                                                            <li><span class="left">Superficie Loggia</span> <span class="right"><?php echo $planta['superficie_logia']; ?> m&sup2;</span></li>
                                                        <?php endif; ?>
                                                        <?php if( $planta['superficie_terraza'] ): ?>
                                                            <li><span class="left">Superficie Terraza</span> <span class="right"><?php echo $planta['superficie_terraza']; ?> m&sup2;</span></li>
                                                        <?php endif; ?>
                                                        <?php if( $planta['superficie_total'] ): ?>
                                                            <li class="total"><span class="left">Superficie Total</span> <span class="right"><?php echo $planta['superficie_total']; ?> m&sup2;</span></li>
                                                        <?php endif; ?>
                                                    </ul>
                                                    <br class="hide-on-tablet hide-on-desktop">
                                                    <a href="#cotizar-<?php echo $i; ?>" class="lightbox mfp-inline button outline white btn-desco">COTIZAR</a>

                                                </div>
                                                <div class="grid-55 tablet-grid-55 img textcenter">
                                                    <img data-lazy="<?php echo $planta['imagen_lightbox_planta']['sizes']['large']; ?>" class="fullwidth lazy">
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                        <!-- end: #zoom -->

                                    </div>
                                <?php $i++; endforeach; ?>
                            </div>
                        </div>

                    </section>
                    <!-- end: #plantas -->
                </div>

            <?php endif; ?>

            <?php $promociones = get_field('promociones'); ?>

            <?php if( !empty($promociones) && get_the_ID() == 54 ): ?>
                <?php
                $url_logo_promocion = $logo_proyecto['url'];
                $promocion_logo = get_field('promocion_logo');
                if( !empty($promocion_logo) ){
                    $url_logo_promocion = $promocion_logo['url'];
                }
                ?>
            <!-- start: #promociones -->
                <section id="promociones" class="section fullpage-section grid-container grid-medium">
                    <div class="middle">
                        <h2 class="titulo medium">#PROMOCIONES</h2>
                        <div class="listado" data-aos="fade-right">
                            <div class="grid-45 grid-parent hide-on-mobile">
                                <article class="item large">
                                    <div class="logo">
                                        <a href="#promociones" style="background-image: url('<?php echo $url_logo_promocion; ?>');"></a>
                                    </div>
                                </article>
                            </div>
                            <div class="grid-55 grid-parent right">
                                <?php $i = 0; foreach($promociones as $promo): ?>                                      
                                    <article class="item small">
                                        <a href="#promo-<?php echo $i; ?>" class="lightbox mfp-inline">
                                            <div class="img grid-25 tablet-grid-25 grid-parent hide-on-mobile cover" style="background-image: url(<?php echo $promo['promocion_imagen']['sizes']['noticia-int']; ?>);"></div>
                                        </a>
                                        <div class="caption grid-75 tablet-grid-75">
                                            <h4 class="titulo usmall"><a href="#promo-<?php echo $i; ?>" class="lightbox mfp-inline"><?php echo $promo['promocion_titulo']; ?></a></h4>
                                            <p class="bajada"><?php echo $promo['promocion_bajada']; ?></p>
                                        </div>
                                        <div class="clear"></div>
                                        <!-- start: .lightbox-promo -->
                                        <div id="promo-<?php echo $i; ?>" class="lightbox-promo mfp-hide ajax grid-container grid-small grid-parent np-tablet">
                                            <div class="grid-50 tablet-grid-40 texto np-mobile">
                                                <h4 class="titulo small"><?php echo $promo['promocion_titulo']; ?></h4>
                                                <p class="bajada"><?php echo $promo['promocion_bajada']; ?></p>
                                                <?php echo $promo['promocion_texto']; ?>
                                            </div>
                                            <div class="grid-50 tablet-grid-60 img grid-parent">
                                                <img src="<?php echo $promo['promocion_imagen']['url']; ?>">
                                            </div>
                                        </div>
                                        <!-- end: .lightbox-promo -->
                                    </article>
                                <?php $i++; endforeach; ?>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- end: #promociones -->
            <?php endif; ?>

            <?php if($texto_equipamiento): ?>

                <div class="">


                    <!-- start: #equipamiento -->
                    <section id="equipamiento" class="fullpage-section">
                    	<div class="fullimg cover" style="background-image: url(<?php echo $fondo_equipamiento['sizes']['slider-home']; ?>);"></div>
                    	<div class="grid-container section">
                            <div class="texto" data-aos="zoom-out">
                                <?php echo $texto_equipamiento; ?>
                                <?php if( get_the_ID() == 251 ): // Solo Alto Miramar ID: 251 ?>
                                    <a href="https://www.desco.cl/alto_miramar/" target="blank" class="button">Tour Virtual</a>
                                <?php endif; ?>
                            </div>
                            <div class="caja_button" data-aos="zoom-out">
                                <a href="#consultas" class="button outline white">Coordina tu visita</a>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <?php if( !empty($galeria_equipamiento) ): ?>
                            <!-- start: .slick-slider -->
                            <div class="slick-slider ep-gallery circle-arrows">
                                <?php foreach($galeria_equipamiento as $imagen): ?>
                                    <!-- start: .item -->
                                    <div class="item">
                                        <?php
                                        $url_imagen = $imagen['sizes']['proyecto-galeria-thumb'];
                                        $url_imagen_hd = $imagen['sizes']['ep-large'];
                                        $caption = $imagen['caption'];
                                        ?>
                                        <div class="fullimg cover" style="background-image: url(<?php echo $url_imagen_hd; ?>);"></div>
                                        <a href="<?php echo $url_imagen_hd; ?>" class="lightbox full-link" data-caption="<?php echo $caption; ?>"></a>
                                        <span class="zoom">+</span>
                                    </div>
                                    <!-- end: .item -->
                                <?php endforeach; ?>
                            </div>
                            <!-- end: .slick-slider -->
                        <?php endif; ?>
                    </section>
                    <!-- end: #equipamiento -->
                </div>

            <?php endif; ?>

            <?php if($texto_obra && $video_obra): ?>

                <div class="fullscreen fullpage-section">

                    <!-- start: #obra -->
                    <section id="obra" class="middle">
                    	<div class="fullimg cover" style="background-image: url(<?php echo $fondo_obra['sizes']['slider-home']; ?>);"></div>
                    	<div class="grid-container section">
                        	<div class="left grid-40">
                                <h3 class="titulo large white">OBRA EN VIVO</h3>
                                <h4 class="titulo small white"><?php echo $subtitulo_obra; ?></h4>
                                <div class="texto white">
                                    <?php echo $texto_obra; ?>
                                </div>
                            </div>
                            <div class="right grid-60 grid-parent" data-aos="fade-left">
                                <iframe id="video" width="100%" height="350" src="//www.youtube.com/embed/<?php echo $video_obra_id; ?>?rel=0&controls=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>

                        <div class="clear"></div>
                    </section>
                    <!-- end: #obra -->
                </div>

            <?php endif; ?>

			<?php if( !empty($gmap_proyecto) ): $direccion_proyecto = get_field('direccion_proyecto'); ?>
                <?php
                $zoom = 15;
                $zoom_gmap = get_field('zoom_gmap');
                if($zoom_gmap) $zoom = $zoom_gmap;
                ?>
                <!-- start: #ubicacion -->
                <section id="ubicacion" class="fullpage-section fullscreen" style="height:400px;">
                    <!--<h3 class="titulo large">Ubicación</h3>-->
                    <div id="map_canvas"></div>
                    <?php if($direccion_proyecto): ?>
                        <div class="info hide-on-mobile">
                            <a class="cerrar"><i class="flaticon-close"></i></a>
                            <h4 class="titulo small white"><?php echo $direccion_proyecto; ?></h4>
                            <?php if($direccion_proyecto_bajada): ?>
                                <p class="titulo usmall white"><?php echo $direccion_proyecto_bajada; ?></p>
							<?php endif; ?>
							<?php if(!empty($mapa_como_llegar)): ?>
                                <p class="ep-gallery"><a href="<?php echo $mapa_como_llegar['sizes']['ep-large']; ?>" class="button outline lightbox" data-caption="<?php echo $direccion_proyecto; ?>" style="padding-top:10px;padding-bottom:10px;">CÓMO LLEGAR<!-- <img src="<?php echo EP_THEMEPATH; ?>/images/icono-gmap.png" class="alignmiddle">--></a></p>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                    <div id="options" style="display: none;" data-lat="<?php echo $gmap_proyecto['lat']; ?>" data-lng="<?php echo $gmap_proyecto['lng']; ?>" data-zoom="<?php echo $zoom; ?>" data-marker="<?php echo EPCL_THEMEPATH; ?>/images/pin-mapa.png">
                        <?php if($direccion_proyecto): ?>
                            <p><?php echo $direccion_proyecto; ?></p>
                        <?php elseif($sv_direccion): ?>
                            <p><?php echo $sv_direccion; ?></p>
                        <?php endif; ?>
                    </div>
                    <div class="clear"></div>
                    
                </section>
                <!-- end: #ubicacion -->
            <?php endif; ?>
            <!-- start: #consultas -->
            <section id="consultas" class="grid-container section fullpage-section">
                <div id="formulario" class="grid-45 tablet-grid-50 grid-parent" data-aos="fade-right" data-aos-offset="300">
                    <form action="send.php" data-url="<?php echo site_url(); ?>/wp-admin/admin-ajax.php" method="post" class="formulario">
                        <h3 class="titulo">Coordina tu Visita</h3>
                        <p>Completa los siguientes campos y nos comunicaremos a la brevedad</p>
                        <div class="input-wrapper grid-50 tablet-grid-50 first">
                            <input type="text" name="nombre" class="inputbox" placeholder="Nombre Completo *" required>
                        </div>
                        <div class="input-wrapper grid-50 tablet-grid-50 last">
                            <input type="email" name="email" class="inputbox" placeholder="Email *" required>
                        </div>
                        <div class="input-wrapper grid-50 tablet-grid-50 first">
                            <input type="text" name="telefono" class="inputbox" placeholder="Teléfono">
                        </div>
                        <div class="input-wrapper grid-50 tablet-grid-50 last">
                            <input type="text" name="rut" class="inputbox" placeholder="Rut *" required>
                        </div>
                        <div class="input-wrapper">
                            <select name="modelo" id="modelo" required>
                                <option value="">Selecciona un Programa *</option>
                                <?php $i = 0; foreach($plantas as $planta): ?>
                                    <option value="<?php echo $planta['nombre_planta']; ?>" data-programa="<?php echo $planta['bajada_planta']; ?>"><?php echo $planta['nombre_planta'].' | '.$planta['bajada_planta']; ?></option>
                                <?php $i++; endforeach; ?>
                            </select>
                            <input type="hidden" id="programa" name="programa">
                        </div>
                        <div class="clear"></div>
                        <div class="input-wrapper">
                            <textarea name="comentario" placeholder="Comentario"></textarea>
                        </div>
                        <input type="hidden" name="proyecto" value="<?php the_title(); ?>">
                        <input type="hidden" name="proyecto_id" value="<?php echo $id_proyecto; ?>">
                        <input type="hidden" name="nonce" value="<?php echo wp_create_nonce("contacto"); ?>">
                        <input type="hidden" name="form_title" value="Proyecto">
                        <input type="hidden" name="action" value="ajax_contact">

                        <div class="msg-exito">
                            <p><i class="fa fa-check-square-o"></i> Tu mensaje ha sido enviado, te responderemos a la brevedad.</p>
                        </div>
                        <div class="captcha grid-65 grid-parent">
                            <div id="recaptcha-proyecto" class="g-recaptcha" data-sitekey="6LeH8DoUAAAAAP9jUE2QUGB1ZXIx8305ihOSjcjE"></div>
                        </div>
                        <button type="submit" class="submit button white icon">
                            <span class="default">ENVIAR</span>
                            <span class="loading">ENVIANDO</span>
                        </button>
                        <div class="clear"></div>
                        <p><small>(*) Campos obligatorios</small></p>
                    </form>
                </div>
                <?php if( $has_sv == true ): ?>
                    <!-- start: #sala-ventas -->
                    <section id="sala-ventas" class="grid-55 tablet-grid-50 grid-parent">
                        <div class="box border-effect">

                            <div class="grid-60 np-mobile">
                                <?php if($sv_direccion): ?>
                                    <div class="item">
                                        <h3 class="titulo usmall green">Sala de Ventas:</h3>
                                        <div class="info">
                                            <?php echo $sv_direccion; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if($sv_email): ?>
                                    <div class="item">
                                        <h3 class="titulo usmall green">Email</h3>
                                        <div class="info">
                                            <a href="mailto:<?php echo antispambot($sv_email); ?>" class="email" target="_blank"><?php echo antispambot($sv_email); ?></a>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>

                            <div class="grid-40 np-mobile">
                                <?php if($sv_telefono || $sv_telefono2): ?>
                                    <div class="item">
                                        <h3 class="titulo usmall green">Teléfono: </h3>
                                        <div class="info">
                                        	<?php if( $sv_telefono2 ): ?>
                                                <a href="tel:<?php echo str_replace(array(' ', '(', ')'), '', $sv_telefono2); ?>" class="telefono hide-on-tablet hide-on-desktop"><i class="fa fa-whatsapp"></i>&nbsp;<?php echo $sv_telefono2; ?></a>
                                                <span class="hide-on-mobile"><i class="fa fa-whatsapp"></i>&nbsp;<?php echo $sv_telefono2; ?></span><br>
                                            <?php endif; ?>
                                            <a href="tel:<?php echo str_replace(array(' ', '(', ')'), '', $sv_telefono); ?>" class="telefono hide-on-tablet hide-on-desktop"><i class="fa fa-phone"></i>&nbsp;<?php echo $sv_telefono; ?></a>
                                            <span class="hide-on-mobile"><i class="fa fa-phone"></i>&nbsp;<?php echo $sv_telefono; ?></span>


                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>

                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                        <p class="legal" style="display: none;">Todas las imágenes y fotos contenidas en el presente sitio web fueron elaboradas con fines ilustrativos y no constituyen necesariamente a una representación exacta de la realidad. Su único objetivo es mostrar una caracterización general del proyecto y no cada uno de sus detalles.</p>
                        <p class="legal">Las características del proyecto pueden sufrir modificaciones sin previo aviso. Lo anterior se informa en virtud de lo señalado en la Ley No. 19.496. Se deja expresamente establecido que el metraje señalado es aproximado y corresponde a metros totales y no a metros útiles municipales.</p>
                    </section>
                    <!-- end: #sala-ventas -->
                <?php endif; ?>
                <div class="clear"></div>
            </section>
            <!-- end: #consultas -->

        </div>
        <!-- end: .content -->
    <?php endif; ?>
</main>
<!-- end: #proyectos-int -->
<?php get_footer(); ?>
