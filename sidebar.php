<?php
if( is_page_template('page-templates/somos.php') || is_singular('desco-medios') )
	$tipo = 'desco-medios';
else
	$tipo = 'post';	
?>

<!-- start: #sidebar -->
<aside id="sidebar" role="complementary" class="grid-30 tablet-grid-30 np-mobile">
	<?php if( !is_page_template('page-templates/somos.php') && !is_singular('desco-medios')  ): ?>
		<?php
        $args = array('posts_per_page' => 4, 'post__not_in' => array($post->ID), 'post_type' => 'post', 'order' => 'DESC', 'suppress_filters' => false);
        $novedades = get_posts($args);
        ?>	
        
        <?php if( !empty($novedades) ): ?>
            <div class="widget ult-noticias">
                <h4 class="titulo usmall">Deberías leer</h4>
                <?php foreach($novedades as $post): setup_postdata($post); ?>
                    <article class="item">
                        <?php if(has_post_thumbnail()): ?>
                            <div class="grid-25 tablet-grid-25 mobile-grid-25 grid-parent scale-effect">
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('noticia-sidebar'); ?></a>
                            </div>
                        <?php endif; ?>
                        <div class="info <?php if(has_post_thumbnail()) echo 'grid-75 tablet-grid-75 mobile-grid-75'; ?>">
                            <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                        </div>
                        <div class="clear"></div>
                    </article>
                <?php endforeach; wp_reset_postdata(); ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <?php
    $args = array('posts_per_page' => -1, 'post_type' => $tipo, 'order' => 'DESC', 'suppress_filters' => false);
    $novedades_archivo = get_posts($args);
    $prev_year = $this_year = '';
    ?>
    <?php if(!empty($novedades_archivo) ): ?>
        <div class="widget archive">
            <div class="toggle active first">
                <h4 class="titulo usmall desplegar">NOTICIAS AÑO <?php echo get_the_date('Y', $novedades_archivo[0]->ID); ?><i class="flaticon-down-arrow"></i></h4>
                <div class="toggle-content">
                    <?php foreach($novedades_archivo as $post): setup_postdata($post); $this_year = get_the_date('Y'); ?>
                        <?php
                            if($prev_year != $this_year && $prev_year)
                                echo '
                                    </div>
                                    </div>
                                    <div class="toggle">
                                        <h4 class="titulo usmall desplegar">NOTICIAS AÑO '.$this_year.' <i class="flaticon-down-arrow"></i></h4>
                                        <div class="toggle-content">';
                        ?>
                        <article class="item">
                            <time datetime="<?php the_time('Y/m/d'); ?>"><?php the_time('d-m-Y'); ?></time>
                            <h6><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
                        </article>
                    <?php $prev_year = $this_year; endforeach; wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</aside>
<!-- end: #sidebar -->