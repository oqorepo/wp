<?php the_post(); ?>

<?php
$descripcion = get_field('descripcion');
$tipo = get_field('tipo');
$ubicacion = get_field('ubicacion');
$fecha = get_field('fecha');
$superficie = get_field('superficie');
$cantidad = get_field('cantidad');
$galeria = get_field('galeria');
?>

<!-- start: #obras -->
<section id="obras" class="carrusel-proyectos ajax grid-container grid-small grid-parent np-tablet">

        <div class="lista grid-40 tablet-grid-33 grid-parent">
            <div class="item">
                <div class="info">
                    <h4 class="titulo usmall"><?php the_title(); ?></h4>
                    <p class="hide-on-mobile"><?php echo $descripcion; ?></p>
                </div>
            </div>
        </div>

        <div class="galeria grid-60 tablet-grid-66 grid-parent alta">
            

                <!-- start: .proyecto -->
                <div class="proyecto active">
                    <div class="slick-slider circle-dots">
                        <?php foreach($galeria as $g): ?>
                            <?php $class_width = ($g['width'] < $g['height']) ? 'contain': 'cover'; ?>
                            <div class="item">
                                <div class="fullimg <?php echo $class_width; ?>" style="background-image: url('<?php echo $g['sizes']['large']; ?>')"></div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="info">
                        <?php if($ubicacion): ?>
                            <div class="grid-35 tablet-grid-50 mobile-grid-65">
                                <h5>Ubicación:</h5>
                                <p><?php echo $ubicacion; ?></p>
                            </div>
                        <?php endif; ?>
                        <?php if($fecha): ?>
                            <div class="grid-10 tablet-grid-50 mobile-grid-35">
                                <h5>Año:</h5>
                                <p><?php echo $fecha; ?></p>
                            </div>
                        <?php endif; ?>
                        <div class="clear hide-on-tablet hide-on-desktop"></div>
                        <?php if($superficie): ?>
                            <div class="grid-20 tablet-grid-50 mobile-grid-50">
                                <h5>Sup. Edificada:</h5>
                                <p><?php echo $superficie; ?></p>
                            </div>
                        <?php endif; ?>
                        <?php if($cantidad): ?>
                            <div class="grid-20 tablet-grid-50 mobile-grid-50">
                                <h5>Departamentos:</h5>
                                <p><?php echo $cantidad; ?></p>
                            </div>
                        <?php endif; ?>
                        <div class="clear"></div>
                    </div>
                </div>
                <!-- end: .proyecto -->

        </div>

        <div class="clear"></div>
</section>
<!-- end: #obras -->

<script type="text/javascript" src="<?php echo EP_THEMEPATH; ?>/js/slick/slick.min.js"></script>
<script type="text/javascript">
(function($){
    $(window).load(function($){
        

        
    }); 
})(jQuery);
</script>
