<?php get_header(); ?>
<!-- start: #page -->
<main id="page" class="novedades" role="main">
    <!-- start: #novedades -->
    <section id="novedades">
        
        <?php get_template_part('partials/categorias-novedades'); ?>
        
        <div class="listado" data-aos="fade-up" data-aos-delay="600">
            <?php while (have_posts()): the_post(); ?>
                <article class="item grid-33 tablet-grid-50 grid-parent <?php if($youtube_id) echo 'has-video'; ?>">
                    <?php the_post_thumbnail('proyecto-thumb'); ?>
                    <div class="video" data-id="<?php echo $youtube_id; ?>" id="video-<?php echo $youtube_id; ?>"></div>
                    <a href="<?php the_permalink(); ?>" class="full-link"></a>
                    <div class="sombra"></div>
                    <div class="overlay"></div>
                    <div class="caption">
                        <h3 class="titulo border small"><?php the_title(); ?></h3>
                        <p><time datetime="<?php the_time('Y/m/d'); ?>"><?php the_time('d.m.Y'); ?></time></p>
                        <?php if($ciudad): ?>
                            <p><?php echo $ciudad; ?></p>
                        <?php endif; ?>
                    </div>
                    <div class="acciones">
                        <a href="<?php the_permalink(); ?>" class="button white icon">MÁS INFORMACIÓN <i class="flaticon-arrow-line-right"></i></a>
                    </div>
                </article> 
            <?php endwhile; ?>
            <div class="clear"></div>
        </div>
        
		<?php ep_pagination(); ?>
        <div class="clear"></div>
        
    </section>
    <!-- end: #novedades -->
    
</main>
<!-- end: #page -->
<?php get_footer(); ?>