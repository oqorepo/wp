<?php
if ( post_password_required() )
	return;
$count = 0;
function ep_comments_callback($comment, $args, $depth) {
	global $count;
	$count++;
	$avatar = get_avatar($comment, 144);
	$class = (!$avatar) ? ' no-avatar' : '';
?>
    <li <?php comment_class('animated opacity0 count-'.$count.$class); ?> id="comment-<?php comment_ID() ?>" data-animation="fadeInUp">
    	<?php if($avatar): ?>
            <div class="avatar"><?php echo $avatar?></div>
        <?php endif; ?>
        <div class="right">
            <cite class="comment-author"><?php comment_author_link(); ?></cite>
            <?php edit_comment_link(); ?>
            <span class="date"><?php comment_date() ?></span>
            <div class="clear10"></div>
            <div class="text">
                <?php if ($comment->comment_approved == '0') : ?>
                    <p><?php _e( 'Your comment is awaiting moderation.', 'ep-theme');?></p>
                <?php endif; ?>
                <?php comment_text() ?>
				<?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
            </div>
        </div>
        <div class="clear"></div>
    </li>
<?php
}
?>
<!-- start: #comments -->
<div id="comments" class="boxed-content">

	<?php if ( have_comments() ) : ?>
		<h3 class="title">
			<?php printf( _n( 'One Comment', '%1$s Comments', get_comments_number(), 'ep-theme'), number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
			?>
		</h3>
		<!-- start: .commentlist -->
		<ol class="commentlist">
        	<?php wp_list_comments( array( 'callback' => 'ep_comments_callback' ) ); ?>
		</ol>
        <!-- end: .commentlist  -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
        	<div class="clear10"></div>
        	<!-- start: #comment-nav -->
            <nav id="comment-nav">
                <!--<h4 class="assistive-text"><?php  _e('Comment Navigation', 'ep-theme'); ?></h4>-->
                <div class="nav-previous alignleft"><?php previous_comments_link( '&larr;'. __('Older Comments', 'ep-theme') ); ?></div>
                <div class="nav-next alignright"><?php next_comments_link(__('Newer Comments', 'ep-theme') .'&rarr;'); ?></div>
                <div class="clear"></div>
            </nav>
            <!-- end: #comment-nav -->
		<?php endif; ?>
        
		<?php if ( ! comments_open() && get_comments_number() ) : ?>
            <p><?php _e('Comments are closed.', 'ep-theme'); ?></p>
		<?php endif; ?>

	<?php endif; // have_comments() ?>
	<?php
		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );
		$fields =  array(
			'author' =>  '<div class="left""><input id="author" name="author" type="text" placeholder="' . __('Name', 'ep-theme') . '" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />',
			'email' =>   '<input id="email" name="email" type="text" placeholder="' . __('Email',  'ep-theme') . '" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />',
			'url' =>     '<input id="url" name="url" type="text" placeholder="' . __('Website',  'ep-theme'). '" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></div>',
		);
		$comments_args = array(
			'fields' => $fields,
			'comment_field' => '<textarea id="comment" name="comment" aria-required="true" rows="10" placeholder="'.__( 'Comment', 'ep-theme').'"></textarea>',
			'must_log_in' => '<p class="must-log-in"><a href="' . wp_login_url( apply_filters( 'the_permalink', get_permalink() ) ) . '">'.  __('Log In', 'ep-theme') .'</a></p>',
			'comment_form_top' => '',
			'comment_notes_after' => '',
			'comment_notes_before' => ''
		);
		comment_form($comments_args);
	?>
	<div class="clear"></div>
</div>
<!-- end: #comments -->