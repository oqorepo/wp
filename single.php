<?php get_header(); ?>
<!-- start: #page -->
<main id="novedades-int" class="page novedades-int" role="main">
	<?php if(have_posts()): the_post(); ?>
		<?php
		$post_id = get_the_ID();
		$categoria = get_the_terms( $post_id, 'category' );
		$pdf = get_field('archivo_pdf');
		?>
        
        <div class="top cover">
        	<div class="middle">
            	<?php if( is_singular('desco-medios') ): ?>
                	<h2 class="titulo large white">DESCO EN LOS MEDIOS</h2>
                <?php else: ?>
                    <h2 class="titulo large white">NOTICIAS</h2>
                <?php endif; ?>	
            </div>
        </div>
        <div class="grid-container grid-large section">
            <div class="left-content grid-70 tablet-grid-70 np-mobile">
            	<article>
                	<header>
                        <h1 class="titulo medium"><?php the_title(); ?></h1>
                        <time datetime="<?php the_time('Y/m/d'); ?>"><?php the_date(); ?></time>
                        <div class="thumb textcenter">
							<?php the_post_thumbnail('noticia-int'); ?>
                            <?php if($pdf): ?>
                            	<br>
                            	<a href="<?php echo $pdf; ?>" class="button" target="_blank">Descargar PDF <i class="flaticon-pdf" style="font-size: 16px;"></i></a>
                            <?php endif; ?>
                        </div>
                    </header>
                    <div class="texto textjustify">
						<?php the_content(); ?>
                    </div>
                    <div class="compartir">
                    	<?php
						$titulo = get_the_title();
						$url = get_permalink();
						?>
                        <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($url); ?>&amp;t=<?php echo $titulo; ?>" class="facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="http://twitter.com/share?text=<?php echo $titulo; ?>&url=<?php echo $url; ?>" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>

                            <li><a href="https://plus.google.com/share?url=<?php echo $url; ?>" class="google-plus" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </article>
    
                
            </div>
            <!-- end: .content -->
            
            <?php get_sidebar(); ?>
            
        </div>
    <?php endif; ?>
</main>
<!-- end: #page -->
<?php get_footer(); ?>