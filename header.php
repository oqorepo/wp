<?php global $epcl_theme; ?>
<!DOCTYPE html>
<!--[if lte IE 7]><html class="ie7 ie" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie8 ie" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 9]><html class="ie9 ie" <?php language_attributes(); ?>><![endif]-->
<!--[if !IE]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N2TLJQF');</script>
<!-- End Google Tag Manager -->

<title><?php bloginfo('name'); ?> - <?php wp_title(''); ?></title>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php if(!empty($epcl_theme['blog_description'])): ?>
<meta name="description" content="<?php echo $epcl_theme['blog_description']; ?>">
<?php endif; ?>
<?php if(!empty($epcl_theme['keywords'])): ?>
<meta name="keywords" content="<?php echo $epcl_theme['keywords']; ?>">
<?php endif; ?>

<?php if(!empty($epcl_theme['favicon_16']) && $epcl_theme['favicon_16']['url']): ?>
<link rel="shortcut icon" href="<?php echo $epcl_theme['favicon_16']['url']; ?>">
<?php endif; ?>
<?php if(!empty($epcl_theme['favicon_57']) && $epcl_theme['favicon_57']['url']): ?>
<link rel="apple-touch-icon" href="<?php echo $epcl_theme['favicon_57']['url']; ?>">
<?php endif; ?>
<?php if(!empty($epcl_theme['favicon_72']) && $epcl_theme['favicon_72']['url']): ?>
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $epcl_theme['favicon_72']['url']; ?>">
<?php endif; ?>
<?php if(!empty($epcl_theme['favicon_114']) && $epcl_theme['favicon_114']['url']): ?>
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $epcl_theme['favicon_114']['url']; ?>">
<?php endif; ?>
<?php if(!empty($epcl_theme['favicon_144']) && $epcl_theme['favicon_144']['url']): ?>
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $epcl_theme['favicon_144']['url']; ?>">
<?php endif; ?>
<style type="text/css">
<?php //get_template_part('partials/critical-css'); ?>
</style>
<!--[if !IE]><!-->
<?php if(!wp_is_mobile()): ?>
	<link rel="stylesheet" href="<?php echo EP_THEMEPATH; ?>/css/aos.css" />
<?php endif; ?>
<!--<![endif]-->
<!--[if (lt IE 9) & (!IEMobile)]>
<link rel="stylesheet" href="<?php echo EP_THEMEPATH; ?>/css/grids-ie.css" />
<script src="<?php echo EP_THEMEPATH; ?>/js/html5shiv.min.js"></script>
<![endif]-->
<?php wp_head(); ?>
    <?php if($epcl_theme['css_code']) echo '<style>'.$epcl_theme['css_code'].'</style>'; ?>
</head>
<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N2TLJQF"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
    
    <?php if ( is_singular( 'proyectos' ) ) { ?>
        <!-- Código de instalación Cliengo para https://www.desco.cl/ --> <script type="text/javascript">(function () { var ldk = document.createElement('script'); ldk.type = 'text/javascript'; ldk.async = true; ldk.src = 'https://s.cliengo.com/weboptimizer/58d04d6be4b06a3ae1484e31/5b3fc13de4b0a8542cfdbba7.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ldk, s); })();</script>
    <?php } ?>

    <!-- <div class="cd-loader">
      <h5>Cargando...</h5>
      <div class="cd-loader__grid">
        <div class="cd-loader__item"></div>
      </div>
    </div> -->
	<?php if($epcl_theme['ga_code']) echo $epcl_theme['ga_code']; ?>

    <nav id="menu" role="navigation">
        <div class="menu-container">
            <a class="cerrar">CERRAR <i class="flaticon-close"></i></a>
            <?php if(has_nav_menu('header')) wp_nav_menu( array('theme_location' => 'header', 'container' => false) ); ?>
            <?php if(has_nav_menu('header2')) wp_nav_menu( array('theme_location' => 'header2', 'container' => false) ); ?>
        </div>
    </nav>

    <!-- start: #wrapper -->
    <div id="wrapper">
    	<div id="overlay"></div>
        <?php get_template_part('partials/header'); ?>
