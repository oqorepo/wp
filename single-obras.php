<?php the_post(); ?>

<?php
$descripcion = get_field('descripcion');
$tipo = get_field('tipo');
$ubicacion = get_field('ubicacion');
$fecha = get_field('fecha');
$superficie = get_field('superficie');
$monto_contrato = get_field('monto_contrato');
$mandante = get_field('mandante');
$galeria = get_field('galeria');
?>

<!-- start: #obras -->
<section id="obras" class="carrusel-proyectos ajax grid-container grid-small grid-parent np-tablet">

        <div class="lista grid-30 tablet-grid-33 grid-parent">
            <div class="item">
                <div class="info">
                    <h4 class="titulo usmall"><?php the_title(); ?></h4>
                    <!-- <p class="hide-on-mobile"><?php echo $descripcion; ?></p> -->
                    <?php if($monto_contrato): ?>
                        <div class="detalle hide-on-mobile hide-on-tablet">
                            <h5><span class="titulo usmall">Precio Contrato: U.F.</span> <?php echo $monto_contrato; ?></h5>
                        </div>
                    <?php endif; ?>
                    <?php if($ubicacion): ?>
                        <div class="detalle hide-on-mobile hide-on-tablet">
                            <h5 class="titulo usmall">Ubicación:</h5>
                            <p><?php echo $ubicacion; ?></p>
                        </div>
                    <?php endif; ?>
                    <?php if($fecha): ?>
                        <div class="detalle hide-on-mobile hide-on-tablet">
                            <h5><span class="titulo usmall">Inicio de Obras:</span> <?php echo $fecha; ?></h5>
                        </div>
                    <?php endif; ?>
                    <?php if($superficie): ?>
                        <div class="detalle hide-on-mobile hide-on-tablet">
                            <h5><span class="titulo usmall">Superficie:</span> <?php echo $superficie; ?> m&sup2;</h5>
                        </div>
                    <?php endif; ?>
                    <?php if($mandante): ?>
                        <div class="detalle hide-on-mobile hide-on-tablet">
                            <h5><span class="titulo usmall">Mandante:</span> <?php echo $mandante; ?></h5>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="galeria grid-70 tablet-grid-66 grid-parent alta">
            

                <!-- start: .proyecto -->
                <div class="proyecto active">
                    <div class="slick-slider circle-dots">
                        <?php foreach($galeria as $g): ?>
                            <?php $class_width = ($g['width'] < $g['height']) ? 'contain': 'cover'; ?>
                            <div class="item">
                                <div class="fullimg <?php echo $class_width; ?>" style="background-image: url('<?php echo $g['sizes']['ep-large']; ?>')"></div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="info hide-on-desktop">
                        <?php if($monto_contrato): ?>
                            <div class="grid-20 tablet-grid-25 mobile-grid-50">
                                <h5>Precio Contrato:</h5>
                                <p>U.F. <?php echo $monto_contrato; ?></p>
                            </div>
                        <?php endif; ?>
                        <?php if($fecha): ?>
                            <div class="grid-20 tablet-grid-25 mobile-grid-50">
                                <h5>Inicio de Obras:</h5>
                                <p><?php echo $fecha; ?></p>
                            </div>
                        <?php endif; ?>
                        <div class="clear hide-on-tablet hide-on-desktop"></div>
                        <?php if($ubicacion): ?>
                            <div class="grid-30 tablet-grid-50">
                                <h5>Ubicación:</h5>
                                <p><?php echo $ubicacion; ?></p>
                            </div>
                        <?php endif; ?>
                        <div class="clear hide-on-desktop"></div>
                        <?php if($superficie): ?>
                            <div class="grid-15 tablet-grid-25 mobile-grid-50">
                                <h5>Superficie:</h5>
                                <p><?php echo $superficie; ?> m&sup2;</p>
                            </div>
                        <?php endif; ?>
                        <?php if($mandante): ?>
                            <div class="grid-15 tablet-grid-50 mobile-grid-50">
                                <h5>Mandante:</h5>
                                <p><?php echo $mandante; ?></p>
                            </div>
                        <?php endif; ?>

                        <div class="clear"></div>
                    </div>
                </div>
                <!-- end: .proyecto -->

        </div>

        <div class="clear"></div>
</section>
<!-- end: #obras -->

<script type="text/javascript" src="<?php echo EP_THEMEPATH; ?>/js/slick/slick.min.js"></script>
<script type="text/javascript">
(function($){
    $(window).load(function($){
        

        
    }); 
})(jQuery);
</script>
