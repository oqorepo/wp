<?php get_header(); ?>
<!-- start: #page -->
<main id="page" class="page" role="main">
	<?php if(have_posts()): the_post(); ?>
        <?php
		// Info General
		$etiqueta = get_field('etiqueta');
		$titulo = get_field('titulo');
		$bajada = get_field('bajada');
		$imagen_superior = get_field('imagen_superior');
		$url_imagen_superior = $imagen_superior['sizes']['slider-home'];
		
		?>
        <!-- start: .top -->
        <div class="top fullheight fullpage-section">
            <?php if($url_imagen_superior): ?>
                <div class="fullimg cover" style="background-image: url(<?php echo $url_imagen_superior; ?>);"></div>
            <?php endif; ?>
            <div class="middle aligntop" data-aos="fade-down">
                <div class="grid-container grid-small">
                	<?php if($etiqueta): ?>
                        <h3 class="etiqueta"><?php echo $etiqueta; ?></h3>
                    <?php endif; ?>
                    <?php if($titulo): ?>
                        <h2 class="titulo large white"><?php echo $titulo; ?></h2>
                    <?php endif; ?>
                    <?php if($bajada): ?>
                        <p class="titulo white"><?php echo $bajada; ?></p>
                    <?php endif; ?>
                </div>
            </div>
            <a href="#content" class="next-section hide-on-mobile">Use scroll para navegar<img src="<?php echo EP_THEMEPATH; ?>/images/scrolldown.png"></a>
        </div>
        <!-- end: .top -->
        
        <!-- start: .content -->
        <div class="content fullpage-section fp-auto-height" id="content">
        
            <section class="texto section">
                <div class="grid-container">
                    <h1 class="titulo large textcenter"><?php the_title(); ?></h1>
                    <div class="texto">
						<?php the_content(); ?>
                    </div>
                </div>
            </section>
            
        </div>
        <!-- end: .content -->
    <?php endif; ?>
</main>
<!-- end: #page -->
<?php get_footer(); ?>