<?php get_header(); ?>
<?php add_filter('excerpt_length', 'small_excerpt_length', 999); ?>
<!-- start: #search-results -->
<main id="search-results" class="main grid-container" role="main">
	<!-- start: .basic-list -->
	<div class="basic-list">
    	<section class="articles grid-66 tablet-grid-66">
			<?php if(have_posts()): the_post(); ?>
				<?php
                $avatar = get_avatar( get_the_author_meta('email'), '260', false , get_the_author());
                $class = (!$avatar) ? 'no-avatar' : 'with-avatar';
                ?>
                <!-- start: .author -->
                <section class="author boxed-content <?php echo $class; ?> grid-container">
                    <h3 class="title"><?php _e('About the Author', 'ep-theme'); ?></h3>
                    <div class="avatar grid-15 tablet-grid-15"><?php echo $avatar ?></div>
                    <div class="right grid-85 tablet-grid-85">
                        <h4 class="alignleft"><?php echo get_the_author(); ?></h4>
                        <div class="border small hide-on-desktop hide-on-tablet"></div>
                        <div class="social alignright">
                            <?php
                            $facebook = get_the_author_meta('facebook');
                            $twitter = get_the_author_meta('twitter');
                            $dribbble = get_the_author_meta('dribbble');
                            ?>
                            <?php if($facebook): ?>
                                <a href="<?php echo $facebook; ?>" target="_blank" class="social facebook"><i class="fa fa-facebook"></i></a>
                            <?php endif; ?>
                            <?php if($twitter): ?>
                                <a href="<?php echo $twitter; ?>" target="_blank" class="social twitter"><i class="fa fa-twitter"></i></a>
                            <?php endif; ?>
                            <?php if($dribbble): ?>
                                <a href="<?php echo $dribbble; ?>" target="_blank" class="social dribbble"><i class="fa fa-dribbble"></i></a>
                            <?php endif; ?>
                        </div>
                        <div class="clear"></div>
                        <div class="border small hide-on-mobile"></div>
                        <p><?php the_author_meta('description'); ?></p>
                    </div>
                    <div class="clear"></div>
                </section>
                <!-- end: .author -->
            <?php endif; ?>
            <?php
                rewind_posts();
                if(have_posts()):
                    while (have_posts()): the_post(); 
                        get_template_part('partials/search-result');
                    endwhile;
                    ep_pagination();
            ?>
            <?php else: ?>
                <article>
                    <h3 class="textcenter"><?php _e('No posts were found.', 'ep-theme'); ?></h3> 
                </article>
            <?php endif; ?>
        </section>
        <?php get_sidebar(); ?>
        <div class="clear"></div>
    </div>
    <!-- end: .basic-list -->
</main>
<!-- end: #search-results -->
<?php get_footer(); ?>