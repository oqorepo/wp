<?php
$query_proyecto = get_query_var('proyectos');
if(!$query_proyecto){
	$id_modelo = get_the_ID();
	$post = get_post($id_modelo);
	$id_proyecto = get_field('proyecto');
	$url_proyecto = get_permalink($id_proyecto);
	$url_modelo = $post->post_name;
	$url_tipologia = get_field('tipologia');
	$permalink = $url_proyecto.$url_tipologia.'/'.$post->post_name;
	if ( wp_redirect( $permalink ) ) {
		exit;
	}
}
?>
<?php get_header(); ?>
<!-- start: #modelos-int -->
<main id="modelos-int" class="main" role="main">
	<?php if(have_posts()): the_post(); ?>
    	<?php
		
		
		// Info General
		$tipologia_actual = get_query_var('tipologia');	
		$cotizar = get_query_var('cotizar');
		$id_modelo = get_the_ID();
		if( get_post_type() != 'modelos'){ // Sin modelo seleccionado
			$id_proyecto = get_the_ID();
			$args = array(
				//'fields' => 'ids',
				'posts_per_page' => 1,
				'post_type' => 'modelos',
				'order' => 'ASC',
				'orderby' => 'title',
				'suppress_filters' => false,
				'meta_query' => array(
					'relation' => 'AND',
						array(
							'key' => 'proyecto',
							'value' => $id_proyecto,
							'compare' => '=',
						),
						array(
							'key' => 'tipologia',
							'value' => $tipologia_actual,
							'compare' => '=',
						)
					)
			);
			$modelos = get_posts($args);
			$url_modelo = $modelos[0]->post_name;
			$id_modelo = $modelos[0]->ID;
		}else{
			$id_proyecto = get_field('proyecto');
			$url_modelo = $post->post_name;
		}
		
		$args = array(
			'posts_per_page' => -1,
			'post_type' => 'modelos',
			'order' => 'ASC',
			'orderby' => 'menu_order',
			'suppress_filters' => false,
			'meta_query' => array(
				'relation' => 'AND',
					array(
						'key' => 'proyecto',
						'value' => $id_proyecto,
						'compare' => '=',
					),
					array(
						'key' => 'tipologia',
						'value' => $tipologia_actual,
						'compare' => '=',
					)
				)
		);
		$modelos = get_posts($args);
		$nombre_modelo = get_the_title($id_modelo);
		
		$nombre_proyecto = get_the_title($id_proyecto);
		$ciudad = get_field('ciudad', $id_proyecto);
		$estado_proyecto = get_field('estado', $id_proyecto);
		$estado_actual = get_field('estado_actual', $id_proyecto);
		$url_proyecto = get_permalink($id_proyecto);
		
		$tipologia_actual_url = $url_proyecto.$tipologia_actual;
		switch($estado_proyecto){
			case 'venta': $nombre_estado = 'Entrega Inmediata'; break;
			case 'vendido': $nombre_estado = 'Vendido'; break;
			case 'futuro': $nombre_estado = 'Proyecto Futuro'; break;
		}
		if($estado_actual) $nombre_estado = $estado_actual;
		$tipologias_proyecto = get_field('tipologias', $id_proyecto);
		
		
		$columna_1 = get_field('columna_1', $id_modelo);
		$columna_2 = get_field('columna_2', $id_modelo);
		$columna_3 = get_field('columna_3', $id_modelo);
		
		$activar_variacion = get_field('activar_variacion', $id_modelo);
		$titulo_variacion = get_field('titulo_variacion', $id_modelo);
		$mt_interior = get_field('mt_interior', $id_modelo);
		$mt_terraza = get_field('mt_terraza', $id_modelo);
		$mt_total = get_field('mt_total', $id_modelo);
		$orientacion = get_field('orientacion', $id_modelo);
		$dormitorios = get_field('dormitorios', $id_modelo);
		$banos = get_field('banos', $id_modelo);
		$texto_planta = get_field('texto_planta', $id_modelo);
		$texto_abierto_izq = get_field('texto_abierto_izq', $id_modelo);
		$texto_abierto_der = get_field('texto_abierto_der', $id_modelo);
		$titulo_valor = get_field('titulo_valor', $id_modelo);
		$valor_uf = get_field('valor_uf', $id_modelo);
		
		// Departamentos
		
		$deptos = get_field('deptos', $id_modelo);
		
		// Imagenes
		
		$imagen_esquicio = get_field('imagen_esquicio', $id_modelo);
		$url_imagen_esquicio = $imagen_esquicio['sizes']['medium'];
		$galeria = get_field('galeria', $id_modelo);
		
		$url_actual = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		
		?>
        <!-- start: .content -->
        <div class="content"> 
            <header>
                <div class="">
                	<!-- start: .nombre -->
                	<div class="nombre grid-65">
                        <img src="<?php echo EPCL_THEMEPATH; ?>/images/logo-modelos.png">
                        <div class="proyecto">
                            <h2 class="titulo small"><?php echo $nombre_proyecto; /*echo ', '.get_the_title($ciudad);*/ ?></h2>
                            <p><?php echo $nombre_estado; ?></p>
                        </div>
                        <div class="lista-modelos hide-on-mobile">
							<?php if(!empty($modelos)): ?>
                                <select name="modelo" class="nice-select dark">
                                    <?php foreach($modelos as $post): setup_postdata($post); ?>
                                        <option value="<?php echo $tipologia_actual_url; ?>/<?php echo $post->post_name; ?>" <?php if($id_modelo == $post->ID) echo 'selected'; ?>><?php the_title(); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            <?php endif; wp_reset_postdata(); ?>
                        </div>
                    </div>
                    <!-- end: .nombre -->
                    <!-- start: .tipologias -->
                    <div class="tipologias grid-35 grid-parent hide-on-mobile hide-on-tablet">
                        <?php foreach($tipologias_proyecto as $t): ?>
                            <?php
                                $nombre = $t['titulo_tipologia'];
                                $url = sanitize_title($nombre);
                            ?>
                            <a href="<?php echo $url_proyecto; ?>/<?php echo $url; ?>/" <?php if($url == $tipologia_actual) echo 'class="active"'; ?>><?php echo $nombre; ?></a>
                        <?php endforeach; ?>
                    </div>
                    <!-- end: .tipologias -->
                </div>
            </header> 
        	<div class="clear"></div>
            
            <!-- start: .menu -->
            <div class="menu">
            	<a href="<?php echo $url_proyecto; ?>#lista-modelos" class="accion cerrar tooltip-small" title="Volver al Proyecto"><i class="flaticon-close"></i></a>
            	<div class="compartir">
                	<?php
						$titulo = get_the_title();
						$url = $url_actual;
					?>
                	<a href="#" class="accion abrir"><i class="flaticon-share"></i></a>
                    <ul>
                    	<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode($url); ?>&amp;t=<?php echo $titulo; ?>" target="_blank"><i class="flaticon-facebook"></i></a></li>
                        <li><a href="http://twitter.com/share?text=<?php echo $titulo; ?>&url=<?php echo $url; ?>" target="_blank"><i class="flaticon-twitter"></i></a></li>
                        <?php if( has_post_thumbnail() ): ?>
                            <li><a href="https://pinterest.com/pin/create/button/?url=<?php echo $url; ?>&media=<?php the_post_thumbnail_url('ep-large'); ?>&description=<?php echo $titulo; ?>" target="_blank"><i class="flaticon-pinterest"></i></a></li>
                        <?php endif; ?>
                        <li class="hide-on-desktop hide-on-tablet"><a href="whatsapp://send?text=Simonetti Inmobiliaria - <?php echo $titulo; ?> - enlace: <?php echo $url; ?>" data-action="share/whatsapp/share" target="_blank"><i class="flaticon-whatsapp"></i></a></li>
                    </ul>
                </div>
            	<a href="javascript:void(0)" id="abrir-compartir" class="accion email tooltip-small" title="Compartir por email"><i class="flaticon-email"></i></a>
                <a href="<?php echo $url_proyecto; ?>imprimir/<?php echo $id_modelo; ?>" class="accion imprimir tooltip-small hide-on-mobile hide-on-tablet" title="Imprimir esta Planta" target="_blank"><i class="flaticon-printer"></i></a>
                
                <!-- start: #compartir-email -->
                <div id="compartir-email">
                    <form action="send.php" data-url="<?php echo site_url(); ?>/wp-admin/admin-ajax.php" method="post" class="formulario">
                        <h4 class="titulo small">ENVIAR POR EMAIL</h4>
                        <div class="cerrar"><i class="flaticon-close"></i></div>
                        <div class="input-wrapper">
                            <input type="text" name="nombre" class="inputbox" placeholder="TU NOMBRE" required>
                        </div>
                        <div class="input-wrapper">
                            <input type="text" name="nombre_destinatario" class="inputbox" placeholder="NOMBRE DESTINATARIO" required>
                        </div>
                        <div class="input-wrapper">
                            <input type="text" name="email_destinatario" class="inputbox" placeholder="EMAIL DESTINATARIO" required>
                        </div>
                        <div class="input-wrapper">
                            <textarea name="comentario" class="inputbox" placeholder="COMENTARIO" required></textarea>
                        </div>
                        <input type="hidden" name="url" value="<?php echo $url_actual; ?>">
                        <input type="hidden" name="nonce" value="<?php echo wp_create_nonce("contacto"); ?>">
                        <input type="hidden" name="form_title" value="Compartir"/>
                        <input type="hidden" name="action" value="ajax_contact">
                        <div class="msg-exito">
                            <p><i class="flaticon-round-check"></i> ¡Haz compartido exitosamente esta planta!</p>
                        </div>
                        <button type="submit" class="submit button dark icon">
                        	<span class="default">ENVIAR <i class="flaticon-arrow-line-right"></i></span>
                            <span class="loading">ENVIANDO...</span>
                        </button>
                        
                    </form>
                </div>
                <!-- end: #compartir-email -->
            </div>
            <!-- end: .menu -->
            
            <div class="">
            	<?php if( !empty($galeria) ): ?>
                    <!-- start: .galeria -->
                    <div class="galeria grid-60">
                    	<div class="lista-modelos hide-on-tablet hide-on-desktop">
                        	<div style="display: inline-block;">
								<?php if(!empty($modelos)): ?>
                                    <select name="modelo">
                                        <?php foreach($modelos as $post): setup_postdata($post); ?>
                                            <option value="<?php echo $tipologia_actual_url; ?>/<?php echo $post->post_name; ?>" <?php if($id_modelo == $post->ID) echo 'selected'; ?>><?php the_title(); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                <?php endif; wp_reset_postdata(); ?>
                            </div>
                        </div>
                        <ul class="tabs">
                        	<?php foreach($galeria as $g): ?>
                            	<?php if( $g['caption'] ): ?>
                                    <li><a href="javascript:void(0)"><?php echo $g['caption']; ?></a></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                        <div class="panes">
                        	<?php foreach($galeria as $g): ?>
                                <div class="tab">
                                	<div class="img">
                                        <img src="<?php echo $g['sizes']['ep-large']; ?>" class="panzoom">
                                    </div>
                                    <div class="clear"></div>
                                    <div class="botones">
                                        <a href="javascript:void(0)" class="zoom-in"><i class="flaticon-zoom-in"></i></a>
                                        <a href="javascript:void(0)" class="zoom-out"><i class="flaticon-zoom-out"></i></a>
                                        <span class="border"></span>
                                        <a href="javascript:void(0)" class="reset"><i class="flaticon-fullscreen"></i></a>
                                    </div>
                                    <div class="clear"></div>
                                    <a href="javascript:void(0)" class="cotizar scrollto button dark icon hide-on-desktop">COTIZAR MODELO <i class="flaticon-arrow-line-right"></i></a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <a href="#" class="tooltip" title="Las Imágenes, información y textos son meramente ilustrativos y referenciales<br>por lo que podrían no representar exactamente la realidad"><i class="flaticon-circle"></i></a>
                    </div>
                    <!-- end: .galeria -->
                <?php endif; ?>
                <!-- start: .right -->
                <div class="right grid-40 grid-parent <?php if($cotizar) echo 'cotizando'; ?>">
                	<!-- start: .info -->
                    <div class="info">
                    	<?php if($url_imagen_esquicio): ?>
                            <div class="esquicio">
                              	<img src="<?php echo $url_imagen_esquicio; ?>" alt="Esquicio">      
                            </div>
                        <?php endif; ?>
                    	<!-- start: #programa -->
                        <div id="programa">
                            <?php if($columna_1): ?>
                            <div class="item grid-33 tablet-grid-33 mobile-grid-100">
                                <h4 class="titulo"><?php echo $columna_1; ?></h4>
                            </div>
                        <?php endif; ?>
                        <?php if($columna_2): ?>
                            <div class="item grid-33 tablet-grid-33 mobile-grid-50">
                                <span class="border hide-on-mobile"></span>
                                <h4 class="titulo"><?php echo $columna_2; ?></h4>
                            </div>
                        <?php endif; ?>
                        <?php if($columna_3): ?>
                            <div class="item grid-33 tablet-grid-33 mobile-grid-50">
                                <span class="border hide-on-tablet"></span>
                                <h4 class="titulo"><?php echo $columna_3; ?></h4>
                            </div>
                        <?php endif; ?>
                        </div>
                        <!-- end: #programa -->

                        <!-- start: .detalle -->
                        <div class="detalle">
							<?php if($activar_variacion == 'si' && $titulo_variacion != ''): ?>
                                <h4 class="titulo"><strong><?php echo $titulo_variacion; ?></strong></h4>
                            <?php endif; ?>
                            <div class="item grid-50 tablet-grid-50 grid-parent">
                                <ul>
                                    <?php if($mt_interior): ?>
                                        <li><span class="nombre">- Sup. Útil:</span> <?php echo $mt_interior; ?></li>
                                    <?php endif; ?>
                                    <?php if($mt_terraza): ?>
                                        <li><span class="nombre">- Sup. Terraza:</span> <?php echo $mt_terraza; ?></li>
                                    <?php endif; ?>
                                    <?php if($texto_abierto_izq): ?>
                                        <li><?php echo $texto_abierto_izq; ?></li>
                                    <?php endif; ?>
                                    <?php if($mt_total): ?>
                                        <li><span class="nombre">- Sup. Total:</span> <?php echo $mt_total; ?></li>
                                    <?php endif; ?>
                                </ul>	
                            </div>
                            <div class="item grid-50 tablet-grid-50 grid-parent">
                                <ul>
                                    <?php if($orientacion): ?>
                                        <li><span class="nombre">- Orientación:</span> <?php echo $orientacion; ?></li>
                                    <?php endif; ?>
                                    <?php if($dormitorios): ?>
                                        <li><span class="nombre">- Dormitorios:</span> <?php echo $dormitorios; ?></li>
                                    <?php endif; ?>
                                    <?php if($banos): ?>
                                        <li><span class="nombre">- Baños:</span> <?php echo $banos; ?></li>
                                    <?php endif; ?>
                                    <?php if($texto_abierto_der): ?>
                                        <li><?php echo $texto_abierto_der; ?></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <div class="clear"></div>
                            <?php if($texto_planta): ?>
                                <div class="item">
                                   <?php echo $texto_planta; ?> 
                                </div>
                            <?php endif; ?>
<?php /*?>                            <div class="item grid-50 tablet-grid-50 grid-parent">
                                <?php if($texto_abierto_izq) echo $texto_abierto_izq; ?>	
                            </div>
                            <div class="item grid-50 tablet-grid-50 grid-parent">
                                <?php if($texto_abierto_der) echo $texto_abierto_der; ?>
                            </div><?php */?>
                            <div class="valor grid-50 grid-parent">
                                <?php if($titulo_valor): ?>
                                    <p><?php echo $titulo_valor; ?></p>
                                <?php endif; ?>
                                <?php if($valor_uf): ?>
                                    <h3 class="titulo"><sup>UF</sup><?php echo $valor_uf; ?></h3>
                                <?php endif; ?>
                            </div>
                            <?php if( $estado_proyecto == 'venta'): ?>
                                <div class="cotizar grid-50 grid-parent">
                                    <!--<a href="<?php echo $tipologia_actual_url; ?>/<?php echo $url_modelo; ?>/cotizar" class="button white icon">COTIZAR MODELO <i class="flaticon-arrow-line-right"></i></a>-->
                                    <a href="javascript:void(0)" class="cotizar button white icon">COTIZAR MODELO <i class="flaticon-arrow-line-right"></i></a>
                                </div>
                            <?php endif; ?>
                            <div class="clear"></div>
                        </div>
                        <!-- end: .detalle -->
                        
                        <!-- start: #cotizar -->
                        <div id="cotizar">
                        	<div class="volver">
                                <!--<a href="<?php echo $tipologia_actual_url; ?>/<?php echo $url_modelo; ?>" class="tooltip-small" title="Volver al modelo"><i class="flaticon-arrow-left"></i> VOLVER</a>-->
                                <a href="javascript:void(0)" class="volver tooltip-small" title="Volver al modelo"><i class="flaticon-arrow-left"></i> VOLVER</a>
                            </div>
                        	<!-- start: #programa -->
                            <div id="programa">
                                <h3 class="titulo large cotizar">Solicitud de Cotización</h3>
                                <?php if($columna_1): ?>
                                    <div class="item grid-33 tablet-grid-33 mobile-grid-100">
                                        <h4 class="titulo"><?php echo $columna_1; ?></h4>
                                    </div>
                                <?php endif; ?>
								<?php if($columna_2): ?>
                                    <div class="item grid-33 tablet-grid-33 mobile-grid-50">
                                        <span class="border hide-on-mobile"></span>
                                        <h4 class="titulo"><?php echo $columna_2; ?></h4>
                                    </div>
                                <?php endif; ?>
                                <?php if($columna_3): ?>
                                    <div class="item grid-33 tablet-grid-33 mobile-grid-50">
                                        <span class="border hide-on-tablet"></span>
                                        <h4 class="titulo"><?php echo $columna_3; ?></h4>
                                    </div>
                                <?php endif; ?>
                                <div class="clear"></div>
                            </div>
                            <!-- end: #programa -->
                            <form action="send.php" data-url="<?php echo site_url(); ?>/wp-admin/admin-ajax.php" method="post" class="formulario">
                                <div class="input-wrapper">
                                    <input type="text" name="nombre" class="inputbox" placeholder="NOMBRE *" required>
                                </div>
                                <div class="input-wrapper">
                                    <input type="email" name="email" id="email" class="inputbox" placeholder="EMAIL *" required>
                                </div>
                                <div class="input-wrapper">
                                    <input type="text" name="telefono" class="inputbox" placeholder="TELÉFONO (9 digitos)">
                                </div>
                                <?php if( !empty($deptos) ): ?>
                                    <div class="input-wrapper">
                                        <select name="numero_depto" class="nice-select dark" required>
                                            <option value="">Seleccione Nº de Departamento *</option>
                                            <?php foreach( $deptos as $d): ?>
                                                <option value="<?php echo $d['nombre_depto']; ?>"><?php echo $d['nombre_depto']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                <?php endif; ?>
                                <div class="input-wrapper">
                                    <textarea name="comentario" placeholder="COMENTARIO"></textarea>
                                </div>
                                <input type="hidden" name="proyecto" value="<?php echo $nombre_proyecto; ?>">
                                <input type="hidden" name="proyecto_id" value="<?php echo $id_proyecto; ?>">
                                <input type="hidden" name="modelo" value="<?php echo $nombre_modelo ?>">
                                <input type="hidden" name="modelo_id" value="<?php echo $id_modelo ?>">
                                <input type="hidden" name="nonce" value="<?php echo wp_create_nonce("contacto"); ?>">
                                <input type="hidden" name="form_title" value="Cotizar"/>
                                <input type="hidden" name="action" value="ajax_contact">
                                <p class="requeridos" style="margin-bottom: 30px;"><i class="flaticon-alert"></i> Los campos marcados con * son requeridos.</p>
                                <label class="radio">
                                    <input type="checkbox" name="info" checked value="si">
                                    Deseo recibir<br>información y novedades
                                </label>
                                <button type="submit" class="submit button icon">
                                	<span class="default">ENVIAR SOLICITUD <i class="flaticon-arrow-line-right"></i></span>
                                    <span class="loading">ENVIANDO...</span>
                                </button>
                                <div class="clear"></div>
                                
                                <!--<div class="msg-exito">
                                    <p><i class="flaticon-round-check"></i> Gracias por contactar a Simonetti Inmobiliaria, la cotización será enviada al mail registrado.</p>
                                </div>-->
                                <div class="msg-exito2">
                                	<div class="table">
                                    	<div class="middle">
                                        	<div class="icono"><i class="flaticon-check"></i></div>
                                            <h3>¡Gracias!</h3>
                                            <p>La cotización será enviada a<br><strong class="email">correo@correo.com</strong></p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <?php /*?><div class="volver">
                                <a href="<?php echo $tipologia_actual_url; ?>/<?php echo $url_modelo; ?>" class="tooltip-small" title="Volver al modelo"><i class="flaticon-arrow-left"></i> VOLVER</a>-->
                                <a href="javascript:void(0)" class="volver tooltip-small" title="Volver al modelo"><i class="flaticon-arrow-left"></i> VOLVER</a>
                            </div><?php */?>
                        </div>
                        <!-- end: #cotizar -->
                    </div>
                    <!-- end: .info -->
                </div>
                <!-- end: .right -->
            </div>
            <div class="clear"></div>
		</div>
        <!-- end: .content -->
    <?php endif; ?>
</main>
<!-- end: #proyectos-int -->
<?php get_footer(); ?>