		<?php get_template_part('partials/footer'); ?>
        <div class="clear"></div>
    </div>
    <!-- end: #wrapper -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,700,700i" rel="stylesheet">
	<!-- W3TC-include-js-head -->
    <?php wp_footer(); ?>

    <script type="text/javascript">
	(function($){
		var site_url = '<?php echo site_url(); ?>';
		$(document).ready(function($){
            $('#master-loader').addClass('loaded');
            $('.single-proyectos #equipamiento .desco-btn').remove();
		});
		$(window).load(function(){
			var document_width = $(document).width();

			// if( $('.fullpage-section').length > 0 && document_width > 1200){
			// 	$.scrollify({
			// 		section:".fullpage-section",
			// 		setHeights: false,
			// 		updateHash :false,
			// 		offset: 0,
			// 		standardScrollElements: '.fp-auto-height',
			// 		before:function(index, elem) {

			// 		},
			// 		afterRender: function(){

			// 		},
			// 		interstitialSection : "#footer",
			// 		//sectionName:false,
			// 		//scrollSpeed:1100,
			// 	});
			// 	$.scrollify.update();
			// }

		});
	})(jQuery);
	</script>
    <!--[if !IE]><!-->
	<script type="text/javascript" src="<?php echo EP_THEMEPATH; ?>/js/aos.js"></script>
    <script type="text/javascript">
    jQuery(window).load(function(){

        AOS.init({
            offset: 200,
            duration: 1000,
            disable: window.innerWidth < 1024,
			easing: 'ease',
            once: true
        });

    });
    </script>
    <!--<![endif]-->

</body>
</html>
