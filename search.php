<?php get_header(); ?>
<?php
$args = array(
	'posts_per_page' => 10,
	'post_type' => 'post',
	'suppress_filters' => false,
	's' => get_search_query()
);
$posts = get_posts($args); 
?>
<!-- start: #trends -->
<main id="trends" class="main" role="main">
	<h2>Resultados para: <?php the_search_query(); ?></h2>
	<div class="masonry">
		<?php foreach($posts as $post): setup_postdata($post); ?>
            <div class="item">
            	<div class="box">
					<?php the_post_thumbnail('trends'); ?>
                    <div class="overlay">
                    	<div class="cell">
                            <h3><?php the_title(); ?></h3>
                            <?php the_excerpt(); ?>
                            <a href="<?php the_permalink(); ?>" class="button vermas">VER MÁS</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</main>
<!-- end: #trends -->
<?php get_footer(); ?>