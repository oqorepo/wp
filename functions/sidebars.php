<?php

/* Default Sidebar Widgets */

register_sidebar(array(
	'name' => 'Panel Lateral',
	'id' => 'sidebar',
	'description' => 'Panel Lateral Noticias',
	'before_widget' => '<section id="%1$s" class="widget tablet-grid-50 %2$s">',
	'after_widget' => '<div class="clear"></div></section>',
	'before_title' => '<h4 class="widget-title">',
	'after_title' => '</h4>',
));


/* Footer Sidebar Widgets */

register_sidebar(array(
	'name' => 'Footer',
	'id' => 'footer',
	'description' => 'Widgets para pie de página',
	'before_widget' => '<section id="%1$s" class="widget %2$s">',
	'after_widget' => '<div class="clear"></div></section>',
	'before_title' => '<h4 class="widget-title"><a href="https://www.instagram.com/inmobiliaria_indesa/" target="_blank">',
	'after_title' => '</a></h4>',
));

?>