<?php

/* Inmobiliaria Proyectos Ejecutados */

$prefix_key = 'proyectos_ejecutados_';

acf_add_local_field_group( array(
	'key' => 'proyectos_ejecutados',
	'title' => 'Información Adicional',
	'fields' => array (
		array (
			'key' => $prefix_key.'tipo',
			'name' => 'tipo',
			'label' => 'Tipo de Proyecto',
			'type' => 'radio',			
			'required' => true,
			'choices' => array (
				'depto' => 'Departamentos',
				'casas' => 'Casas',
			),
			'default_value' => 'depto',
			'layout' => 'horizontal',
		),
		array (
			'key' => $prefix_key.'descripcion',
			'name' => 'descripcion',
			'label' => 'Descripción corta',
			'required' => true,
			'type' => 'textarea',
			'rows' => 3,			
			'instructions' => 'ej. Esta primera etapa consiste en 2 edificios de 13 pisos cada uno, de un ...'
		),
		array (
			'key' => $prefix_key.'ubicacion',
			'name' => 'ubicacion',
			'label' => 'Ubicación',
			'type' => 'text',			
			'instructions' => 'ej. Paso Hondo, Quilpué, V Región'
		),
		array (
			'key' => $prefix_key.'fecha',
			'name' => 'fecha',
			'label' => 'Fecha (año)',
			'type' => 'number',			
			'instructions' => 'ej. 2013'
		),
		array (
			'key' => $prefix_key.'superficie',
			'name' => 'superficie',
			'label' => 'Superficie Edificada',
			'type' => 'text',
			'required' => true,
			'prepend' => 'm2',			
			'instructions' => 'ej. 10.400'
		),
		array (
			'key' => $prefix_key.'cantidad',
			'name' => 'cantidad',
			'label' => 'Cantidad de Deptos o Casas',
			'type' => 'text',
			'required' => true,		
			'instructions' => 'ej. 102'
		),
		array (
			'key' => $prefix_key.'galeria',
			'label' => 'Galería del Proyecto',
			'name' => 'galeria',
			'type' => 'gallery',
			'instructions' => '',
			'required' => false,
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
		
	),
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'proyectos-ejecutados',
			),
		),
	)
));


?>