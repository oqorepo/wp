<?php

/* Desco en los Medios */

$prefix_key = 'desco_medios_';

acf_add_local_field_group( array(
	'key' => 'desco_medios',
	'title' => 'Información Adicional',
	'fields' => array (
		array (
			'key' => $prefix_key.'medio',
			'name' => 'medio',
			'label' => 'Medio de Comunicación',
			'type' => 'text',			
			'instructions' => 'ej. El Mercurio'
		),
		array (
			'key' => $prefix_key.'archivo_pdf',
			'label' => 'Archivo PDF (opcional)',
			'name' => 'archivo_pdf',
			'type' => 'file',
			'instructions' => 'Anexo en formato PDF',
			'required' => false,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'medium',
			'library' => 'all',
			'mime_types' => 'pdf',
		),
		
	),
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'desco-medios',
			),
		),
	)
));


?>