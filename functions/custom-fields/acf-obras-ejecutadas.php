<?php

/* Constructora Obras Ejecutadas */

$prefix_key = 'obras_ejecutadas_';

acf_add_local_field_group( array(
	'key' => 'obras_ejecutadas',
	'title' => 'Información Adicional',
	'fields' => array (
        array (
            'key' => $prefix_key.'numero_obra',
            'name' => 'numero_obra',
            'label' => 'Nº Obra',
            'type' => 'number',
            'instructions' => 'ej. 512'
        ),
		array (
			'key' => $prefix_key.'descripcion',
			'name' => 'descripcion',
			'label' => 'Descripción corta',
			'required' => false,
			'type' => 'textarea',
			'rows' => 3,			
			'instructions' => 'ej. Esta primera etapa consiste en 2 edificios de 13 pisos cada uno, de un ...'
		),
		array (
			'key' => $prefix_key.'ubicacion',
			'name' => 'ubicacion',
			'label' => 'Ubicación',
			'type' => 'text',			
			'instructions' => 'ej. Paso Hondo, Quilpué, V Región'
		),
		array (
			'key' => $prefix_key.'fecha',
			'name' => 'fecha',
			'label' => 'Fecha (año)',
			'type' => 'text',			
			'instructions' => 'ej. 2013-2015'
		),
		array (
			'key' => $prefix_key.'monto_contrato',
			'name' => 'monto_contrato',
			'label' => 'Monto Contrato',
			'type' => 'text',
			'required' => false,
			'prepend' => 'UF',	
			'instructions' => 'ej. 58.300'
		),
		array (
			'key' => $prefix_key.'superficie',
			'name' => 'superficie',
			'label' => 'Superficie Edificada',
			'type' => 'text',
			'required' => false,
			'prepend' => 'm2',			
			'instructions' => 'ej. 10.400'
		),
		array (
			'key' => $prefix_key.'mandante',
			'name' => 'mandante',
			'label' => 'Mandante (opcional)',
			'type' => 'text',
			'required' => false,	
			'instructions' => 'ej. Mall Viña del Mar'
		),
		array (
			'key' => $prefix_key.'galeria',
			'label' => 'Galería del Proyecto',
			'name' => 'galeria',
			'type' => 'gallery',
			'instructions' => '',
			'required' => false,
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
		
	),
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'obras-ejecutadas',
			),
		),
	)
));


?>
