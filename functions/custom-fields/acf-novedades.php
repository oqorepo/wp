<?php

/* Pagina Preguntas Frecuentes */

$prefix_key = 'novedades_';

acf_add_local_field_group( array(
	'key' => 'novedades',
	'title' => 'Información Adicional',
	'fields' => array (
		array (
			'key' => $prefix_key.'visualizaciones',
			'name' => 'visualizaciones',
			'label' => 'Visualizaciones',
			'type' => 'number',
			'readonly' => true,	
			'instructions' => '',
			'default_value' => 0
		),
		
	),
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
	)
));


?>