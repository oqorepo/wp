<?php

/* Constructora */

$prefix_key = 'constructora_';

acf_add_local_field_group( array(
	'key' => 'constructora',
	'title' => 'Información Adicional',
	'fields' => array (
		array (
			'key' => $prefix_key.'tab_info', // Información General
			'label' => 'Info. Superior',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'imagen_superior',
			'label' => 'Imagen Superior',
			'name' => 'imagen_superior',
			'type' => 'image',
			'instructions' => 'Recomendado: 1920x1080',
			'required' => false,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
		array (
			'key' => $prefix_key.'etiqueta',
			'name' => 'etiqueta',
			'label' => 'Etiqueta',
			'type' => 'text',
			'instructions' => 'ej. Proyectos'
		),
		array (
			'key' => $prefix_key.'titulo',
			'name' => 'titulo',
			'label' => 'Título',
			'type' => 'text',
			'instructions' => 'ej. Inmobiliaria'
		),
		array (
			'key' => $prefix_key.'bajada',
			'name' => 'bajada',
			'label' => 'Bajada',
			'type' => 'text',
			'instructions' => 'ej. capacitación / trabajo en equipo / tecnología de punta'
		),
		array (
			'key' => $prefix_key.'tab_aereo', // Galeria Aérea
			'label' => 'Galeria Aérea',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'galeria_aerea',
			'label' => 'Añadir Galería',
			'name' => 'galeria_aerea',
			'type' => 'repeater',
			'instructions' => '',
			'layout' => 'block',
			'sub_fields' => array (
				array (
					'key' => $prefix_key.'galeria_tipo',
					'label' => 'Tipo',
					'name' => 'galeria_tipo',
					'type' => 'radio',
					'choices' => array (
						'video' => 'Video',
						'imagen' => 'imagen',
					),
					'default_value' => 'venta',
					'layout' => 'horizontal',
				),
				array (
					'key' => $prefix_key.'url_video',
					'label' => 'Url del video en Youtube',
					'name' => 'url_video',
					'type' => 'oembed',
					'instructions' => 'ej. https://www.youtube.com/watch?v=X0uuvfsiIdw',
					'conditional_logic' => array(
						array (
							array (
								'field' => $prefix_key.'galeria_tipo',
								'operator' => '==',
								'value' => 'video',
							)
						)
					),
				),
				array (
					'key' => $prefix_key.'galeria_imagen',
					'label' => 'Imagen',
					'name' => 'galeria_imagen',
					'type' => 'image',
					'instructions' => 'En móvil siempre es requerida una imagen, independiente si es video o no.',
					'required' => false,
					'wrapper' => array (
						'width' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
					/*'conditional_logic' => array(
						array (
							array (
								'field' => $prefix_key.'galeria_tipo',
								'operator' => '==',
								'value' => 'imagen',
							)
						)
					),*/
				),
				array (
					'key' => $prefix_key.'galeria_titulo',
					'name' => 'galeria_titulo',
					'label' => 'Título',
					'type' => 'text',
					'instructions' => 'ej. Edificio Bahía'
				),
				array (
					'key' => $prefix_key.'galeria_bajada',
					'name' => 'galeria_bajada',
					'label' => 'Breve descripción',
					'type' => 'textarea',
					'instructions' => 'ej. El proyecto se encuentra ubicado en...'
				),
			),

		),


	),
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'location' => array (
		array (
			array (
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'page-templates/constructora.php',
			),
		),
		array (
			array (
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'page-templates/obras-desarrollo.php',
			),
		),
		array (
			array (
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'page-templates/obras-ejecutadas.php',
			),
		),
	)
));


?>
