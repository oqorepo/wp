<?php

/* Custom Post Type: Modelos */

$prefix_key = 'modelos_';

$post_id = sanitize_text_field($_GET['post']);
$choices = array();
if(get_post_type($post_id) == 'modelos'){
	$proyecto = get_field('proyecto', $post_id);
	$tipologias = get_field('tipologias', $proyecto);
	if($proyecto && !empty($tipologias))
		foreach($tipologias as $t){
			$titulo = $t['titulo_tipologia'];
			$url = sanitize_title($titulo);
			$choices[$url] = $titulo;	
		}
}
if( empty($choices) ) $choices = 'Proyecto sin tipologias creadas.';

$solo_activar_variacion = array (
	array (
		array (
		'field' => $prefix_key.'activar_variacion',
		'operator' => '==',
		'value' => 'si',
		),
	),
);


acf_add_local_field_group( array(
	'key' => 'modelos',
	'title' => 'Información Adicional',
	'fields' => array (
		array (
			'key' => $prefix_key.'tab_info', // Información General
			'label' => 'Información General',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'proyecto',
			'name' => 'proyecto',
			'label' => 'Proyecto al cual pertenece',
			'type' => 'post_object',
			'post_type' => 'proyectos',
			'required' => false,
			'return_format' => 'id',
			'allow_null' => true
		),
		array (
			'key' => $prefix_key.'tipologia',
			'name' => 'tipologia',
			'label' => 'Tipología a la cual pertenece',
			'type' => 'select',
			'required' => false,
			'ui' => false,
			'ajax' => false,
			'default_value' => 'emplazamiento',
			'choices' => $choices
		),
		/*array (
			'key' => $prefix_key.'tipologia_url',
			'name' => 'tipologia_url',
			'label' => 'URL Tipologia',
			'type' => 'text',
			'readonly' => true,
			'required' => false,
			'default_value' => ''
		),*/
		array (
			'key' => $prefix_key.'programa',
			'name' => 'programa',
			'label' => 'Programa',
			'type' => 'text',
			'instructions' => 'ej. 3 DORM + ESTAR + SERVICIO<br>(se utiliza como nombre en listados)'
		),
		array (
			'key' => $prefix_key.'col_message',
			'name' => 'col_message',
			'label' => 'Columnas de texto libre:',
			'type' => 'message',
			'message' => 'Esta sección utiliza la etiqueta &lt;span&gt; que permite asignar un texto más pequeño justo debajo del número o texto fuera de la etiqueta.',		
			'instructions' => '',
			'new_lines' => 'br'
		),
		array (
			'key' => $prefix_key.'columna_1',
			'name' => 'columna_1',
			'label' => 'Info Columna 1',
			'type' => 'text',			
			'instructions' => 'ej. 3 &lt;span&gt;dorm + estar + servicios&lt;/span&gt'
		),
		array (
			'key' => $prefix_key.'columna_2',
			'name' => 'columna_2',
			'label' => 'Info Columna 2',
			'type' => 'text',			
			'instructions' => 'ej. 4 &lt;span&gt;BAÑOS&lt;/span&gt'
		),
		array (
			'key' => $prefix_key.'columna_3',
			'name' => 'columna_3',
			'label' => 'Info Columna 3',
			'type' => 'text',			
			'instructions' => 'ej. 226 &ltsup&gtm2&lt;/sup&gt; &lt;span&gt;SUP. TOTAL&lt;/span&gt;'
		),
		array (
			'key' => $prefix_key.'mt_interior',
			'name' => 'mt_interior',
			'label' => 'Superficie Interior',
			'type' => 'text',
			'required' => false,	
			//'prepend' => 'm&sup2;',			
			'instructions' => 'ej. 210 m2'
		),
		array (
			'key' => $prefix_key.'mt_terraza',
			'name' => 'mt_terraza',
			'label' => 'Superficie Terraza (opcional)',
			'type' => 'text',
			'required' => false,
			//'prepend' => 'm&sup2;',				
			'instructions' => 'ej. 50 m2'
		),
		array (
			'key' => $prefix_key.'mt_total',
			'name' => 'mt_total',
			'label' => 'Superficie Total',
			'type' => 'text',
			'required' => false,
			//'prepend' => 'm&sup2;',			
			'instructions' => 'ej. 260 m2'
		),
		array (
			'key' => $prefix_key.'orientacion',
			'name' => 'orientacion',
			'label' => 'Orientación',
			'type' => 'text',
			'required' => false,	
			//'prepend' => 'm&sup2;',			
			'instructions' => 'ej. O - S - P'
		),
		array (
			'key' => $prefix_key.'dormitorios',
			'name' => 'dormitorios',
			'label' => 'Nº de dormitorios',
			'type' => 'text',
			'required' => false,	
			//'prepend' => 'm&sup2;',			
			'instructions' => 'ej. 3'
		),
		array (
			'key' => $prefix_key.'banos',
			'name' => 'banos',
			'label' => 'Nº de baños',
			'type' => 'text',
			'required' => false,	
			//'prepend' => 'm&sup2;',			
			'instructions' => 'ej. 4'
		),
		array (
			'key' => $prefix_key.'texto_planta',
			'name' => 'texto_planta',
			'label' => 'Texto Servicios Adicionales (opcional)',
			'type' => 'wysiwyg',
			'media_upload' => false,
			'toolbar' => 'basic',		
			'instructions' => 'ej. - Incluye 4 estacionamientos.'
		),
		array (
			'key' => $prefix_key.'texto_abierto_izq',
			'name' => 'texto_abierto_izq',
			'label' => 'Texto Abierto Izquierda (arriba de sup.total)',
			'type' => 'text',		
			'instructions' => 'ej. &lt;span class="nombre">- Sup. Logía:&lt;/span> 30 m2'
		),
		array (
			'key' => $prefix_key.'texto_abierto_der',
			'name' => 'texto_abierto_der',
			'label' => 'Texto Abierto Derecha (debajo de baños)',
			'type' => 'text',	
			'instructions' => 'ej. &lt;span class="nombre">- Otro dato:&lt;/span> 2'
		),
		array (
			'key' => $prefix_key.'titulo_valor',
			'name' => 'titulo_valor',
			'label' => 'Título valor',
			'type' => 'text',
			//'required' => true,
			'prepend' => '',
			'instructions' => 'ej. Valor desde'
		),
		array (
			'key' => $prefix_key.'valor_uf',
			'name' => 'valor_uf',
			'label' => 'Valor',
			'type' => 'text',
			//'required' => true,
			'prepend' => 'UF',
			'instructions' => 'ej. UF 40.000'
		),
		array (
			'key' => $prefix_key.'tab_numeros', // Departamentos
			'label' => 'Departamentos',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'activar_variacion',
			'name' => 'activar_variacion',
			'label' => 'Activar variación',
			'type' => 'radio',
			//'required' => true,
			'prepend' => '',
			'choices' => array (
				'si' => 'Si',
				'no' => 'No',
			),
			'default_value' => 'no',
			'layout' => 'horizontal',
			'instructions' => 'Todos los campos son requeridos al momento de activar las variaciones.'
		),
		array (
			'key' => $prefix_key.'titulo_variacion',
			'name' => 'titulo_variacion',
			'label' => 'Titulo al activar variaciones',
			'type' => 'text',
			'required' => false,
			//'prepend' => 'm&sup2;',	
			'default_value' => 'Desde:',		
			'instructions' => 'Por defecto: Desde',
			'conditional_logic' => $solo_activar_variacion,
		),
		array (
			'key' => $prefix_key.'deptos',
			'label' => 'Añadir Departamentos por Numeración',
			'name' => 'deptos',
			'type' => 'repeater',
			'instructions' => '',
			'layout' => 'block',
			'button_label' => 'Añadir Otro',
			'sub_fields' => array (
				array (
					'key' => $prefix_key.'nombre_depto',
					'name' => 'nombre_depto',
					'label' => 'Número',
					'type' => 'number',
					'instructions' => 'ej. 201',
					'wrapper' => array (
						'width' => '50%',
					),
				),
				array (
					'key' => $prefix_key.'valor_depto',
					'name' => 'valor_depto',
					'label' => 'Valor',
					'type' => 'text',
					'prepend' => 'UF',
					'instructions' => 'ej. UF 40.000',
					'wrapper' => array (
						'width' => '50%',
					),
				),
				array (
					'key' => $prefix_key.'mt_interior_depto',
					'name' => 'mt_interior_depto',
					'label' => 'Superficie Interior',
					'type' => 'text',
					'required' => false,	
					//'prepend' => 'm&sup2;',			
					'instructions' => 'ej. 210 m2',
					'wrapper' => array (
						'width' => '50%',
					),
					'conditional_logic' => $solo_activar_variacion,
				),
				array (
					'key' => $prefix_key.'mt_terraza_depto',
					'name' => 'mt_terraza_depto',
					'label' => 'Superficie Terraza (opcional)',
					'type' => 'text',
					'required' => false,
					//'prepend' => 'm&sup2;',				
					'instructions' => 'ej. 50 m2',
					'wrapper' => array (
						'width' => '50%',
					),
					'conditional_logic' => $solo_activar_variacion,
				),
				array (
					'key' => $prefix_key.'mt_total_depto',
					'name' => 'mt_total_depto',
					'label' => 'Superficie Total',
					'type' => 'text',
					'required' => false,
					//'prepend' => 'm&sup2;',			
					'instructions' => 'ej. 260 m2',
					'wrapper' => array (
						'width' => '50%',
					),
					'conditional_logic' => $solo_activar_variacion,
				),
				array (
					'key' => $prefix_key.'orientacion_depto',
					'name' => 'orientacion_depto',
					'label' => 'Orientación',
					'type' => 'text',
					'required' => false,	
					//'prepend' => 'm&sup2;',			
					'instructions' => 'ej. O - S - P',
					'wrapper' => array (
						'width' => '50%',
					),
					'conditional_logic' => $solo_activar_variacion,
				),
				array (
					'key' => $prefix_key.'dormitorios_depto',
					'name' => 'dormitorios_depto',
					'label' => 'Nº de dormitorios',
					'type' => 'text',
					'required' => false,	
					//'prepend' => 'm&sup2;',			
					'instructions' => 'ej. 3',
					'wrapper' => array (
						'width' => '50%',
					),
					'conditional_logic' => $solo_activar_variacion,
				),
				array (
					'key' => $prefix_key.'banos_depto',
					'name' => 'banos_depto',
					'label' => 'Nº de baños',
					'type' => 'text',
					'required' => false,	
					//'prepend' => 'm&sup2;',			
					'instructions' => 'ej. 4',
					'wrapper' => array (
						'width' => '50%',
					),
					'conditional_logic' => $solo_activar_variacion,
				),
				array (
					'key' => $prefix_key.'texto_planta_depto',
					'name' => 'texto_planta_depto',
					'label' => 'Texto Servicios Adicionales (opcional)',
					'type' => 'wysiwyg',
					'media_upload' => false,
					'toolbar' => 'basic',		
					'instructions' => 'ej. - Incluye 4 estacionamientos.',
					'conditional_logic' => $solo_activar_variacion,
				),
				array (
					'key' => $prefix_key.'imagen_esquicio_depto',
					'label' => 'Imagen Esquicio',
					'name' => 'imagen_esquicio_depto',
					'type' => 'image',
					'instructions' => 'Tamaño recomendado: 200x125 px, PNG sobre fondo transparente',
					'required' => false,
					'wrapper' => array (
						'width' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
					'conditional_logic' => $solo_activar_variacion,
				),
				array (
					'key' => $prefix_key.'galeria_depto',
					'label' => 'Añadir Imágenes (pisos)',
					'name' => 'galeria_depto',
					'type' => 'gallery',
					'instructions' => 'Utilizar el campo caption para definir el nombre del piso.',
					'required' => false,
					'return_format' => 'array',
					'preview_size' => 'thumbnail',
					'library' => 'all',
					'wrapper' => array (
						'class' => 'galeria-small',
					),
					'conditional_logic' => $solo_activar_variacion,
				),
				
			),
		),
		array (
			'key' => $prefix_key.'tab_imagenes', // Imagenes
			'label' => 'Imágenes',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'imagen_esquicio',
			'label' => 'Imagen Esquicio',
			'name' => 'imagen_esquicio',
			'type' => 'image',
			'instructions' => 'Tamaño recomendado: 200x125 px, PNG sobre fondo transparente',
			'required' => false,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
		array (
			'key' => $prefix_key.'galeria',
			'label' => 'Añadir Imágenes (tabs)',
			'name' => 'galeria',
			'type' => 'gallery',
			'instructions' => 'Utilizar el campo caption para definir el nombre del tab.',
			'required' => false,
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
	),
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'modelos',
			),
		),
	),
));

?>