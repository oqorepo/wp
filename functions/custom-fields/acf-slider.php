<?php

/* Custom Post Type: Slider */

$prefix_key = 'slider_';

acf_add_local_field_group( array(
	'key' => 'slider',
	'title' => 'Información Adicional',
	'fields' => array (
		array (
			'key' => $prefix_key.'titulo',
			'name' => 'titulo',
			'label' => 'Título',
			'type' => 'text',			
			'instructions' => 'ej. Laguna de la Pirámide'
		),
		array (
			'key' => $prefix_key.'bajada',
			'name' => 'bajada',
			'label' => 'Bajada',
			'type' => 'text',		
			'instructions' => 'ej. Disfruta de 365 días de vacaciones con la mejor conectividad'
		),
		array (
			'key' => $prefix_key.'estado',
			'name' => 'estado',
			'label' => 'Estado (opcional)',
			'type' => 'text',		
			'instructions' => 'ej. Próxima entrega'
		),
		array (
			'key' => $prefix_key.'imagen',
			'label' => 'Imagen',
			'name' => 'imagen',
			'type' => 'image',
			'instructions' => 'Tamaño recomendado: 1920x1080',
			'required' => true,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
		),
		array (
			'key' => $prefix_key.'video',
			'name' => 'video',
			'label' => 'URL del video de Fondo en Youtube (opcional)',
			'type' => 'url',			
			'instructions' => 'ej. https://www.youtube.com/watch?v=13tg_I6Rv7Q'
		),
		array (
			'key' => $prefix_key.'destino',
			'label' => 'Destino',
			'name' => 'destino',
			'type' => 'radio',
			'instructions' => '',
			'required' => false,
			'conditional_logic' => 0,
			'choices' => array (
				'_self' => '_self (ventana actual)',
				'_blank' => '_blank (otra ventana)',
			),
			'default_value' => '_self',
			'layout' => 'horizontal',
			'instructions' => 'Como se abrira el enlace.',
		),
		array (
			'key' => $prefix_key.'url',
			'name' => 'url',
			'label' => 'URL (opcional)',
			'type' => 'url',
			'required' => false,
			'placeholder' => '',	
			'instructions' => 'ej. http://www.oqo.cl',
		),
		array (
			'key' => $prefix_key.'texto_boton',
			'name' => 'texto_boton',
			'label' => 'Texto boton (opcional)',
			'type' => 'text',			
			'instructions' => 'Por defecto: "Ver Proyecto"'
		),
		
	),
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'slider',
			),
		),
	),
));

?>