<?php
/* Pagina Inicio */

$prefix_key = 'inicio_';

acf_add_local_field_group( array(
	'key' => 'inicio',
	'title' => 'Información Adicional',
	'fields' => array (
		array (
			'key' => $prefix_key.'mensaje',
			'name' => 'mensaje',
			'label' => 'Nota:',
			'type' => 'message',
			'message' => 'Esta sección contiene una breve reseña',		
			'instructions' => '',
			'new_lines' => 'br'
		),
		array (
			'key' => $prefix_key.'titulo_somos',
			'name' => 'titulo_somos',
			'label' => 'Título Somos',
			'type' => 'text',			
			'instructions' => 'ej. #SOMOS DESCO'
		),
		array (
			'key' => $prefix_key.'video_somos',
			'name' => 'video_somos',
			'label' => 'URL del video en Youtube (opcional)',
			'type' => 'url',			
			'instructions' => 'ej. https://www.youtube.com/watch?v=13tg_I6Rv7Q'
		),
		array (
			'key' => $prefix_key.'imagen_somos',
			'label' => 'Imagen de fondo',
			'name' => 'imagen_somos',
			'type' => 'image',
			'instructions' => 'Tamaño recomendado: 1920x1080',
			'required' => false,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
		),
		array (
			'key' => $prefix_key.'texto_somos',
			'name' => 'texto_somos',
			'label' => 'Texto Reseña',
			'type' => 'wysiwyg',
			'media_upload' => false,
			'toolbar' => 'basic',		
			'instructions' => 'ej. Con 80 años de trayectoria...'
		),

	),
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'location' => array (
		array (
			array (
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'page-templates/inicio.php',
			),
		),
	),
));

?>
