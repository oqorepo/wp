<?php

/* Inmobiliaria */

$prefix_key = 'inmobiliaria_';

acf_add_local_field_group( array(
	'key' => 'inmobiliaria',
	'title' => 'Información Adicional',
	'fields' => array (
		array (
			'key' => $prefix_key.'imagen_superior',
			'label' => 'Imagen Superior',
			'name' => 'imagen_superior',
			'type' => 'image',
			'instructions' => 'Recomendado: 1920x1080',
			'required' => false,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
		array (
			'key' => $prefix_key.'etiqueta',
			'name' => 'etiqueta',
			'label' => 'Etiqueta',
			'type' => 'text',			
			'instructions' => 'ej. Proyectos'
		),
		array (
			'key' => $prefix_key.'titulo',
			'name' => 'titulo',
			'label' => 'Título',
			'type' => 'text',			
			'instructions' => 'ej. Inmobiliaria'
		),
		array (
			'key' => $prefix_key.'bajada',
			'name' => 'bajada',
			'label' => 'Bajada',
			'type' => 'text',			
			'instructions' => 'ej. capacitación / trabajo en equipo / tecnología de punta'
		),
		
	),
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'location' => array (
		array (
			array (
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'page-templates/inmobiliaria.php',
			),
		),
	)
));


?>