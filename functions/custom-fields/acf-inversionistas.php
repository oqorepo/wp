<?php

/* Página Inversionistas */

$prefix_key = 'inversionistas_';

acf_add_local_field_group( array(
	'key' => 'inversionistas',
	'title' => 'Información Adicional',
	'fields' => array (
		array (
			'key' => $prefix_key.'tab_info', // Información General
			'label' => 'Info. Superior',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'imagen_superior',
			'label' => 'Imagen Superior',
			'name' => 'imagen_superior',
			'type' => 'image',
			'instructions' => 'Recomendado: 1920x1080',
			'required' => false,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
		array (
			'key' => $prefix_key.'etiqueta',
			'name' => 'etiqueta',
			'label' => 'Etiqueta',
			'type' => 'text',			
			'instructions' => 'ej. Proyectos'
		),
		array (
			'key' => $prefix_key.'titulo',
			'name' => 'titulo',
			'label' => 'Título',
			'type' => 'text',			
			'instructions' => 'ej. Inmobiliaria'
		),
		array (
			'key' => $prefix_key.'bajada',
			'name' => 'bajada',
			'label' => 'Bajada',
			'type' => 'text',			
			'instructions' => 'ej. capacitación / trabajo en equipo / tecnología de punta'
		),
		array (
			'key' => $prefix_key.'tab_beneficios', // Beneficios
			'label' => 'Beneficios',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'titulo_beneficios',
			'name' => 'titulo_beneficios',
			'label' => 'Título Sección',
			'type' => 'text',			
			'instructions' => 'ej. #Beneficios de ser Inversionista'
		),
		array (
			'key' => $prefix_key.'beneficios',
			'label' => 'Añadir Beneficios',
			'name' => 'beneficios',
			'type' => 'repeater',
			'instructions' => '',
			'layout' => 'row',
			'button_label' => 'Añadir Otro',
			'sub_fields' => array (
				array (
					'key' => $prefix_key.'icono_beneficio',
					'label' => 'Icono',
					'name' => 'icono_beneficio',
					'type' => 'image',
					'instructions' => 'Tamaño recomendado: 160x120px',
					'required' => false,
					'wrapper' => array (
						'width' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'medium',
					'library' => 'all',
				),	
				array (
					'key' => $prefix_key.'titulo_beneficio',
					'name' => 'titulo_beneficio',
					'label' => 'Título',
					'type' => 'text',
					'instructions' => 'ej. Equivale a un seguro de vida',
					'wrapper' => array (
						'width' => '50%',
					),
				),
				array (
					'key' => $prefix_key.'bajada_beneficio',
					'name' => 'bajada_beneficio',
					'label' => 'Bajada',
					'type' => 'textarea',
					'instructions' => 'ej. En el caso que ya no estés, tu crédito queda pagado y la propiedad a nombre de tu familia...',
					'wrapper' => array (
						'width' => '50%',
					),
				),	
			),
		),
		array (
			'key' => $prefix_key.'tab_invertir', // Porque Invertir
			'label' => 'Por qué Invertir',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'fondo_invertir',
			'label' => 'Imagen de Fondo',
			'name' => 'fondo_invertir',
			'type' => 'image',
			'instructions' => 'Tamaño recomendado: 1920x1080px',
			'required' => false,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
		),	
		array (
			'key' => $prefix_key.'texto_invertir',
			'name' => 'texto_invertir',
			'label' => 'Texto',
			'type' => 'wysiwyg',
			'media_upload' => true,
			'toolbar' => 'full',
			'required' => false,
			'instructions' => ''
		),
		array (
			'key' => $prefix_key.'tab_cifras', // Desco en cifras
			'label' => 'Desco en Cifras',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'titulo_cifras',
			'name' => 'titulo_cifras',
			'label' => 'Título Sección',
			'type' => 'text',			
			'instructions' => 'ej. #DESCO EN CIFRAS'
		),
		array (
			'key' => $prefix_key.'cifras',
			'label' => 'Añadir Datos',
			'name' => 'cifras',
			'type' => 'repeater',
			'instructions' => '',
			'layout' => 'row',
			'button_label' => 'Añadir Otro',
			'sub_fields' => array (
				array (
					'key' => $prefix_key.'imagen_cifras',
					'name' => 'imagen_cifras',
					'label' => 'Imagen',
					'type' => 'image',
					'instructions' => 'Tamaño recomendado: 700x350px',
					'required' => false,
					'wrapper' => array (
						'width' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'medium',
					'library' => 'all',
				),	
				array (
					'key' => $prefix_key.'texto_cifras',
					'name' => 'texto_cifras',
					'label' => 'Texto',
					'type' => 'textarea',
					'instructions' => 'Se permite HTML para destacar, &lt;span>destacar&lt;/span>',
					'wrapper' => array (
						'width' => '50%',
					),
				),	
			),
		),
		array (
			'key' => $prefix_key.'tab_faq', // Preguntas Frecuentes
			'label' => 'Preguntas Frecuentes',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'titulo_faq',
			'name' => 'titulo_faq',
			'label' => 'Título Sección',
			'type' => 'text',			
			'instructions' => 'ej. #PREGUNTAS FRECUENTES'
		),
		array (
			'key' => $prefix_key.'fondo_faq',
			'label' => 'Imagen de Fondo',
			'name' => 'fondo_faq',
			'type' => 'image',
			'instructions' => 'Tamaño recomendado: 1920x1080px',
			'required' => false,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
		),	
		array (
			'key' => $prefix_key.'faq',
			'label' => 'Añadir Datos',
			'name' => 'faq',
			'type' => 'repeater',
			'instructions' => '',
			'layout' => 'row',
			'button_label' => 'Añadir Otro',
			'sub_fields' => array (
				array (
					'key' => $prefix_key.'pregunta_faq',
					'name' => 'pregunta_faq',
					'label' => 'Título Pregunta',
					'type' => 'text',
					'instructions' => 'ej. ¿Qué implica un crédito hipotecario?',
				),
				array (
					'key' => $prefix_key.'respuesta_faq',
					'name' => 'respuesta_faq',
					'label' => 'Respuesta',
					'type' => 'wysiwyg',
					'media_upload' => false,
					'toolbar' => 'basic',
					'required' => false,
					'instructions' => ''
				),	
			),
		),
	),
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'location' => array (
		array (
			array (
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'page-templates/inversionistas.php',
			),
		),
	)
));


?>