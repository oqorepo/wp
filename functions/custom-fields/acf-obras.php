<?php

/* Constructora Obras en Desarrollo */

$prefix_key = 'obras_';

acf_add_local_field_group( array(
	'key' => 'obras',
	'title' => 'Información Adicional',
	'fields' => array (
		array (
			'key' => $prefix_key.'descripcion',
			'name' => 'descripcion',
			'label' => 'Descripción corta',
			'required' => true,
			'type' => 'textarea',
			'rows' => 3,			
			'instructions' => 'ej. Esta primera etapa consiste en 2 edificios de 13 pisos cada uno, de un ...'
		),
		array (
			'key' => $prefix_key.'ubicacion',
			'name' => 'ubicacion',
			'label' => 'Ubicación',
			'type' => 'text',			
			'instructions' => 'ej. Paso Hondo, Quilpué, V Región'
		),
		array (
			'key' => $prefix_key.'fecha',
			'name' => 'fecha',
			'label' => 'Inicio de Obras',
			'type' => 'text',			
			'instructions' => 'ej. 01-07-2017'
		),
		array (
			'key' => $prefix_key.'monto_contrato',
			'name' => 'monto_contrato',
			'label' => 'Monto Contrato',
			'type' => 'text',
			'required' => false,
			'prepend' => 'UF',	
			'instructions' => 'ej. 58.300'
		),
		array (
			'key' => $prefix_key.'superficie',
			'name' => 'superficie',
			'label' => 'Superficie',
			'type' => 'text',
			'required' => true,
			'prepend' => 'm2',			
			'instructions' => 'ej. 10.400'
		),
		array (
			'key' => $prefix_key.'mandante',
			'name' => 'mandante',
			'label' => 'Mandante',
			'type' => 'text',
			'required' => false,	
			'instructions' => 'ej. Mall Viña del Mar'
		),
		array (
			'key' => $prefix_key.'galeria',
			'label' => 'Galería del Proyecto',
			'name' => 'galeria',
			'type' => 'gallery',
			'instructions' => '',
			'required' => false,
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
		
	),
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'obras',
			),
		),
	)
));


?>