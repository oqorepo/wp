<?php

/* Página Somos */

$prefix_key = 'somos_';

acf_add_local_field_group( array(
	'key' => 'somos',
	'title' => 'Información Adicional',
	'fields' => array (
		array (
			'key' => $prefix_key.'tab_info', // Información General
			'label' => 'Info. Superior',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'imagen_superior',
			'label' => 'Imagen Superior',
			'name' => 'imagen_superior',
			'type' => 'image',
			'instructions' => 'Recomendado: 1920x1080',
			'required' => false,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
		array (
			'key' => $prefix_key.'etiqueta',
			'name' => 'etiqueta',
			'label' => 'Etiqueta',
			'type' => 'text',			
			'instructions' => 'ej. Proyectos'
		),
		array (
			'key' => $prefix_key.'titulo',
			'name' => 'titulo',
			'label' => 'Título',
			'type' => 'text',			
			'instructions' => 'ej. Inmobiliaria'
		),
		array (
			'key' => $prefix_key.'bajada',
			'name' => 'bajada',
			'label' => 'Bajada',
			'type' => 'text',			
			'instructions' => 'ej. capacitación / trabajo en equipo / tecnología de punta'
		),
		array (
			'key' => $prefix_key.'tab_mision', // Mision Vision
			'label' => 'Misión',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'texto_mision',
			'name' => 'texto_mision',
			'label' => 'Misión / Visión',
			'type' => 'wysiwyg',
			'media_upload' => true,
			'toolbar' => 'full',
			'required' => false,
			'instructions' => ''
		),
		array (
			'key' => $prefix_key.'tab_historia', // Mision Vision
			'label' => 'Trayectoria',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'texto_historia',
			'name' => 'texto_historia',
			'label' => 'Historia',
			'type' => 'wysiwyg',
			'media_upload' => true,
			'toolbar' => 'full',
			'required' => false,
			'instructions' => ''
		),
		array (
			'key' => $prefix_key.'tab_directores', // Directores
			'label' => 'Directores',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'directores',
			'label' => 'Añadir Directores',
			'name' => 'directores',
			'type' => 'repeater',
			'instructions' => '',
			'layout' => 'block',
			'button_label' => 'Añadir Otro',
			'sub_fields' => array (
				array (
					'key' => $prefix_key.'imagen_director',
					'label' => 'Fotografía',
					'name' => 'imagen_director',
					'type' => 'image',
					'instructions' => 'Tamaño recomendado: 600x600',
					'required' => false,
					'wrapper' => array (
						'width' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'medium',
					'library' => 'all',
				),	
				array (
					'key' => $prefix_key.'nombre_director',
					'name' => 'nombre_director',
					'label' => 'Nombre',
					'type' => 'text',
					'instructions' => 'ej. Eduardo Ferrer Corrales',
					'wrapper' => array (
						'width' => '50%',
					),
				),
				array (
					'key' => $prefix_key.'cargo_director',
					'name' => 'cargo_director',
					'label' => 'Cargo',
					'type' => 'text',
					'instructions' => 'ej. Gerente General',
					'wrapper' => array (
						'width' => '50%',
					),
				),	
			),
		),
		array (
			'key' => $prefix_key.'imagen_directores',
			'label' => 'Imagen de todos los Directores',
			'name' => 'imagen_directores',
			'type' => 'image',
			'instructions' => 'Tamaño recomendado: 1200x400',
			'required' => false,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
		),
		array (
			'key' => $prefix_key.'tab_obras', // Obras
			'label' => 'Obras',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'obras_historicas',
			'label' => 'Añadir Obras Históricas',
			'name' => 'obras_historicas',
			'type' => 'repeater',
			'instructions' => '',
			'layout' => 'block',
			'button_label' => 'Añadir Otra',
			'sub_fields' => array (
				array (
					'key' => $prefix_key.'nombre_obra',
					'name' => 'nombre_obra',
					'label' => 'Nombre',
					'type' => 'text',
					'instructions' => 'ej. Escuela Santa Marta',
					'wrapper' => array (
						'width' => '50%',
					),
				),
				array (
					'key' => $prefix_key.'fecha_obra',
					'name' => 'fecha_obra',
					'label' => 'Fecha Construcción',
					'type' => 'number',
					'instructions' => 'ej. 1944',
					'wrapper' => array (
						'width' => '50%',
					),
				),
				array (
					'key' => $prefix_key.'imagen_obra',
					'label' => 'Imagen',
					'name' => 'imagen_obra',
					'type' => 'image',
					'instructions' => '',
					'required' => false,
					'wrapper' => array (
						'width' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'medium',
					'library' => 'all',
				),	
				
					
			),
		),
		array (
			'key' => $prefix_key.'tab_revista', // Revista y Video Corporativo
			'label' => 'Revista y Video',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'fondo_revista',
			'label' => 'Imagen de Fondo para la Sección',
			'name' => 'fondo_revista',
			'type' => 'image',
			'instructions' => 'Tamaño recomendado: 1920x1080',
			'required' => false,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
		),
		array (
			'key' => $prefix_key.'titulo_revista',
			'name' => 'titulo_revista',
			'label' => 'Título Revista',
			'type' => 'text',			
			'instructions' => 'ej. Revista 75 Años'
		),
		array (
			'key' => $prefix_key.'texto_revista',
			'name' => 'texto_revista',
			'label' => 'Texto Revista',
			'type' => 'wysiwyg',
			'media_upload' => true,
			'toolbar' => 'full',
			'required' => false,
			'instructions' => ''
		),
		array (
			'key' => $prefix_key.'titulo_video',
			'name' => 'titulo_video',
			'label' => 'Título Video',
			'type' => 'text',			
			'instructions' => 'ej. Video Corporativo'
		),
		array (
			'key' => $prefix_key.'url_video',
			'name' => 'url_video',
			'label' => 'URL Video (Youtube)',
			'type' => 'text',			
			'instructions' => 'ej. https://www.youtube.com/watch?v=HFqWAUyVcKM'
		),
	),
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'location' => array (
		array (
			array (
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'page-templates/somos.php',
			),
		),
	)
));


?>