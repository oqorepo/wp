<?php

/* Custom Post Type: Proyectos */

$prefix_key = 'proyecto_';

acf_add_local_field_group( array(
	'key' => 'proyectos',
	'title' => 'Información del Proyecto',
	'fields' => array (
		array (
			'key' => $prefix_key.'tab_info', // Información General
			'label' => 'Información General',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'estado',
			'label' => 'Estado',
			'name' => 'estado',
			'type' => 'radio',
			'instructions' => 'Cuando el estado es "Finalizado", no estara visible en el Inicio',
			'required' => true,
			'conditional_logic' => 0,
			'choices' => array (
				'venta' => 'En venta',
				'finalizado' => 'Finalizado',
			),
			'default_value' => 'venta',
			'layout' => 'horizontal',
		),
		array (
			'key' => $prefix_key.'bajada',
			'name' => 'bajada',
			'label' => 'Bajada (plus del proyecto)',
			'type' => 'text',			
			'instructions' => 'ej. Experiencias Únicas'
		),
		array (
			'key' => $prefix_key.'logo_proyecto',
			'label' => 'Logo Proyecto (para el listado de Inicio)',
			'name' => 'logo_proyecto',
			'type' => 'image',
			'instructions' => 'Tamaño recomendado: 500x150 sobre fondo transparente',
			'required' => false,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
		),
		array (
			'key' => $prefix_key.'ciudad',
			'name' => 'ciudad',
			'label' => 'Ciudad/Comuna',
			'type' => 'text',			
			'instructions' => 'ej. Santiago'
		),
		array (
			'key' => $prefix_key.'video_proyecto',
			'name' => 'video_proyecto',
			'label' => 'Microvideo en Youtube (loop) (opcional)',
			'type' => 'url',			
			'instructions' => 'ej. https://www.youtube.com/watch?v=13tg_I6Rv7Q'
		),
		array (
			'key' => $prefix_key.'video_proyecto_int',
			'name' => 'video_proyecto_int',
			'label' => 'URL del video Completo en Youtube (lightbox) (opcional)',
			'type' => 'url',			
			'instructions' => 'ej. https://www.youtube.com/watch?v=13tg_I6Rv7Q'
		),
		array (
			'key' => $prefix_key.'fondo_descripcion',
			'label' => 'Fondo Descripción Proyecto',
			'name' => 'fondo_descripcion',
			'type' => 'image',
			'instructions' => 'Tamaño recomendado: 1920x1080',
			'required' => false,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
		),
		array (
			'key' => $prefix_key.'texto_proyecto',
			'name' => 'texto_proyecto',
			'label' => 'Descripción del Proyecto',
			'type' => 'wysiwyg',
			'media_upload' => false,
			'toolbar' => 'full',
			'required' => true,
			'instructions' => 'ej. Hectárea desarrollada con la tecnología de Crystal Lagoons..'
		),
		array (
			'key' => $prefix_key.'galeria_proyecto',
			'label' => 'Galería del Proyecto',
			'name' => 'galeria_proyecto',
			'type' => 'gallery',
			'instructions' => '',
			'required' => false,
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
		),
/*		array (
			'key' => $prefix_key.'superficie_total',
			'name' => 'superficie_total',
			'label' => 'Superficie Total',
			'type' => 'text',			
			'instructions' => 'ej. 226',
			'prepend' => 'M2',
		),
		array (
			'key' => $prefix_key.'valor_uf_proyecto',
			'name' => 'valor_uf',
			'label' => 'Valor en UF',
			'type' => 'text',
			'prepend' => 'UF',
			//'step' => '100',	
			'instructions' => 'ej. 9.550,<br>Se utilizará como valor minimo de compra.'
		),*/
		array (
			'key' => $prefix_key.'tab_slider', // Información General
			'label' => 'Slider',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'slider',
			'label' => 'Añadir Slider',
			'name' => 'slider',
			'type' => 'repeater',
			'instructions' => '',
			'layout' => 'block',
			'button_label' => 'Añadir Otro',
			'sub_fields' => array (
				array (
					'key' => $prefix_key.'titulo_slider',
					'name' => 'titulo_slider',
					'label' => 'Título',
					'type' => 'text',
					'instructions' => 'ej. Disfruta de 365 días de vacaciones ',
					'wrapper' => array (
						'width' => '50%',
					),
				),
				array (
					'key' => $prefix_key.'bajada_slider',
					'name' => 'bajada_slider',
					'label' => 'Bajada',
					'type' => 'text',
					'instructions' => 'ej. con la mejor conectividad',
					'wrapper' => array (
						'width' => '50%',
					),
				),	
				array (
					'key' => $prefix_key.'enlace_slider',
					'name' => 'enlace_slider',
					'label' => 'Enlace (opcional para el boton ver más)',
					'type' => 'url',
					'instructions' => 'ej. http://www.desco.cl/somos',
				),
				array (
					'key' => $prefix_key.'imagen_slider',
					'label' => 'Imagen de Fondo',
					'name' => 'imagen_slider',
					'type' => 'image',
					'instructions' => 'Tamaño recomendado: 1920x1080',
					'required' => false,
					'wrapper' => array (
						'width' => '',
					),
					'return_format' => 'array',
					'preview_size' => 'medium',
					'library' => 'all',
				),	
			),
		),
		array (
			'key' => $prefix_key.'tab_equipamiento', // Equipamiento
			'label' => 'Equipamiento',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'fondo_equipamiento',
			'label' => 'Imagen de Fondo',
			'name' => 'fondo_equipamiento',
			'type' => 'image',
			'instructions' => 'Tamaño recomendado: 1920x1080',
			'required' => false,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
		),
		array (
			'key' => $prefix_key.'texto_equipamiento',
			'name' => 'texto_equipamiento',
			'label' => 'Texto',
			'type' => 'wysiwyg',
			'media_upload' => false,
			'toolbar' => 'full',
			'required' => true,
			'instructions' => ''
		),
		array (
			'key' => $prefix_key.'galeria_equipamiento',
			'label' => 'Galería Equipamiento',
			'name' => 'galeria_equipamiento',
			'type' => 'gallery',
			'instructions' => '',
			'required' => false,
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'wrapper' => array (
				'class' => 'galeria-small',
			),
		),
		array (
			'key' => $prefix_key.'tab_obra', // Obra en Vivo
			'label' => 'Obra en Vivo',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'fondo_obra',
			'label' => 'Imagen de Fondo',
			'name' => 'fondo_obra',
			'type' => 'image',
			'instructions' => 'Tamaño recomendado: 1920x1080',
			'required' => false,
			'wrapper' => array (
				'width' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
		),
		array (
			'key' => $prefix_key.'texto_obra',
			'name' => 'texto_obra',
			'label' => 'Texto',
			'type' => 'wysiwyg',
			'media_upload' => false,
			'toolbar' => 'basic',
			'required' => false,
			'instructions' => ''
		),
		
		array (
			'key' => $prefix_key.'video_obra',
			'name' => 'video_obra',
			'label' => 'URL Youtube de la Transmisión',
			'type' => 'url',			
			'instructions' => 'ej. https://www.youtube.com/watch?v=X0uuvfsiIdw'
		),
		array (
			'key' => $prefix_key.'tab_plantas', // Plantas
			'label' => 'Plantas',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'plantas',
			'label' => 'Añadir Plantas',
			'name' => 'plantas',
			'type' => 'repeater',
			'instructions' => '',
			'layout' => 'block',
			'button_label' => 'Añadir Otra',
			'sub_fields' => array (
				array (
					'key' => $prefix_key.'nombre_planta',
					'name' => 'nombre_planta',
					'label' => 'Nombre',
					'type' => 'text',			
					'instructions' => 'ej. Planta C',
					'wrapper' => array (
						'width' => '33%',
					),
				),
				array (
					'key' => $prefix_key.'bajada_planta',
					'name' => 'bajada_planta',
					'label' => 'Bajada',
					'type' => 'text',			
					'instructions' => 'ej. 3D + 2B + ESTAR',
					'wrapper' => array (
						'width' => '33%',
					),
				),
				array (
					'key' => $prefix_key.'valor_uf_planta',
					'name' => 'valor_uf_planta',
					'label' => 'Valor en UF',
					'type' => 'text',
					'prepend' => 'UF',
					//'step' => '100',	
					'instructions' => 'ej. 6.750',
					'wrapper' => array (
						'width' => '33%',
					),
				),
				array (
					'key' => $prefix_key.'superficie_int',
					'name' => 'superficie_int',
					'label' => 'Superficie Interior',
					'type' => 'text',			
					'instructions' => 'ej. 116',
					'prepend' => 'M2',
					'wrapper' => array (
						'width' => '50%',
					),
				),
				array (
					'key' => $prefix_key.'superficie_logia',
					'name' => 'superficie_logia',
					'label' => 'Superficie Logia (opcional)',
					'type' => 'text',			
					'instructions' => 'ej. 3',
					'prepend' => 'M2',
					'wrapper' => array (
						'width' => '50%',
					),
				),
				array (
					'key' => $prefix_key.'superficie_terraza',
					'name' => 'superficie_terraza',
					'label' => 'Superficie Terraza (opcional)',
					'type' => 'text',			
					'instructions' => 'ej. 12',
					'prepend' => 'M2',
					'wrapper' => array (
						'width' => '50%',
					),
				),
				array (
					'key' => $prefix_key.'superficie_total',
					'name' => 'superficie_total',
					'label' => 'Superficie Total',
					'type' => 'text',			
					'instructions' => 'ej. 131',
					'prepend' => 'M2',
					'wrapper' => array (
						'width' => '50%',
					),
				),
				
			),
		),
		array (
			'key' => $prefix_key.'tab_ubicacion', // Ubicación
			'label' => 'Ubicación',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'direccion_proyecto',
			'name' => 'direccion_proyecto',
			'label' => 'Dirección del proyecto',
			'type' => 'text',			
			'instructions' => 'ej. Dirección del proyecto, sector privilegiado, número etc...'
		),
		array (
			'key' => $prefix_key.'gmap_proyecto',
			'name' => 'gmap_proyecto',
			'label' => 'Ubicación del proyecto',
			'type' => 'google_map',			
			'instructions' => 'Arraste el marcador para una ubicación más exacta.',
			'center_lat' => '-33.4727091',
			'center_lng' => '-70.7699142',
			'zoom' => '5',
			'height' => 400,
		),
		array (
			'key' => $prefix_key.'zoom_gmap',
			'name' => 'zoom_gmap',
			'label' => 'Zoom del mapa',
			'type' => 'number',	
			'min' => 1,	
			'max' => 20,
			'step' => 1,	
			'default_value' => 15,
			'instructions' => 'Minimo: 1, Máximo: 22'
		),
		array (
			'key' => $prefix_key.'tab_sv', // Sala de Ventas
			'label' => 'Sala de Ventas',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'sv_direccion',
			'name' => 'sv_direccion',
			'label' => 'Dirección',
			'type' => 'text',			
			'instructions' => 'ej. Dirección 10479, Comuna'
		),
/*		array (
			'key' => $prefix_key.'sv_horarios',
			'name' => 'sv_horarios',
			'label' => 'Horarios',
			'type' => 'text',	
			'instructions' => 'ej. Lunes a Domingo: 10:00 a 19:00 hrs.'
		),*/
/*		
		array (
			'key' => $prefix_key.'sv_gmap',
			'name' => 'sv_gmap',
			'label' => 'Ubicación de la Sala de Ventas',
			'type' => 'google_map',			
			'instructions' => 'Opcional, solo es necesario cuando la sala de ventas se ubica en un lugar distinto al proyecto.',
			'center_lat' => '-33.4727091',
			'center_lng' => '-70.7699142',
			'zoom' => '7',
			'height' => 400,
		),*/
		array (
			'key' => $prefix_key.'sv_telefono',
			'name' => 'sv_telefono',
			'label' => 'Teléfono',
			'type' => 'text',
			'rows' => 4,			
			'instructions' => 'ej. +(56 2) 27045755'
		),
		array (
			'key' => $prefix_key.'sv_telefono2',
			'name' => 'sv_telefono2',
			'label' => 'Teléfono Secundario o Celular (opcional)',
			'type' => 'text',
			'rows' => 4,			
			'instructions' => 'ej. +(56 9) 9 87689707'
		),
		array (
			'key' => $prefix_key.'sv_email',
			'name' => 'sv_email',
			'label' => 'Email',
			'type' => 'email',			
			'instructions' => 'ej. contacto@oqo.cl<br>Nota: será utilizado solo a nivel visual.'
		),
		array (
			'key' => $prefix_key.'tab_emails', // Email
			'label' => 'Emails Formulario',
			'type' => 'tab',
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => $prefix_key.'form_message',
			'name' => 'form_message',
			'label' => 'Modo de uso:',
			'type' => 'message',
			'message' => 'Esta sección contiene a los destinatarios que recibirán todos los emails del formulario cotización del actual proyecto.<br>También es posible asignar multiples destinatarios separados por coma ej: (<strong>ventas@oqo.cl, cotizaciones@oqo.cl</strong>)',		
			'instructions' => '',
			'new_lines' => 'br'
		),
		array (
			'key' => $prefix_key.'form_email',
			'name' => 'form_email',
			'label' => 'Email Principal',
			'type' => 'text',
			'required' => false,	
			'instructions' => 'ej. ventas@oqo.cl'
		),
		array (
			'key' => $prefix_key.'form_email_cc',
			'name' => 'form_email_cc',
			'label' => 'Email con Copia (CC)',
			'type' => 'text',			
			'instructions' => 'ej. nombreproyecto@oqo.cl'
		),
		array (
			'key' => $prefix_key.'form_email_cco',
			'name' => 'form_email_cco',
			'label' => 'Email con Copia Oculta (CCO)',
			'type' => 'text',			
			'instructions' => 'ej. gerencia@oqo.cl'
		),
		
	),
	'label_placement' => 'left',
	'instruction_placement' => 'label',
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'proyectos',
			),
		),
	),
));
?>