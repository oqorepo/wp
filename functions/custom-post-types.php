<?php

/* Custom Post Type: Slider */

function register_slider() {
	$labels = array(
		'name' => __('Sliders', 'epcl_framework'),
		'singular_name' => __('Slider', 'epcl_framework'),
		'add_new' => __('Añadir Nuevo', 'epcl_framework'),
		'add_new_item' => __('Añadir Nuevo Slider', 'epcl_framework'),
		'edit_item' => __('Editar Slider', 'epcl_framework'),
		'new_item' => __('Nuevo Slider', 'epcl_framework'),
		'view_item' => __('Ver Slider', 'epcl_framework'),
		'search_items' => __('Buscar Slider', 'epcl_framework'),
		'not_found' => __('Ningún Slider encontrado', 'epcl_framework'),
		'not_found_in_trash' => __('Ningún Slider encontrado en la Papelera', 'epcl_framework'),
		'parent_item_colon' => ''
	);
	$args = array(
		'menu_icon' => 'dashicons-format-video',
		'labels' => $labels,
		'public' => false,
		'show_ui' => true,
		'show_in_nav_menus' => false,
		'exclude_from_search' => true,
		'supports' => array('title')
	);
	register_post_type('slider', $args);
}
add_action('init', 'register_slider');


/* Custom Post Type: Proyectos */

function register_proyectos() {
	$labels = array(
		'name' => __('Proyectos Venta', 'epcl_framework'),
		'singular_name' => __('Proyectos', 'epcl_framework'),
		'add_new' => __('Añadir Nuevo', 'epcl_framework'),
		'add_new_item' => __('Añadir Nuevo', 'epcl_framework'),
		'edit_item' => __('Editar', 'epcl_framework'),
		'new_item' => __('Nueva', 'epcl_framework'),
		'view_item' => __('Ver', 'epcl_framework'),
		'search_items' => __('Buscar', 'epcl_framework'),
		'not_found' => __('Ningún Proyecto encontrado', 'epcl_framework'),
		'not_found_in_trash' => __('Ningún Proyecto encontrado en la Papelera', 'epcl_framework'),
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $labels,
		'menu_icon' => 'dashicons-hammer',
		'supports' => array('title', 'editor', 'thumbnail', 'revisions'),
		'public' => true,
		'show_in_nav_menus' => true,
		'exclude_from_search' => true
	);
	register_post_type('proyectos', $args);
}
add_action('init', 'register_proyectos');

/* Custom Post Type: Proyectos */

function register_proyectos_ejecutados() {
	$labels = array(
		'name' => __('Proyectos Ejecutados', 'epcl_framework'),
		'singular_name' => __('Proyectos Ejecutados', 'epcl_framework'),
		'add_new' => __('Añadir Nuevo', 'epcl_framework'),
		'add_new_item' => __('Añadir Nuevo', 'epcl_framework'),
		'edit_item' => __('Editar', 'epcl_framework'),
		'new_item' => __('Nueva', 'epcl_framework'),
		'view_item' => __('Ver', 'epcl_framework'),
		'search_items' => __('Buscar', 'epcl_framework'),
		'not_found' => __('Ningún Proyecto encontrado', 'epcl_framework'),
		'not_found_in_trash' => __('Ningún Proyecto encontrado en la Papelera', 'epcl_framework'),
		'parent_item_colon' => ''
	);
	$args = array(
		'menu_icon' => 'dashicons-portfolio',
		'labels' => $labels,
		'public' => true,
		'show_ui' => true,
		'show_in_nav_menus' => false,
		'exclude_from_search' => true,
		'supports' => array('title')
	);
	register_post_type('proyectos-ejecutados', $args);
}
add_action('init', 'register_proyectos_ejecutados');

/* Custom Post Type: Obras en Desarrollo */

function register_obras() {
	$labels = array(
		'name' => __('Obras en Desarrollo', 'epcl_framework'),
		'singular_name' => __('Obras en Desarrollo', 'epcl_framework'),
		'add_new' => __('Añadir Nuevo', 'epcl_framework'),
		'add_new_item' => __('Añadir Nuevo', 'epcl_framework'),
		'edit_item' => __('Editar', 'epcl_framework'),
		'new_item' => __('Nueva', 'epcl_framework'),
		'view_item' => __('Ver', 'epcl_framework'),
		'search_items' => __('Buscar', 'epcl_framework'),
		'not_found' => __('Ningún Proyecto encontrado', 'epcl_framework'),
		'not_found_in_trash' => __('Ningún Proyecto encontrado en la Papelera', 'epcl_framework'),
		'parent_item_colon' => ''
	);
	$args = array(
		'menu_icon' => 'dashicons-hammer',
		'labels' => $labels,
		'public' => true,
		'show_ui' => true,
		'show_in_nav_menus' => false,
		'exclude_from_search' => true,
		'supports' => array('title')
	);
	register_post_type('obras', $args);
}
add_action('init', 'register_obras');

/* Custom Post Type: Obras Ejecutadas */

function register_obras_ejecutadas() {
	$labels = array(
		'name' => __('Obras Ejecutadas', 'epcl_framework'),
		'singular_name' => __('Obras Ejecutadas', 'epcl_framework'),
		'add_new' => __('Añadir Nuevo', 'epcl_framework'),
		'add_new_item' => __('Añadir Nuevo', 'epcl_framework'),
		'edit_item' => __('Editar', 'epcl_framework'),
		'new_item' => __('Nueva', 'epcl_framework'),
		'view_item' => __('Ver', 'epcl_framework'),
		'search_items' => __('Buscar', 'epcl_framework'),
		'not_found' => __('Ningún Proyecto encontrado', 'epcl_framework'),
		'not_found_in_trash' => __('Ningún Proyecto encontrado en la Papelera', 'epcl_framework'),
		'parent_item_colon' => ''
	);
	$args = array(
		'menu_icon' => 'dashicons-category',
		'labels' => $labels,
		'public' => true,
		'publicaly_queryable' => false,
		'query_var' => true,
		'show_ui' => true,
		'show_in_nav_menus' => false,
		'exclude_from_search' => true,
		'supports' => array('title')
	);
	register_post_type('obras-ejecutadas', $args);
}

function categoria_obras_ejecutadas() {
	register_taxonomy('categoria-obras', 'obras-ejecutadas', array(
		'hierarchical' => true,
		'show_in_nav_menus' => false
	));
}
add_action('init', 'categoria_obras_ejecutadas', 0 );
add_action('init', 'register_obras_ejecutadas');


/* Custom Post Type: Desco en Los medios */

function register_desco_medios() {
	$labels = array(
		'name' => __('Desco en los Medios', 'epcl_framework'),
		'singular_name' => __('Desco en los Medios', 'epcl_framework'),
/*		'add_new' => __('Añadir Nuevo', 'epcl_framework'),
		'add_new_item' => __('Añadir Nuevo', 'epcl_framework'),
		'edit_item' => __('Editar', 'epcl_framework'),
		'new_item' => __('Nueva', 'epcl_framework'),
		'view_item' => __('Ver', 'epcl_framework'),
		'search_items' => __('Buscar', 'epcl_framework'),
		'not_found' => __('Ningún Proyecto encontrado', 'epcl_framework'),
		'not_found_in_trash' => __('Ningún Proyecto encontrado en la Papelera', 'epcl_framework'),
		'parent_item_colon' => ''*/
	);
	$args = array(
		'menu_icon' => 'dashicons-media-document',
		'labels' => $labels,
		'public' => true,
		'show_in_nav_menus' => true,
		'exclude_from_search' => false,
		'supports' => array('title', 'editor', 'thumbnail'),
		'menu_position' => 5,
		'rewrite' => array( 'slug' => 'desco-en-los-medios' ),
	);
	register_post_type('desco-medios', $args);
}
add_action('init', 'register_desco_medios');

?>