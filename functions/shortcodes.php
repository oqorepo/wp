<?php

/* Clear */

function ep_clear_shortcode($atts, $content = NULL){
	return '<div class="ep-shortcode ep-clear clear"></div>';
}
add_shortcode('clear', 'ep_clear_shortcode');

/* Pasos */

function ep_paso_shortcode($atts, $content = NULL){
	extract( shortcode_atts( array(
		'titulo' => ''
	), $atts ) );
	return '<div class="ep-shortcode ep-paso">'.$content.'</div>';
}
add_shortcode('paso', 'ep_paso_shortcode');


/* Columnas */

function ep_column_shortcode($atts, $content = NULL){
	extract( shortcode_atts( array(
		'ancho' => '50',
		'posicion' => ''
	), $atts ) );
	return '<div class="ep-shortcode ep-columna grid-'.$ancho.' posicion-'.$posicion.' np-mobile np-tablet">'.do_shortcode($content).'</div>';
}
add_shortcode('columna', 'ep_column_shortcode');

/* Boton */

function ep_button_shortcode($atts, $content = NULL){
	extract( shortcode_atts( array(
		'url' => '',
		'estilo' => '',
		'icono' => ''
	), $atts ) );
/*	switch($estilo){
		default: $style = 'outline'; break;
		case 'blanco': $style = 'white'; break;
	}*/
	return '<a href="'.$url.'" class="button outline icon" target="_blank">'.$content.' <i class="'.$icono.'"></i></a>';
}
add_shortcode('boton', 'ep_button_shortcode');

/* Accordion */

function ep_accordion_wrapper_shortcode($atts, $content = NULL){
	return '<div class="ep-shortcode ep-accordion-wrapper">'.do_shortcode($content).'</div>';
}

add_shortcode('ep_accordion_wrapper', 'ep_accordion_wrapper_shortcode');

function ep_accordion_shortcode($atts, $content = NULL){
	extract( shortcode_atts( array(
		'titulo' => ''
	), $atts ) );
	return '<div class="ep-shortcode ep-toggle accordion-elem"><h3 class="toggle-title">'.$titulo.'</h3><div class="toggle-content">'.do_shortcode($content).'</div></div>';
}

add_shortcode('ep_accordion', 'ep_accordion_shortcode');

/* Tabs */

$tabs_divs = '';

function ep_tabs_shortcode($atts, $content = NULL ) {
    global $tabs_divs;

    $output = '<div class="ep-shortcode ep-tab">';
		$output.= '<ul class="tab-links wow fadeInLeft">'.do_shortcode($content).'</ul>';
		$output.= '<div class="tab-container wow fadeInRight text">'.$tabs_divs.'</div>';
		$output.= '<div class="clear"></div>';
	$output.= '</div>';
    return $output;  
}
add_shortcode('ep_tabs', 'ep_tabs_shortcode');

function ep_tab_shortcode($atts, $content = NULL) {  
    global $tabs_divs;
    extract(shortcode_atts(array(  
        'titulo' => ''
    ), $atts));  
	$id = 'tab-'.sanitize_title($titulo);
    $output = '<li><a href="javascript:void(0)" data-id="'.$id.'">'.$titulo.'</a></li>';
    $tabs_divs .= '<div id="'.$id.'" class="tab-item">'.do_shortcode($content).'</div>';
    return $output;
}
add_shortcode('ep_tab', 'ep_tab_shortcode');

?>