<?php

/* Unique options for every EP theme */

$admin_url = EPCL_THEMEPATH.'/functions/admin';

/* Textos */
	
/*Redux::setSection( $opt_name, array(
	'title' => __('Textos Globales', 'epcl_framework'),
	'icon' => ' el-icon-font',
	'fields' => array(
		array(
			'id' => 'home_texto_somos',
			'type' => 'editor',
			'title' => __('Home Somos', 'epcl_framework'),
			'desc' => 'Será utilizado en la página de Inicio',
			'args' => array(
				'teeny' => true,
				'media_buttons' => false,
				'textarea_rows' => 10
			)
		),
		array(
			'id' => 'home_bajada_proyectos',
			'type' => 'textarea',
			'title' => __('Home Bajada Proyectos', 'epcl_framework'),
			'desc' => 'Será utilizado en la página de Inicio',
		),
	)
) );*/

$args = array(
	'posts_per_page' => -1,
	'post_type' => 'proyectos',
	'suppress_filters' => false,
	'meta_key' => 'estado',
	'meta_value' => 'venta'
);
$proyectos = get_posts($args);
$array_proyectos = array();
foreach ($proyectos as $p){
	$array_proyectos['id-'.$p->ID] = $p->post_title;
}

//var_dump($array_proyectos);

/* Contacto */

Redux::setSection( $opt_name, array(
	'title' => __('Configuración General', 'epcl_framework'),
	'icon' => 'el-icon-globe',
	'fields' => array(
		array(
			'id'     => 'section_footer',
			'type'   => 'section',
			'title' => 'Footer (pie de página y otros)',
			'indent' => true,
		),
		array(
			'id' => 'email',
			'type' => 'text',
			'title' => __('Email (requerido)', 'epcl_framework'),
			'subtitle' => __('Destinatario para el formulario general (otras consultas).', 'epcl_framework'),
			'desc' => __('ej. contacto@oqo.cl', 'epcl_framework'),
			'validate' => 'email'
		),
		array(
			'id' => 'email_inversiones',
			'type' => 'text',
			'title' => __('Email Inversiones (requerido)', 'epcl_framework'),
			'subtitle' => __('Destinatario para el formulario Inversiones.', 'epcl_framework'),
			'desc' => __('ej. inversiones@oqo.cl', 'epcl_framework'),
			'validate' => 'email'
		),
		array(
			'id' => 'telefono',
			'type' => 'text',
			'title' => __('Telefono (oficina)', 'epcl_framework'),
			'desc' => __('ej. +562 67 233444', 'epcl_framework'),
			'default' => ''
		),
		array(
			'id' => 'direccion',
			'type' => 'text',
			'title' => __('Dirección Oficina Central', 'epcl_framework'),
			'desc' => __('ej. Av. Lorem ipsum 3445', 'epcl_framework'),
			'default' => ''
		),
		array(
			'id'     => 'section_footer2',
			'type'   => 'section',
			'title' => 'Redes Sociales',
			'indent' => true,
		),
		array(
			'id' => 'facebook',
			'type' => 'text',
			'validate' => 'url',
			'title' => __('<i class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x"></i></i> Facebook', 'epcl_framework'),
			'desc' => __('ej, http://www.facebook.com/oqo', 'epcl_framework'),
			'default' => ''
		),
		array(
			'id' => 'twitter',
			'type' => 'text',
			'validate' => 'url',
			'title' => __('<i class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x"></i></i> Twitter', 'epcl_framework'),
			'desc' => __('ej, https://twitter.com/oqodigital', 'epcl_framework'),
			'default' => ''
		),
		array(
			'id' => 'youtube',
			'type' => 'text',
			'validate' => 'url',
			'title' => __('<i class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-youtube fa-stack-1x"></i></i> Youtube', 'epcl_framework'),
			'desc' => __('ej, https://www.youtube.com/oqo', 'epcl_framework'),
			'default' => ''
		),
		array(
			'id' => 'instagram',
			'type' => 'text',
			'validate' => 'url',
			'title' => __('<i class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-instagram fa-stack-1x"></i></i> Instagram', 'epcl_framework'),
			'desc' => __('ej, https://www.instagram.com/oqo', 'epcl_framework'),
			'default' => ''
		),
	)
) );

/* Favicons */

Redux::setSection( $opt_name, array(
	'title' => __('Favicons', 'epcl_framework'),
	'icon' => 'el-icon-star',
	'fields' => array(
		array(
			'id' => 'favicon_16',
			'type' => 'media',
			'url' => true,
			'title' => __('Favicon 16x16 pixels (ico)', 'epcl_framework'),
			'desc' => __('Favicon estándar', 'epcl_framework')
		),
		array(
			'id' => 'favicon_57',
			'type' => 'media', 
			'url' => true,
			'title' => __('Apple Touch Icon 57x57 pixels (png)', 'epcl_framework'),
			'desc' => __('Para iphone sin retina, iPod Touch y Android 2.1+', 'epcl_framework')
		),
		array(
			'id' => 'favicon_72',
			'type' => 'media', 
			'url' => true,
			'title' => __('Apple Touch Icon 72x72 pixels (png)', 'epcl_framework'),
			'desc' => __('1ra y 2da generación iPad', 'epcl_framework')
		),
		array(
			'id' => 'favicon_114',
			'type' => 'media', 
			'url' => true,
			'title' => __('Apple Touch Icon 114x114 pixels (png)', 'epcl_framework'),
			'desc' => __('Iphone con Retina display', 'epcl_framework')
		),
		array(
			'id' => 'favicon_144',
			'type' => 'media', 
			'url' => true,
			'title' => __('Apple Touch Icon 144x144 pixels (png)', 'epcl_framework'),
			'desc' => __('3ra generación iPad con Retina display', 'epcl_framework')
		)
	)
) );

/* Advanced Settings */

Redux::setSection( $opt_name, array(
	'title' => __('Opciones Avanzadas', 'epcl_framework'),
	'icon' => 'el-icon-dashboard',
	'fields' => array(
		array(
			'id' => 'ga_code',
			'type' => 'textarea',
			'title' => __('Código Google Analytics', 'epcl_framework'), 
			'subtitle' => __('El código quedara incrustado dentro del &lt;body&gt;', 'epcl_framework'), 
			'desc' => __('Pegar código completo incluyendo &lt;script&gt;.', 'epcl_framework')
		),
		array(
			'id' => 'css_code',
			'type' => 'ace_editor',
			'title' => __('Código CSS', 'epcl_framework'), 
			'desc' => __('ej. #header{ background: #000; }', 'epcl_framework'),
			//'subtitle' => __('Paste your CSS code here.', 'epcl_framework'),
			'mode' => 'css',
			'theme' => 'monokai'
		),
	)
) );
