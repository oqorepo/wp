<?php

if( !function_exists('acf_add_local_field_group') ) return;

$prefix_key = 'field_';

function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyCchcoGJFmW7Tg3di_xtx3HfK3Y7RGQoq0');
}
add_action('acf/init', 'my_acf_init');

add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars'  );
function my_toolbars( $toolbars ){

	array_splice($toolbars['Basic' ][1], 10, 0, 'alignjustify') ;
	array_splice($toolbars['Full' ][1], 9, 0, 'alignjustify') ;

	// remove the 'Basic' toolbar completely
	//unset( $toolbars['Basic' ] );

	// return $toolbars - IMPORTANT!
	return $toolbars;
}


/* Paginas */

include(EPCL_ABSPATH.'/functions/custom-fields/acf-inicio.php');

include(EPCL_ABSPATH.'/functions/custom-fields/acf-somos.php');
include(EPCL_ABSPATH.'/functions/custom-fields/acf-paginas-genericas.php');
include(EPCL_ABSPATH.'/functions/custom-fields/acf-contacto.php');
include(EPCL_ABSPATH.'/functions/custom-fields/acf-inmobiliaria.php');
include(EPCL_ABSPATH.'/functions/custom-fields/acf-constructora.php');
include(EPCL_ABSPATH.'/functions/custom-fields/acf-inversionistas.php');

/* Custom Posts */

include(EPCL_ABSPATH.'/functions/custom-fields/acf-slider.php');
include(EPCL_ABSPATH.'/functions/custom-fields/acf-proyectos.php');
include(EPCL_ABSPATH.'/functions/custom-fields/acf-proyectos-ejecutados.php');
include(EPCL_ABSPATH.'/functions/custom-fields/acf-obras.php');
include(EPCL_ABSPATH.'/functions/custom-fields/acf-obras-ejecutadas.php');
include(EPCL_ABSPATH.'/functions/custom-fields/acf-desco-medios.php');

?>