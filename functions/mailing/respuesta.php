<?php
// Formularios con formato preestablecido: Contacto, Inversiones, Denuncias.

function generar_email_respuesta($data, $motivo = ''){
	
	$header = generar_header_email();
	$footer = generar_footer_email();
	$info_adicional = '';
	
	if($motivo){
		$data['Motivo'] = $motivo;
	}

	$html = $header.'
		<tr>
			<td align="center" style="padding: 40px 40px 40px 40px; font-size: 18px; color: #333; line-height: 140%;">
				<strong>&iexcl;Gracias por contactarnos!,<br />te responderemos a la brevedad.</strong>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding: 0px 40px 40px 40px;">
				<div style="padding: 30px 15px; background: #ffffff; color: #333333;"><em>Un asesor se pondrá en contacto contigo en un plazo m&aacute;ximo de 2 d&iacute;as h&aacute;biles para aclarar tus dudas.</em></div>
			</td>
		</tr> 
		
	'.$footer;

	return $html;		
}

?>