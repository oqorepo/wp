<?php
// Formularios con formato preestablecido: Contacto, Inversiones, Denuncias.

function generar_email_respuesta($data, $motivo = ''){
	
	$header = generar_header_email();
	$footer = generar_footer_email();
	$info_adicional = '';
	
	if($motivo){
		$data['Motivo'] = $motivo;
	}
	if($data['Rut']){
		$info_adicional .= '<div style="padding-botom: 5px;"><span style="display: inline-block; width: 70px;">Rut:</span> '.$data['Rut'].'</div>';
	}	
	if($data['Proyecto']){
		$info_adicional .= '<div style="padding-botom: 5px;"><span style="display: inline-block; width: 70px;">Proyecto:</span> '.$data['Proyecto'].'</div>';
	}
	if($data['Modelo']){
		$info_adicional .= '<div style="padding-botom: 5px;"><span style="display: inline-block; width: 70px;">Modelo:</span> '.$data['Modelo'].'</div>';
	}	

	$html = $header.'
		<tr>
			<td align="center" style="padding: 40px 40px 0px 40px;">
				Un asesor se contactar&aacute; con usted a la brevedad.<br />
			</td>
		</tr>
		<tr>
			<td class="two-column" style="padding: 40px 40px 25px 40px;" align="center">
				<!--[if (gte mso 9)|(IE)]>
				<table width="100%">
				<tr>
				<td width="50%" valign="top">
				<![endif]-->
				<div class="column" style="width: 100%;max-width: 290px;display: inline-block; vertical-align: middle;">
					<table width="100%" style="border-spacing: 0;">
						<tr>
							<td style="padding: 0px;">
								<table style="border-spacing: 0;width: 100%;">
									<tr>
										<td align="center" style="font-size: 18px; color: #004C80;">
											<strong>INFORMACI&Oacute;N DEL CONTACTO</strong>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<!--[if (gte mso 9)|(IE)]>
				</td><td width="50%" valign="top">
				<![endif]-->
				<div class="column" style="width: 100%;max-width: 220px;display: inline-block; vertical-align: middle;">
					<table width="100%" style="border-spacing: 0;">
						<tr>
							<td style="padding: 0px;">
								<table style="border-spacing: 0;width: 100%;">
									<tr>
										<td align="center">
											<font style="font-size: 11px; color: #999999;">'.current_time('d/m/Y H:i').'</font>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<!--[if (gte mso 9)|(IE)]>
				</td>
				</tr>
				</table>
				<![endif]-->
			</td>
		</tr>
		<tr>
			<td align="left" style="padding: 0px 40px 40px 40px;">
				<div style="padding-botom: 5px;"><span style="display: inline-block; width: 70px;">Nombre:</span> '.$data['Nombre'].'</div>
				<div style="padding-botom: 5px;"><span style="display: inline-block; width: 70px;">Email:</span> '.$data['Email'].'</div>
				<div style="padding-botom: 5px;"><span style="display: inline-block; width: 70px;">Tel&eacute;fono:</span> '.$data['Teléfono'].'</div>
				'.$info_adicional.'
			</td>
		</tr>
		<tr>
			<td align="center" style="padding: 0px 40px 40px 40px;">
				<div style="padding: 15px; background: #6DB539; color: #ffffff;">Motivo del contacto: '.$data['Motivo'].'</div>
				<div style="padding: 30px 15px; background: #ffffff; color: #333333;"><em>'.$data['Comentario'].'</em></div>
	
			</td>
		</tr>
		
	'.$footer;

	return $html;		
}

?>