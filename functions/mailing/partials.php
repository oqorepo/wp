<?php

/* Header */

function generar_header_email(){

	$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Contacto</title>
<!--[if (gte mso 9)|(IE)]>
<style type="text/css">
    table {border-collapse: collapse;}
</style>
<![endif]-->
</head>
<body style="padding: 0;margin: 0 !important;">
	<center class="wrapper" style="width: 100%;line-height: 130%;table-layout: fixed;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;">
		<div class="webkit" style="max-width: 600px;margin: 0 auto; background-color: #ffffff;">
			<!--[if (gte mso 9)|(IE)]>
			<table width="100%" align="center">
			<tr>
			<td width="600">
			<![endif]-->
			<table class="outer" align="center" bgcolor="#f7f7f7" style="font-family: Arial, sans-serif; font-size: 13px; color: #777777; letter-spacing: 1px; border-spacing: 0;margin: 0 auto;width: 100%;max-width: 600px;">
				<tr>
					<td align="center" bgcolor="#004C80" style="padding: 30px;">
						<a href="'.site_url().'" target="_blank"><img src="'.EP_THEMEPATH.'/images/mailing/logo-blanco.png" alt="Logo Desco" border="0" style="border: 0; max-width: 50%; height: auto;"></a>
					</td>
				</tr>';

	return $html;
}

/* Footer */

function generar_footer_email($cliente = ''){
	$html = '';

	if(!$cliente){
		$html .= '
			<tr>
				<td style="padding: 0 40px 30px 40px; font-size: 12px; line-height: 160%;" align="center">
					<p>Oficina Central: Av. Del Parque 4160, 5 piso, <br>Ciudad Empresarial, Huechuraba / Tel: +(56 2) 23929200<br />contacto@desco.cl</p>
				</td>
			</tr>
			<tr>
				<td align="center" style="padding: 0px 30px 30px 30px;">
					<a href="https://www.facebook.com/descoinmobiliaria" target="_blank"><img src="'.EP_THEMEPATH.'/images/mailing/facebook.png" width="35" height="35" alt="Facebook" border="0" /></a>
					<a href="https://twitter.com/Desco_CL" target="_blank"><img src="'.EP_THEMEPATH.'/images/mailing/twitter.png" width="35" height="35" alt="Twitter" border="0" /></a>
					<a href="https://www.youtube.com/user/DescoChile" target="_blank"><img src="'.EP_THEMEPATH.'/images/mailing/youtube.png" width="35" height="35" alt="Youtube" border="0" /></a>
					<a href="https://www.instagram.com/descoinmobiliaria" target="_blank"><img src="'.EP_THEMEPATH.'/images/mailing/instagram.png" width="35" height="35" alt="Instagram" border="0" /></a>
				</td>
			</tr>';
	}

	$html .= '
			</table>
			<!--[if (gte mso 9)|(IE)]>
			</td>
			</tr>
			</table>
			<![endif]-->
		</div>
	</center>
</body>
</html>';

	return $html;

}

?>
