<?php
// Formulario Cotizacion desde Proyecto y desde Modelos

function generar_email_cotizacion($data, $proyecto_id){
	
	$plantas = get_field('plantas', $proyecto_id);
	$html_imagen_planta = $html_logia = $html_terraza = $html_sv = $html_info_sup = $html_info_inf = $html_banner = '';
	
	$logo_proyecto = get_field('logo_proyecto', $proyecto_id);
	
	$mailing_banner = get_field('mailing_banner', $proyecto_id);
	$mailing_titulo_sup = get_field('mailing_titulo_sup', $proyecto_id);
	$mailing_bajada_sup = get_field('mailing_bajada_sup', $proyecto_id);
	$mailing_titulo_inf = get_field('mailing_titulo_inf', $proyecto_id);
	$mailing_bajada_inf = get_field('mailing_bajada_inf', $proyecto_id);
	$mailing_galeria = get_field('mailing_galeria', $proyecto_id);
	
	if(!empty($mailing_banner)){
		$html_banner = '
			<tr>
				<td align="center" style="padding: 0;">
					<img src="'.$mailing_banner['sizes']['noticia-blog'].'" alt="Banner" width="600" style="max-width: 100%; height: auto;" />
				</td>
			</tr>';
					
	}
	
	if($mailing_titulo_sup && $mailing_bajada_sup){
		$html_info_sup = '
				<tr>
					<td align="center" style="padding: 40px 40px 0px 40px; line-height: 160%;" colspan="2">
						<div style="font-size: 24px; color: #1f4075; line-height: 120%; text-transform: uppercase;"><strong>'.$mailing_titulo_sup.'</strong></div>
                        <div style="padding-top: 20px;">'.nl2br($mailing_bajada_sup).'</div>
					</td>
				</tr>';	
	}
	
	if($mailing_titulo_inf && $mailing_bajada_inf){
		$html_info_inf = '
				<tr>
					<td align="center" style="padding: 0px 40px 40px 40px; line-height: 160%;" colspan="2">
						<div style="font-size: 24px; color: #1f4075; line-height: 120%; text-transform: uppercase;"><strong>'.$mailing_titulo_inf.'</strong></div>
                        <div style="padding-top: 20px;">'.nl2br($mailing_bajada_inf).'</div>
					</td>
				</tr>';	
	}
	
	foreach($plantas as $p){
		if($p['nombre_planta'] == $data['Modelo']){	
			$nombre_planta = $p['nombre_planta'];
			$bajada_planta = $p['bajada_planta'];
			$valor_uf_planta = $p['valor_uf_planta'];
			
			$superficie_int = $p['superficie_int'];
			$superficie_logia = $p['superficie_logia'];
			$superficie_terraza = $p['superficie_terraza'];
			$superficie_total = $p['superficie_total'];
			$imagen_planta = $p['imagen_planta'];
			
			
			if(!empty($imagen_planta))
				$html_imagen_planta = '<img src="'.$imagen_planta['sizes']['planta-thumb'].'" alt="Emplazamiento" width="450" style="max-width: 90%; height: auto;" />';
			
			if($superficie_logia)
				$html_logia = '<div style="padding-bottom: 5px;">Superficie Loggia: <span style="display: inline-block;">'.$superficie_logia.' m2</span></div>';
			
			if($superficie_terraza)
				$html_terraza = '<div style="padding-bottom: 5px;">Superficie Terraza: <span style="display: inline-block;">'.$superficie_terraza.' m2</span></div>';	
		}
	}
	
	// Sala de Ventas
	
	$sv_telefono = get_field('sv_telefono', $proyecto_id);
	$sv_telefono2 = get_field('sv_telefono2', $proyecto_id);
	$sv_email = get_field('sv_email', $proyecto_id);
	$sv_direccion = get_field('sv_direccion', $proyecto_id);
	
	if($sv_telefono || $sv_telefono2){
		$html_sv = '
		<div style="color: #62b030; padding-top: 10px;"><strong>TEL&Eacute;FONO:</strong></div>
		<div><a href="tel:'.str_replace(array(' ', '(', ')'), '', $sv_telefono).'" style="color: #ffffff;">'.$sv_telefono.'</a>';
		if($sv_telefono2){
			$html_sv .= ' / <a href="tel:'.str_replace(array(' ', '(', ')'), '', $sv_telefono2).'" style="color: #ffffff;">'.$sv_telefono2.'</a>';
		}
		$html_sv .= '</div>';	
	}

	$html = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Cotizacion</title>
<!--[if (gte mso 9)|(IE)]>
<style type="text/css">
    table {border-collapse: collapse;}
</style>
<![endif]-->
</head>
<body style="padding: 0;margin: 0 !important;">
	<center class="wrapper" style="width: 100%;line-height: 130%;table-layout: fixed;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;">
		<div class="webkit" style="max-width: 600px;margin: 0 auto; background-color: #ffffff;">
			<!--[if (gte mso 9)|(IE)]>
			<table width="100%" align="center">
			<tr>
			<td width="600">
			<![endif]-->
			<table class="outer" align="center" bgcolor="#f7f7f7" style="font-family: Arial, sans-serif; font-size: 13px; color: #777777; letter-spacing: 1px; border-spacing: 0;margin: 0 auto;width: 100%;max-width: 600px;">
            	<tr>
                	<td>
                        <table width="100%" border="0" cellspacing="0">
                            <tr>
                                <td align="center" width="50%" bgcolor="#ffffff" style="padding: 30px 40px 30px 40px;">
                                    <img src="'.$logo_proyecto['sizes']['proyecto-logo'].'" alt="Logo '.$data['Proyecto'].'" style="max-width: 140px; height: auto;" />
                                </td>
                                <td align="center" width="50%" bgcolor="#ffffff" style="padding: 30px 40px 30px 40px;">
                                    <img src="'.EP_THEMEPATH.'/images/mailing/logo.png" alt="Logo Desco" style="max-width: 100%; height: auto;" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                '.$html_banner.'
                <tr bgcolor="#efefef">
					<td class="two-column" style="padding: 25px 40px 10px 40px;" align="center">
                        <!--[if (gte mso 9)|(IE)]>
                        <table width="100%">
                        <tr>
                        <td width="300" valign="top">
                        <![endif]-->
                        <div class="column" style="width: 100%;max-width: 290px;display: inline-block; vertical-align: middle;">
                            <table width="100%" style="border-spacing: 0;">
                                <tr>
                                    <td style="padding: 0px;">
                                        <table style="border-spacing: 0;width: 100%;">
                                            <tr>
                                                <td align="center" style="font-size: 13px; color: #004C80;">
                                                	<strong>INFORMACI&Oacute;N DE CONTACTO</strong>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td><td width="300" valign="top">
                        <![endif]-->
                        <div class="column" style="width: 100%;max-width: 220px;display: inline-block; vertical-align: middle;">
                            <table width="100%" style="border-spacing: 0;">
                                <tr>
                                    <td style="padding: 0px;">
                                        <table style="border-spacing: 0;width: 100%;">
                                            <tr>
                                                <td align="center">
                                                	<font style="font-size: 11px; color: #999999;">'.current_time('d/m/Y H:i').'</font>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
					</td>
				</tr>
                <tr bgcolor="#efefef">
                    <td align="left" style="padding: 0px 40px 25px 40px;">
                        <div style="padding-botom: 5px;"><span style="display: inline-block; width: 70px;">Nombre:</span> '.$data['Nombre'].'</div>
				<div style="padding-botom: 5px;"><span style="display: inline-block; width: 70px;">Email:</span> '.$data['Email'].'</div>
				<div style="padding-botom: 5px;"><span style="display: inline-block; width: 70px;">Tel&eacute;fono:</span> '.$data['Teléfono'].'</div>
                    </td>
                </tr>
                '.$html_info_sup.'
                <tr>
                	<td colspan="2" style="padding: 40px 40px 0px 40px;">
                        <table border="0" cellspacing="0" width="100%">
                        	<tr>
                                <td align="center" width="50%" bgcolor="#6DB539" style="font-size: 12px; padding: 0px 5px 0px 5px;">
                                    <div style="padding: 15px 15px 15px 15px; color: #ffffff;">Modelo: '.$data['Modelo'].'</div>
                                </td>
                                <td align="center" width="50%" bgcolor="#6DB539" style="font-size: 12px; padding: 0px 5px 0px 5px;">
                                    <div style="background: #6DB539; padding: 15px 15px 15px 15px; color: #ffffff;">Desde UF '.$valor_uf_planta.'</div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
					<td align="center" style="padding: 0px 40px 40px 40px;">
                    	<div style="background: #ffffff; padding-top: 20px;">
                            <div style="color: #1f4075; font-size: 24px; line-height: 120%; text-transform: uppercase;"><strong><font color="#84838a">'.$nombre_planta.'</font><br />'.$bajada_planta.'</strong></div>
                            '.$html_imagen_planta.'
                            <table border="0" cellspacing="0" width="100%" style="font-size: 12px;">
                                <tr>
                                    <td align="left" valign="top" width="50%" style="padding: 20px;">
                                        <div style="padding-bottom: 5px;">Superficie Interior: <span style="display: inline-block;">'.$superficie_int.'</span></div>
                                        '.$html_logia.'
                                    </td>
                                    <td align="left" valign="top" width="50%" style="padding: 20px;">
                                        '.$html_terraza.'
                                        <div style="color: #1f4075; padding-bottom: 5px;"><strong>Superficie Total: <span style="display: inline-block;">'.$superficie_total.' m2</span></strong></div>
                                    </td>
                                </tr>
							</table>
                        </div>
					</td>
				</tr>
                '.$html_info_inf.'
                <tr>
					<td class="two-column" style="padding: 0px;" align="center">
                        <!--[if (gte mso 9)|(IE)]>
                        <table width="100%">
                        <tr>
                        <td width="300" valign="top">
                        <![endif]-->
                        <div class="column" style="width: 100%;max-width: 297px;display: inline-block; vertical-align: middle;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding: 4px 0px 0px 0px; border-spacing: 0;">
                                <tr>
                                    <td style="padding: 0px;">
                                        <table style="border-spacing: 0;width: 100%;">
                                            <tr>
                                                <td align="center" style="padding: 0;">
                                                	<img src="'.$mailing_galeria[0]['sizes']['mailing-galeria'].'" width="300" style="display: block; max-width: 100%; height: auto;" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td><td width="300" valign="top">
                        <![endif]-->
                        <div class="column" style="width: 100%;max-width: 297px;display: inline-block; vertical-align: middle;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding: 4px 0px 0px 0px; border-spacing: 0;">
                                <tr>
                                    <td style="padding: 0px;">
                                        <table style="border-spacing: 0;width: 100%;">
                                            <tr>
                                                <td align="center">
                                                	<img src="'.$mailing_galeria[1]['sizes']['mailing-galeria'].'" width="300" style="display: block; max-width: 100%; height: auto;" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
					</td>
				</tr>
                <tr>
					<td class="two-column" style="padding: 0px;" align="center">
                        <!--[if (gte mso 9)|(IE)]>
                        <table width="100%">
                        <tr>
                        <td width="300" valign="top">
                        <![endif]-->
                        <div class="column" style="width: 100%;max-width: 297px;display: inline-block; vertical-align: middle;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding: 4px 0px 0px 0px; border-spacing: 0;">
                                <tr>
                                    <td style="padding: 0px;">
                                        <table style="border-spacing: 0;width: 100%;">
                                            <tr>
                                                <td align="center" style="padding: 0;">
                                                	<img src="'.$mailing_galeria[2]['sizes']['mailing-galeria'].'" width="300" style="display: block; max-width: 100%; height: auto;" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td><td width="300" valign="top">
                        <![endif]-->
                        <div class="column" style="width: 100%;max-width: 297px;display: inline-block; vertical-align: middle;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding: 4px 0px 0px 0px; border-spacing: 0;">
                                <tr>
                                    <td style="padding: 0px;">
                                        <table style="border-spacing: 0;width: 100%;">
                                            <tr>
                                                <td align="left">
                                                	<div style="background: #1f4075; color: #fff; font-size: 11px; line-height: 150%; padding: 15px; min-height: 137px;">
                                                        <div style="color: #62b030;"><strong>SALA DE VENTAS:</strong></div>
                                                        <div>'.$sv_direccion.'</div>
                                                        '.$html_sv.'
                                                        <div style="color: #62b030; padding-top: 10px;"><strong>EMAIL:</strong></div>
                                                        <div><a href="mailto:'.$sv_email.'" style="color: #ffffff;">'.$sv_email.'</a></div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
					</td>
				</tr>
                <tr>
					<td style="padding: 40px 40px 0px 40px; font-size: 12px; line-height: 160%;" align="center">
                        La información y textos contenidos en este documento son meramente ilustrativos 
                        y referenciales, por lo que podrían no representar exactamente la realidad.
                        Confirmar valores y disponibilidad en Sala de Ventas.
					</td>
				</tr>
                <tr>
                    <td align="center" style="padding: 30px;">
                        <a href="https://www.facebook.com/descoinmobiliaria" target="_blank"><img src="'.EP_THEMEPATH.'/images/mailing/facebook.png" width="35" height="35" alt="Facebook" border="0" /></a>
                        <a href="https://twitter.com/Desco_CL" target="_blank"><img src="'.EP_THEMEPATH.'/images/mailing/twitter.png" width="35" height="35" alt="Twitter" border="0" /></a>
                        <a href="https://www.youtube.com/user/DescoChile" target="_blank"><img src="'.EP_THEMEPATH.'/images/mailing/youtube.png" width="35" height="35" alt="Youtube" border="0" /></a>
                        <a href="https://www.instagram.com/descoinmobiliaria" target="_blank"><img src="'.EP_THEMEPATH.'/images/mailing/instagram.png" width="35" height="35" alt="Instagram" border="0" /></a>
                    </td>
                </tr>
			</table>
			<!--[if (gte mso 9)|(IE)]>
			</td>
			</tr>
			</table>
			<![endif]-->
		</div>
	</center>
</body>
</html>
	';

	return $html;		
}

?>