<?php

/* Scripts and Styles */

function ep_scripts() {
	global $ep_theme;

	$directory_uri = get_template_directory_uri(); // better include

	$ver = '5.3';

	/* jQuery to Footer */

/*	wp_scripts()->add_data( 'jquery', 'group', 1 );
    wp_scripts()->add_data( 'jquery-core', 'group', 1 );
    wp_scripts()->add_data( 'jquery-migrate', 'group', 1 );*/

	/* Styles */

	wp_enqueue_style('grids', $directory_uri.'/css/grids.css', NULL, NULL);
	//wp_enqueue_style('aos', $directory_uri.'/css/aos.css', NULL, NULL);
	wp_enqueue_style('fontawesome', $directory_uri.'/css/font-awesome.min.css', NULL, NULL);
	wp_enqueue_style('flaticon', $directory_uri.'/css/flaticon.css', NULL, $ver);
	wp_enqueue_style('style', $directory_uri.'/style.css', NULL, $ver);
	wp_enqueue_style('animate', $directory_uri.'/css/animate.css', array('style'), NULL);
	wp_enqueue_style('common-elements', $directory_uri.'/css/common-elements.css', NULL, $ver);
	wp_register_style('responsive', $directory_uri.'/css/responsive.css', array('style'), $ver);


	//jQuery Plugins

	wp_register_style('magnific', $directory_uri.'/js/magnific/magnific-popup.css', NULL, $ver);
	wp_register_style('slick', $directory_uri.'/js/slick/slick.css', NULL, $ver);
	wp_register_style('slick-theme', $directory_uri.'/js/slick/slick-theme.css', NULL, $ver);
	wp_register_style('tooltipster', $directory_uri.'/js/tooltipster/css/tooltipster.css', NULL, $ver);
	wp_register_style('tooltipster-small', $directory_uri.'/js/tooltipster/css/themes/tooltipster-small.css', NULL, $ver);
	wp_register_style('fancybox', $directory_uri.'/js/fancybox/jquery.fancybox.css', NULL, $ver);
	wp_register_style('pikaday', $directory_uri.'/js/pikaday/pikaday.css', NULL, $ver);

	wp_register_style('masterslider', $directory_uri.'/js/masterslider/css/masterslider.main.css', NULL, $ver);
	wp_register_style('scrollbar', $directory_uri.'/js/scrollbar/jquery.mCustomScrollbar.css', NULL, $ver);

	wp_register_style('nice-select', $directory_uri.'/js/nice-select/nice-select.css', NULL, $ver);
	wp_register_style('fullpage', $directory_uri.'/js/fullpage/jquery.fullpage.css', NULL, $ver);

	/* Scripts */

	wp_register_script('global', $directory_uri.'/js/global.js', array('jquery'), $ver, true);
	wp_register_script('sticky', $directory_uri.'/js/jquery.sticky.js', array('jquery'), $ver, true);
	wp_register_script('debounce', $directory_uri.'/js/jquery.debounce.js', array('jquery'), $ver, true);
	wp_register_script('placeholder', $directory_uri.'/js/jquery.placeholder.min.js', array('jquery'), $ver, true);

	wp_register_script('tooltipster', $directory_uri.'/js/tooltipster/jquery.tooltipster.min.js', array('jquery'), $ver, true);
	wp_register_script('magnific', $directory_uri.'/js/magnific/jquery.magnific-popup.min.js', array('jquery'), $ver, true);
	wp_register_script('gmap', 'https://maps.google.com/maps/api/js?key=AIzaSyA098xzD4UzRUZBv8OSIQ-ddupC-lB64Gg&sensor=false', array('jquery'), $ver, true);
	wp_register_script('mapa', $directory_uri.'/js/mapa.js', array('jquery', 'gmap'), $ver, true);
	wp_register_script('slick', $directory_uri.'/js/slick/slick.min.js', array('jquery'), $ver, true);

	wp_register_script('fancybox', $directory_uri.'/js/fancybox/jquery.fancybox.pack.js', array('jquery'), $ver, true);
	wp_register_script('fancybox-media', $directory_uri.'/js/fancybox/helpers/jquery.fancybox-media.js', array('jquery'), $ver, true);
	wp_register_script('tabs', $directory_uri.'/js/jquery.tools.min.js', array('jquery'), $ver, true);

	wp_register_script('scrollto', $directory_uri.'/js/jquery.scrollTo.min.js', array('jquery'), $ver, true);
	wp_register_script('localscroll', $directory_uri.'/js/jquery.localScroll.min.js', array('jquery'), $ver, true);

	wp_register_script('stellar', $directory_uri.'/js/jquery.stellar.min.js', array('jquery'), $ver, true);
	wp_register_script('pikaday', $directory_uri.'/js/pikaday/pikaday.js', array('jquery'), $ver, true);
	wp_register_script('pikaday-jq', $directory_uri.'/js/pikaday/pikaday.jquery.js', array('jquery'), $ver, true);
	wp_register_script('pikaday-moment', $directory_uri.'/js/pikaday/moment.min.js', array('jquery'), $ver, true);

	wp_register_script('masterslider', $directory_uri.'/js/masterslider/masterslider.min.js', array('jquery'), $ver, true);
	wp_register_script('masterslider-easing', $directory_uri.'/js/masterslider/jquery.easing.min.js', array('jquery'), $ver, true);

	wp_register_script('scrollbar', $directory_uri.'/js/scrollbar/jquery.mCustomScrollbar.concat.min.js', array('jquery'), $ver, true);

	wp_register_script('youtube-bg', $directory_uri.'/js/jquery.youtubebackground.js', array('jquery'), $ver, true);

	wp_register_script('nice-select', $directory_uri.'/js/nice-select/jquery.nice-select.min.js', array('jquery'), $ver, true);

	wp_register_script('scrollify', $directory_uri.'/js/jquery.scrollify.min.js', array('jquery'), $ver, true);

	wp_register_script('fullpage', $directory_uri.'/js/fullpage/jquery.fullpage.min.js', array('jquery'), $ver, true);
	wp_register_script('fullpage-ext', $directory_uri.'/js/fullpage/jquery.fullpage.extensions.min.js', array('jquery'), $ver, true);
	wp_register_script('recaptcha', 'https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit', array('jquery'), $ver, true);

	wp_register_script('lazyload', $directory_uri.'/js/lazyload.min.js', array('jquery'), $ver, true);

	/* Scripts Paginas */

	wp_register_script('inicio', $directory_uri.'/js/pagina-inicio.js', array('jquery', 'masterslider'), $ver, true);
	wp_register_script('proyecto-int', $directory_uri.'/js/pagina-proyecto-int.js', array('jquery'), $ver, true);
	wp_register_script('somos', $directory_uri.'/js/pagina-somos.js', array('jquery'), $ver, true);
	wp_register_script('inversionistas', $directory_uri.'/js/pagina-inversionistas.js', array('jquery'), $ver, true);
	wp_register_script('formularios', $directory_uri.'/js/formularios.js', array('jquery'), $ver, true);
	wp_register_script('page', $directory_uri.'/js/pagina-generica.js', array('jquery'), $ver, true);

	/* Variables Ajax */

		wp_localize_script('faq', 'ajax_var', array(
			'url' => admin_url('admin-ajax.php'),
			'nonce' => wp_create_nonce('faq')
		));

	/* Calls */

	//if(!is_page_template('page-templates/contacto.php') && !is_page_template('page-templates/video.php')){
		wp_enqueue_style('responsive');
		wp_enqueue_style('magnific');


		wp_enqueue_style('slick');
		wp_enqueue_style('slick-theme');
		wp_enqueue_style('tooltipster');
		wp_enqueue_style('tooltipster-small');
		wp_enqueue_style('fullpage');

		if( !is_admin() ){
			wp_deregister_script("jquery");
			wp_enqueue_script('jquery', $directory_uri.'/js/jquery.min.js', false, null, true);
		}
		wp_enqueue_script('debounce');
		wp_enqueue_script('sticky');
		wp_enqueue_script('placeholder');
		wp_enqueue_script('scrollify');
		wp_enqueue_script('fullpage');
		wp_enqueue_script('fullpage-ext');

		wp_enqueue_script('lazyload');

		wp_enqueue_script('tooltipster');

		wp_enqueue_script('magnific');
		wp_enqueue_script('slick');
		wp_enqueue_script('tabs');
		//wp_enqueue_script('smoothscroll');
		wp_enqueue_script('stellar');
		wp_enqueue_script('scrollto');
		wp_enqueue_script('localscroll');
		wp_enqueue_script('formularios');
		wp_enqueue_script('youtube-bg');

		wp_enqueue_style('nice-select');
		wp_enqueue_script('nice-select');

		wp_enqueue_style('scrollbar');
		wp_enqueue_script('scrollbar');

		wp_enqueue_script('global');

	//}

	if(!isset($_GET['width'])){
		wp_enqueue_style('responsive');
	}

	/* Paginas Genericas */

	if( is_page_template('page-templates/inicio.php') ){
		wp_enqueue_style('masterslider');
		wp_enqueue_script('masterslider-easing');
		wp_enqueue_script('masterslider');
		wp_enqueue_script('inicio');
	}

	if( is_page_template('page-templates/contacto.php') || is_singular('proyectos') || is_page_template('page-templates/canal-denuncia.php') || is_page_template('page-templates/inversionistas.php') ){
		wp_enqueue_script('recaptcha');
	}

	if( is_page_template('page-templates/somos.php') ){
		wp_enqueue_script('somos');
	}

	if( is_page_template('page-templates/inversionistas.php') ){
		wp_enqueue_script('inversionistas');
	}


	/* Proyectos*/

	if( is_singular('proyectos') ){
		wp_enqueue_script('gmap');
		wp_enqueue_script('proyecto-int');
	}


	/* Exportar */

	if( is_page_template('page-templates/exportar.php') ){
		wp_enqueue_style('pikaday');
		wp_enqueue_script('pikaday-moment');
		wp_enqueue_script('pikaday');
		wp_enqueue_script('pikaday-jq');
	}

}
add_action('wp_enqueue_scripts', 'ep_scripts');

?>
